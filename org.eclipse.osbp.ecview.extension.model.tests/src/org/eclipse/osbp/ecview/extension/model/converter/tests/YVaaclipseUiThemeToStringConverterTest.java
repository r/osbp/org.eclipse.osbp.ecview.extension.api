/**
 * All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.
 * 
 * Contributors:
 *       Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.extension.model.converter.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;
import org.eclipse.osbp.ecview.extension.model.converter.YVaaclipseUiThemeToStringConverter;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>YVaaclipse Ui Theme To String Converter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVaaclipseUiThemeToStringConverterTest extends TestCase {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The fixture for this YVaaclipse Ui Theme To String Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVaaclipseUiThemeToStringConverter fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(YVaaclipseUiThemeToStringConverterTest.class);
	}

	/**
	 * Constructs a new YVaaclipse Ui Theme To String Converter test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVaaclipseUiThemeToStringConverterTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this YVaaclipse Ui Theme To String Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(YVaaclipseUiThemeToStringConverter fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this YVaaclipse Ui Theme To String Converter test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected YVaaclipseUiThemeToStringConverter getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(YConverterFactory.eINSTANCE.createYVaaclipseUiThemeToStringConverter());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //YVaaclipseUiThemeToStringConverterTest
