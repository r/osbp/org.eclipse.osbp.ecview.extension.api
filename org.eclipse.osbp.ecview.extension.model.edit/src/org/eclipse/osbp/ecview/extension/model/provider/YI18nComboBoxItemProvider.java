/**
 *                                                                            
 *  Copyright (c) 2011, 2015 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.provider.YInputItemProvider;

import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YI18nComboBox;

import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YI18nComboBox} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class YI18nComboBoxItemProvider extends YInputItemProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YI18nComboBoxItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCollectionBindingEndpointPropertyDescriptor(object);
			addSelectionBindingEndpointPropertyDescriptor(object);
			addDatadescriptionPropertyDescriptor(object);
			addDatatypePropertyDescriptor(object);
			addSelectionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Collection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCollectionBindable_collectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCollectionBindable_collectionBindingEndpoint_feature", "_UI_YCollectionBindable_type"),
				 CoreModelPackage.Literals.YCOLLECTION_BINDABLE__COLLECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Binding Endpoint feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSelectionBindable_selectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSelectionBindable_selectionBindingEndpoint_feature", "_UI_YSelectionBindable_type"),
				 CoreModelPackage.Literals.YSELECTION_BINDABLE__SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datadescription feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatadescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YI18nComboBox_datadescription_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YI18nComboBox_datadescription_feature", "_UI_YI18nComboBox_type"),
				 YECviewPackage.Literals.YI1_8N_COMBO_BOX__DATADESCRIPTION,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Datatype feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatatypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YI18nComboBox_datatype_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YI18nComboBox_datatype_feature", "_UI_YI18nComboBox_type"),
				 YECviewPackage.Literals.YI1_8N_COMBO_BOX__DATATYPE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YI18nComboBox_selection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YI18nComboBox_selection_feature", "_UI_YI18nComboBox_type"),
				 YECviewPackage.Literals.YI1_8N_COMBO_BOX__SELECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YI18nComboBox_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YI18nComboBox_type_feature", "_UI_YI18nComboBox_type"),
				 YECviewPackage.Literals.YI1_8N_COMBO_BOX__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YI18nComboBox_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YI18nComboBox_emfNsURI_feature", "_UI_YI18nComboBox_type"),
				 YECviewPackage.Literals.YI1_8N_COMBO_BOX__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YI18nComboBox_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YI18nComboBox_typeQualifiedName_feature", "_UI_YI18nComboBox_type"),
				 YECviewPackage.Literals.YI1_8N_COMBO_BOX__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YI18nComboBox.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YI18nComboBox"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YI18nComboBox)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YI18nComboBox_type") :
			getString("_UI_YI18nComboBox_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YI18nComboBox.class)) {
			case YECviewPackage.YI1_8N_COMBO_BOX__SELECTION:
			case YECviewPackage.YI1_8N_COMBO_BOX__TYPE:
			case YECviewPackage.YI1_8N_COMBO_BOX__EMF_NS_URI:
			case YECviewPackage.YI1_8N_COMBO_BOX__TYPE_QUALIFIED_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYObjectToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYStringToByteArrayConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYCustomDecimalConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYNumericToResourceConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYStringToResourceConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYPriceToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYQuantityToStringConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYNumericToUomoConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYDecimalToUomoConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYSimpleDecimalConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 YConverterFactory.eINSTANCE.createYVaaclipseUiThemeToStringConverter()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
