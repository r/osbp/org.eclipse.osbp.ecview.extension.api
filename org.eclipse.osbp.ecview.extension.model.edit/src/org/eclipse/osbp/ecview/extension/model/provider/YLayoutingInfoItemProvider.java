/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IChildCreationExtender;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelFactory;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;

/**
 * This is the item provider adapter for a
 * {@link org.eclipse.osbp.ecview.extension.model.YLayoutingInfo} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class YLayoutingInfoItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public YLayoutingInfoItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addTagsPropertyDescriptor(object);
			addIdPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addLayoutPropertyDescriptor(object);
			addFirstFocusPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_id_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YElement_name_feature", "_UI_YElement_type"),
				 CoreModelPackage.Literals.YELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Tags feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTagsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YTaggable_tags_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YTaggable_tags_feature", "_UI_YTaggable_type"),
				 CoreModelPackage.Literals.YTAGGABLE__TAGS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Layout feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addLayoutPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YLayoutingInfo_layout_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YLayoutingInfo_layout_feature", "_UI_YLayoutingInfo_type"),
				 YECviewPackage.Literals.YLAYOUTING_INFO__LAYOUT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the First Focus feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addFirstFocusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YLayoutingInfo_firstFocus_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YLayoutingInfo_firstFocus_feature", "_UI_YLayoutingInfo_type"),
				 YECviewPackage.Literals.YLAYOUTING_INFO__FIRST_FOCUS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CoreModelPackage.Literals.YELEMENT__PROPERTIES);
			childrenFeatures.add(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT);
			childrenFeatures.add(YECviewPackage.Literals.YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns YLayoutingInfo.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YLayoutingInfo"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YLayoutingInfo)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YLayoutingInfo_type") :
			getString("_UI_YLayoutingInfo_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YLayoutingInfo.class)) {
			case YECviewPackage.YLAYOUTING_INFO__TAGS:
			case YECviewPackage.YLAYOUTING_INFO__ID:
			case YECviewPackage.YLAYOUTING_INFO__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case YECviewPackage.YLAYOUTING_INFO__PROPERTIES:
			case YECviewPackage.YLAYOUTING_INFO__CONTENT:
			case YECviewPackage.YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YELEMENT__PROPERTIES,
				 CoreModelFactory.eINSTANCE.create(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP)));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYStrategyLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYBlobUploadComponent()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYCustomDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYI18nComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYIconComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYQuantityTextField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYContentSensitiveLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYRichTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYPrefixedMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYMaskedNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYMaskedDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 YECviewFactory.eINSTANCE.createYPairComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 CoreModelFactory.eINSTANCE.createYLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 CoreModelFactory.eINSTANCE.createYHelperLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 CoreModelFactory.eINSTANCE.createYField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 CoreModelFactory.eINSTANCE.createYAction()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYGridLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYHorizontalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYVerticalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYTable()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYTree()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYList()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYLabel()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYImage()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYTextField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYBeanReferenceField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYCheckBox()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYBrowser()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYDateTime()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYButton()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYSlider()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYToggleButton()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYProgressBar()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYMasterDetail()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYFormLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYTextSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYBooleanSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYNumericSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYReferenceSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYPanel()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYSplitPanel()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYEnumList()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYCssLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYAbsoluteLayout()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYSuggestTextField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYPasswordField()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYFilteringComponent()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__CONTENT,
				 ExtensionModelFactory.eINSTANCE.createYKanban()));

		newChildDescriptors.add
			(createChildParameter
				(YECviewPackage.Literals.YLAYOUTING_INFO__ACTIVE_SUSPECT_INFOS,
				 YECviewFactory.eINSTANCE.createYSuspectInfo()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the resource locator
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ((IChildCreationExtender)adapterFactory).getResourceLocator();
	}

}
