/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.provider.YLayoutItemProvider;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesFactory;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;
import org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class YContentSensitiveLayoutItemProvider extends YLayoutItemProvider {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public YContentSensitiveLayoutItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSpacingPropertyDescriptor(object);
			addMarginPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Spacing feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSpacingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSpacingable_spacing_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSpacingable_spacing_feature", "_UI_YSpacingable_type"),
				 CoreModelPackage.Literals.YSPACINGABLE__SPACING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Margin feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addMarginPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMarginable_margin_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMarginable_margin_feature", "_UI_YMarginable_type"),
				 CoreModelPackage.Literals.YMARGINABLE__MARGIN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns YContentSensitiveLayout.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/YContentSensitiveLayout"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((YContentSensitiveLayout)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_YContentSensitiveLayout_type") :
			getString("_UI_YContentSensitiveLayout_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(YContentSensitiveLayout.class)) {
			case YECviewPackage.YCONTENT_SENSITIVE_LAYOUT__SPACING:
			case YECviewPackage.YCONTENT_SENSITIVE_LAYOUT__MARGIN:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTextDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTextAreaDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYNumericDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYDecimalDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTableDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYCheckBoxDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYComboBoxDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYListDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYOptionsGroupDataType()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYBrowserDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYDateTimeDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTreeDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYProgressBarDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYTabSheetDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YEMBEDDABLE__ORPHAN_DATATYPES,
				 ExtDatatypesFactory.eINSTANCE.createYMasterDetailDatatype()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYStrategyLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYBlobUploadComponent()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYCustomDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYI18nComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYIconComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYQuantityTextField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYContentSensitiveLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYRichTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYPrefixedMaskedTextField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYMaskedNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYMaskedDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 YECviewFactory.eINSTANCE.createYPairComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYGridLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYHorizontalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYVerticalLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYTable()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYTree()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYList()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYLabel()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYImage()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYTextField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYBeanReferenceField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYTextArea()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYCheckBox()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYBrowser()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYDateTime()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYDecimalField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYNumericField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYButton()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYSlider()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYToggleButton()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYProgressBar()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYTabSheet()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYMasterDetail()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYFormLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYTextSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYBooleanSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYNumericSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYReferenceSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYPanel()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYSplitPanel()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYSearchPanel()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYEnumOptionsGroup()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYEnumComboBox()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYEnumList()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYCssLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYAbsoluteLayout()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYSuggestTextField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYPasswordField()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYFilteringComponent()));

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YLAYOUT__ELEMENTS,
				 ExtensionModelFactory.eINSTANCE.createYKanban()));
	}

}
