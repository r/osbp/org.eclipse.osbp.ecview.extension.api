/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts;

import java.util.List;

import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;

import org.eclipse.osbp.ecview.extension.api.IFocusingEnhancer;
import org.eclipse.osbp.ecview.extension.editparts.strategies.IFocusingStrategyEditpart;

/**
 * The abstraction for a strategy layout.
 */
public interface IStrategyLayoutEditpart extends IEmbeddableEditpart {

	/**
	 * Returns an unmodifiable list with all suspects contained in the layout.
	 * 
	 * @return elements The suspect editparts contained in this layout.
	 */
	List<ISuspectEditpart> getSuspects();

	/**
	 * Deactivates the info editpart.
	 *
	 * @param info
	 *            the info
	 */
	void deactivate(ILayoutingInfoEditpart info);

	/**
	 * Activates the info editpart.
	 *
	 * @param info
	 *            the info
	 */
	void activate(ILayoutingInfoEditpart info);

	/**
	 * Requests a new layouting for the whole layout.
	 */
	void requestLayouting();

	/**
	 * Calculates the next focused field and sets the focus. The passed source
	 * and target parameter are the UI-Kit specific UI-elements that are
	 * involved in the key stroke event. To support hybrid applications based on
	 * ECView and native UI-Code, the original objects are passed.
	 * <p>
	 * Use {@link IWidgetAssocationsService} to map the given elements to their
	 * model elements.
	 * 
	 * @param source
	 *            - the source element the key stroke listener was registered at
	 * @param target
	 *            - the element the key stroke happened at
	 * @param strategy
	 *            - the strategy that requests focusing
	 */
	void requestFocusing(Object source, Object target,
			IFocusingStrategyEditpart strategy);

	/**
	 * Will reset the current focusing strategies and applies the default one.
	 */
	void resetToDefaultFocusing();

	/**
	 * Requests a refresh of the strategy. Something internally changed. The key
	 * stroke listener need to be removed and registered again.
	 *
	 * @param strategy
	 *            the strategy
	 */
	void requestRefresh(IFocusingStrategyEditpart strategy);

	/**
	 * Find focusing enhancer service.
	 *
	 * @param id
	 *            the id
	 * @return the i focusing enhancer
	 */
	//TODO
	public IFocusingEnhancer findFocusingEnhancerService(String id);
	
	/**
	 * Find default focusing enhancer service.
	 *
	 * @return the i focusing enhancer
	 */
	//TODO
	public IFocusingEnhancer findDefaultFocusingEnhancerService();

}
