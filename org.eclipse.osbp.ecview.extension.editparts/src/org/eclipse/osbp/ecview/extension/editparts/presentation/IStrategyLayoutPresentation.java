/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.presentation;

import org.eclipse.osbp.ecview.core.common.editpart.IEmbeddableEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IKeyStrokeDefinitionEditpart;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.runtime.common.keystroke.KeyStrokeCallback;

/**
 * An presentation abstraction for the StrategyLayout.
 *
 * @param <C>
 *            the generic type
 */
public interface IStrategyLayoutPresentation<C> extends IWidgetPresentation<C> {

	/**
	 * Sets the content editpart.
	 *
	 * @param contentEditpart
	 *            the new content
	 */
	void setContent(IEmbeddableEditpart contentEditpart);

	/**
	 * Registers a keystroke handler for the given callback. If a handler is
	 * already installed, for the given callback, the call will be ignored.
	 *
	 * @param keyStrokeDefEditpart
	 *            the key stroke def editpart
	 * @param callback
	 *            - the callback that should be invoked, if the key stroke was
	 *            pressed.
	 */
	void registerKeyStroke(IKeyStrokeDefinitionEditpart keyStrokeDefEditpart,
			KeyStrokeCallback callback);

	/**
	 * Unregisters the keystroke for the given callback.
	 *
	 * @param callback
	 *            the callback
	 */
	void unregisterKeyStroke(KeyStrokeCallback callback);

}
