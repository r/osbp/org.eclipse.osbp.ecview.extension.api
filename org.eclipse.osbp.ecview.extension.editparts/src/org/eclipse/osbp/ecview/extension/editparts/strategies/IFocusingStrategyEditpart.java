/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.editparts.strategies;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IKeyStrokeDefinitionEditpart;
import org.eclipse.osbp.ecview.core.common.editpart.IViewEditpart;
import org.eclipse.osbp.ecview.core.common.services.IWidgetAssocationsService;

/**
 * Is used to set the focus to a field.
 */
public interface IFocusingStrategyEditpart extends IElementEditpart {

	/**
	 * Calculates the next focused field and sets the focus. The passed source
	 * and target parameter are the UI-Kit specific UI-elements that are
	 * involved in the key stroke event. To support hybrid applications based on
	 * ECView and native UI-Code, the original objects are passed.
	 * <p>
	 * Use {@link IWidgetAssocationsService} to map the given elements to their
	 * model elements.
	 * 
	 * @param source
	 *            - the source element the key stroke listener was registered at
	 * @param target
	 *            - the element the key stroke happened at
	 */
	void focus(Object source, Object target);

	/**
	 * Returns the view edit part of this editpart.<br>
	 * May be <code>null</code> as far as this element and no of its parents are
	 * attached to a view.
	 * 
	 * @return editpart The view editpart
	 */
	IViewEditpart getView();

	/**
	 * Returns the parent that contains this editpart.
	 *
	 * @return the parent
	 */
	IElementEditpart getParent();

	/**
	 * Returns the keystroke definition that will trigger this focusing
	 * strategy.
	 *
	 * @return the key stroke definition
	 */
	IKeyStrokeDefinitionEditpart getKeyStrokeDefinition();

}
