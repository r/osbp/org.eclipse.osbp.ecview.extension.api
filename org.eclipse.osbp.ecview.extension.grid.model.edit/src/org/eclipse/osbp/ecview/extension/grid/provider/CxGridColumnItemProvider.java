/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.provider;


import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelFactory;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridColumnItemProvider extends CxGridGroupableItemProvider {
	
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public CxGridColumnItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPropertyIdPropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
			addLabelI18nKeyPropertyDescriptor(object);
			addEditablePropertyDescriptor(object);
			addConverterPropertyDescriptor(object);
			addRendererPropertyDescriptor(object);
			addEditorFieldPropertyDescriptor(object);
			addHeaderCaptionPropertyDescriptor(object);
			addHeaderCaptionI18nKeyPropertyDescriptor(object);
			addExpandRatioPropertyDescriptor(object);
			addHiddenPropertyDescriptor(object);
			addHideablePropertyDescriptor(object);
			addSortablePropertyDescriptor(object);
			addPropertyPathPropertyDescriptor(object);
			addWidthPropertyDescriptor(object);
			addMinWidthPixelsPropertyDescriptor(object);
			addMaxWidthPixelsPropertyDescriptor(object);
			addUsedInMetaCellsPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
			addEditsDtoPropertyDescriptor(object);
			addFilterPropertyPathForEditsDtoPropertyDescriptor(object);
			addSourceTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Property Id feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addPropertyIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_propertyId_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_propertyId_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__PROPERTY_ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_label_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_label_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__LABEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Label I1 8n Key feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addLabelI18nKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_labelI18nKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_labelI18nKey_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__LABEL_I1_8N_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editable feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEditablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_editable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_editable_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__EDITABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Converter feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addConverterPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_converter_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_converter_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__CONVERTER,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Renderer feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addRendererPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_renderer_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_renderer_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__RENDERER,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editor Field feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEditorFieldPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_editorField_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_editorField_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__EDITOR_FIELD,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Header Caption feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHeaderCaptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_headerCaption_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_headerCaption_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__HEADER_CAPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Header Caption I1 8n Key feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHeaderCaptionI18nKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_headerCaptionI18nKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_headerCaptionI18nKey_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__HEADER_CAPTION_I1_8N_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Expand Ratio feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addExpandRatioPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_expandRatio_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_expandRatio_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__EXPAND_RATIO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Hidden feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHiddenPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_hidden_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_hidden_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__HIDDEN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Hideable feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHideablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_hideable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_hideable_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__HIDEABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sortable feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addSortablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_sortable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_sortable_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__SORTABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Property Path feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addPropertyPathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_propertyPath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_propertyPath_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__PROPERTY_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Width feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addWidthPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_width_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_width_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__WIDTH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Min Width Pixels feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addMinWidthPixelsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_minWidthPixels_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_minWidthPixels_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__MIN_WIDTH_PIXELS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max Width Pixels feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addMaxWidthPixelsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_maxWidthPixels_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_maxWidthPixels_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__MAX_WIDTH_PIXELS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Used In Meta Cells feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addUsedInMetaCellsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_usedInMetaCells_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_usedInMetaCells_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__USED_IN_META_CELLS,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_type_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_typeQualifiedName_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Edits Dto feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEditsDtoPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_editsDto_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_editsDto_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__EDITS_DTO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Filter Property Path For Edits Dto feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFilterPropertyPathForEditsDtoPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_filterPropertyPathForEditsDto_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_filterPropertyPathForEditsDto_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__FILTER_PROPERTY_PATH_FOR_EDITS_DTO,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridColumn_sourceType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridColumn_sourceType_feature", "_UI_CxGridColumn_type"),
				 CxGridPackage.Literals.CX_GRID_COLUMN__SOURCE_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CxGridPackage.Literals.CX_GRID_COLUMN__SEARCH_FIELD);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CxGridColumn.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 *
	 * @param object
	 *            the object
	 * @return the image
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CxGridColumn"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CxGridColumn)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CxGridColumn_type") :
			getString("_UI_CxGridColumn_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CxGridColumn.class)) {
			case CxGridPackage.CX_GRID_COLUMN__PROPERTY_ID:
			case CxGridPackage.CX_GRID_COLUMN__LABEL:
			case CxGridPackage.CX_GRID_COLUMN__LABEL_I1_8N_KEY:
			case CxGridPackage.CX_GRID_COLUMN__EDITABLE:
			case CxGridPackage.CX_GRID_COLUMN__HEADER_CAPTION:
			case CxGridPackage.CX_GRID_COLUMN__HEADER_CAPTION_I1_8N_KEY:
			case CxGridPackage.CX_GRID_COLUMN__EXPAND_RATIO:
			case CxGridPackage.CX_GRID_COLUMN__HIDDEN:
			case CxGridPackage.CX_GRID_COLUMN__HIDEABLE:
			case CxGridPackage.CX_GRID_COLUMN__SORTABLE:
			case CxGridPackage.CX_GRID_COLUMN__PROPERTY_PATH:
			case CxGridPackage.CX_GRID_COLUMN__WIDTH:
			case CxGridPackage.CX_GRID_COLUMN__MIN_WIDTH_PIXELS:
			case CxGridPackage.CX_GRID_COLUMN__MAX_WIDTH_PIXELS:
			case CxGridPackage.CX_GRID_COLUMN__TYPE:
			case CxGridPackage.CX_GRID_COLUMN__TYPE_QUALIFIED_NAME:
			case CxGridPackage.CX_GRID_COLUMN__EDITS_DTO:
			case CxGridPackage.CX_GRID_COLUMN__FILTER_PROPERTY_PATH_FOR_EDITS_DTO:
			case CxGridPackage.CX_GRID_COLUMN__SOURCE_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CxGridPackage.CX_GRID_COLUMN__SEARCH_FIELD:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID_COLUMN__SEARCH_FIELD,
				 ExtensionModelFactory.eINSTANCE.createYTextSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID_COLUMN__SEARCH_FIELD,
				 ExtensionModelFactory.eINSTANCE.createYBooleanSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID_COLUMN__SEARCH_FIELD,
				 ExtensionModelFactory.eINSTANCE.createYNumericSearchField()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID_COLUMN__SEARCH_FIELD,
				 ExtensionModelFactory.eINSTANCE.createYReferenceSearchField()));
	}

}
