/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.provider;


import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridQuantityRendererItemProvider extends CxGridRendererItemProvider {
	
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public CxGridQuantityRendererItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addValuePropertyPathPropertyDescriptor(object);
			addUomPropertyPathPropertyDescriptor(object);
			addNullRepresentationPropertyDescriptor(object);
			addHtmlPatternPropertyDescriptor(object);
			addNumberFormatPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Value Property Path feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addValuePropertyPathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridQuantityRenderer_valuePropertyPath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridQuantityRenderer_valuePropertyPath_feature", "_UI_CxGridQuantityRenderer_type"),
				 CxGridRendererPackage.Literals.CX_GRID_QUANTITY_RENDERER__VALUE_PROPERTY_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Uom Property Path feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addUomPropertyPathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridQuantityRenderer_uomPropertyPath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridQuantityRenderer_uomPropertyPath_feature", "_UI_CxGridQuantityRenderer_type"),
				 CxGridRendererPackage.Literals.CX_GRID_QUANTITY_RENDERER__UOM_PROPERTY_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Null Representation feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addNullRepresentationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridQuantityRenderer_nullRepresentation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridQuantityRenderer_nullRepresentation_feature", "_UI_CxGridQuantityRenderer_type"),
				 CxGridRendererPackage.Literals.CX_GRID_QUANTITY_RENDERER__NULL_REPRESENTATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Html Pattern feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHtmlPatternPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridQuantityRenderer_htmlPattern_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridQuantityRenderer_htmlPattern_feature", "_UI_CxGridQuantityRenderer_type"),
				 CxGridRendererPackage.Literals.CX_GRID_QUANTITY_RENDERER__HTML_PATTERN,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Format feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addNumberFormatPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGridQuantityRenderer_numberFormat_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGridQuantityRenderer_numberFormat_feature", "_UI_CxGridQuantityRenderer_type"),
				 CxGridRendererPackage.Literals.CX_GRID_QUANTITY_RENDERER__NUMBER_FORMAT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns CxGridQuantityRenderer.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CxGridQuantityRenderer"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CxGridQuantityRenderer)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CxGridQuantityRenderer_type") :
			getString("_UI_CxGridQuantityRenderer_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CxGridQuantityRenderer.class)) {
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER__VALUE_PROPERTY_PATH:
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER__UOM_PROPERTY_PATH:
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER__NULL_REPRESENTATION:
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER__HTML_PATTERN:
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER__NUMBER_FORMAT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
