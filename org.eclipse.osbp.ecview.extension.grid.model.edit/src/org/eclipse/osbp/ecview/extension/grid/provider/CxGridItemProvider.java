/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.provider;


import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.provider.YInputItemProvider;

/**
 * This is the item provider adapter for a {@link org.eclipse.osbp.ecview.extension.grid.CxGrid} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridItemProvider extends YInputItemProvider {
	
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param adapterFactory
	 *            the adapter factory
	 * @generated
	 */
	public CxGridItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @return the property descriptors
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCollectionBindingEndpointPropertyDescriptor(object);
			addSelectionBindingEndpointPropertyDescriptor(object);
			addMultiSelectionBindingEndpointPropertyDescriptor(object);
			addUseBeanServicePropertyDescriptor(object);
			addSelectionTypePropertyDescriptor(object);
			addSelectionEventTopicPropertyDescriptor(object);
			addSelectionPropertyDescriptor(object);
			addMultiSelectionPropertyDescriptor(object);
			addCollectionPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addEmfNsURIPropertyDescriptor(object);
			addTypeQualifiedNamePropertyDescriptor(object);
			addColumnReorderingAllowedPropertyDescriptor(object);
			addFilteringVisiblePropertyDescriptor(object);
			addCustomFiltersPropertyDescriptor(object);
			addHeaderVisiblePropertyDescriptor(object);
			addFooterVisiblePropertyDescriptor(object);
			addEditorEnabledPropertyDescriptor(object);
			addEditorCancelI18nLabelKeyPropertyDescriptor(object);
			addEditorSaveI18nLabelKeyPropertyDescriptor(object);
			addEditorSavedPropertyDescriptor(object);
			addSetLastRefreshTimePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Collection Binding Endpoint feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YCollectionBindable_collectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YCollectionBindable_collectionBindingEndpoint_feature", "_UI_YCollectionBindable_type"),
				 CoreModelPackage.Literals.YCOLLECTION_BINDABLE__COLLECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Binding Endpoint feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YSelectionBindable_selectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YSelectionBindable_selectionBindingEndpoint_feature", "_UI_YSelectionBindable_type"),
				 CoreModelPackage.Literals.YSELECTION_BINDABLE__SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Multi Selection Binding Endpoint feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiSelectionBindingEndpointPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YMultiSelectionBindable_multiSelectionBindingEndpoint_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YMultiSelectionBindable_multiSelectionBindingEndpoint_feature", "_UI_YMultiSelectionBindable_type"),
				 CoreModelPackage.Literals.YMULTI_SELECTION_BINDABLE__MULTI_SELECTION_BINDING_ENDPOINT,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Use Bean Service feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addUseBeanServicePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_YBeanServiceConsumer_useBeanService_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_YBeanServiceConsumer_useBeanService_feature", "_UI_YBeanServiceConsumer_type"),
				 ExtensionModelPackage.Literals.YBEAN_SERVICE_CONSUMER__USE_BEAN_SERVICE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Type feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addSelectionTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_selectionType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_selectionType_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__SELECTION_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection Event Topic feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelectionEventTopicPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_selectionEventTopic_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_selectionEventTopic_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__SELECTION_EVENT_TOPIC,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Selection feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addSelectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_selection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_selection_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__SELECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Multi Selection feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addMultiSelectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_multiSelection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_multiSelection_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__MULTI_SELECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Collection feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addCollectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_collection_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_collection_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__COLLECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_type_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_type_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Emf Ns URI feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEmfNsURIPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_emfNsURI_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_emfNsURI_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__EMF_NS_URI,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Type Qualified Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addTypeQualifiedNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_typeQualifiedName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_typeQualifiedName_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__TYPE_QUALIFIED_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Column Reordering Allowed feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumnReorderingAllowedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_columnReorderingAllowed_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_columnReorderingAllowed_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__COLUMN_REORDERING_ALLOWED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Filtering Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addFilteringVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_filteringVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_filteringVisible_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__FILTERING_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Custom Filters feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addCustomFiltersPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_customFilters_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_customFilters_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__CUSTOM_FILTERS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Header Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addHeaderVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_headerVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_headerVisible_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__HEADER_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Footer Visible feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addFooterVisiblePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_footerVisible_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_footerVisible_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__FOOTER_VISIBLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editor Enabled feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEditorEnabledPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_editorEnabled_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_editorEnabled_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__EDITOR_ENABLED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editor Cancel I1 8n Label Key feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEditorCancelI18nLabelKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_editorCancelI18nLabelKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_editorCancelI18nLabelKey_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__EDITOR_CANCEL_I1_8N_LABEL_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editor Save I1 8n Label Key feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEditorSaveI18nLabelKeyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_editorSaveI18nLabelKey_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_editorSaveI18nLabelKey_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__EDITOR_SAVE_I1_8N_LABEL_KEY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Editor Saved feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param object
	 *            the object
	 * @generated
	 */
	protected void addEditorSavedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_editorSaved_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_editorSaved_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__EDITOR_SAVED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Set Last Refresh Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSetLastRefreshTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_CxGrid_setLastRefreshTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_CxGrid_setLastRefreshTime_feature", "_UI_CxGrid_type"),
				 CxGridPackage.Literals.CX_GRID__SET_LAST_REFRESH_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(CxGridPackage.Literals.CX_GRID__COLUMNS);
			childrenFeatures.add(CxGridPackage.Literals.CX_GRID__SORT_ORDER);
			childrenFeatures.add(CxGridPackage.Literals.CX_GRID__CELL_STYLE_GENERATOR);
			childrenFeatures.add(CxGridPackage.Literals.CX_GRID__HEADERS);
			childrenFeatures.add(CxGridPackage.Literals.CX_GRID__FOOTERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param object
	 *            the object
	 * @param child
	 *            the child
	 * @return the child feature
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns CxGrid.gif.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/CxGrid"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((CxGrid)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_CxGrid_type") :
			getString("_UI_CxGrid_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(CxGrid.class)) {
			case CxGridPackage.CX_GRID__USE_BEAN_SERVICE:
			case CxGridPackage.CX_GRID__SELECTION_TYPE:
			case CxGridPackage.CX_GRID__SELECTION_EVENT_TOPIC:
			case CxGridPackage.CX_GRID__SELECTION:
			case CxGridPackage.CX_GRID__MULTI_SELECTION:
			case CxGridPackage.CX_GRID__COLLECTION:
			case CxGridPackage.CX_GRID__TYPE:
			case CxGridPackage.CX_GRID__EMF_NS_URI:
			case CxGridPackage.CX_GRID__TYPE_QUALIFIED_NAME:
			case CxGridPackage.CX_GRID__COLUMN_REORDERING_ALLOWED:
			case CxGridPackage.CX_GRID__FILTERING_VISIBLE:
			case CxGridPackage.CX_GRID__CUSTOM_FILTERS:
			case CxGridPackage.CX_GRID__HEADER_VISIBLE:
			case CxGridPackage.CX_GRID__FOOTER_VISIBLE:
			case CxGridPackage.CX_GRID__EDITOR_ENABLED:
			case CxGridPackage.CX_GRID__EDITOR_CANCEL_I1_8N_LABEL_KEY:
			case CxGridPackage.CX_GRID__EDITOR_SAVE_I1_8N_LABEL_KEY:
			case CxGridPackage.CX_GRID__EDITOR_SAVED:
			case CxGridPackage.CX_GRID__SET_LAST_REFRESH_TIME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case CxGridPackage.CX_GRID__COLUMNS:
			case CxGridPackage.CX_GRID__SORT_ORDER:
			case CxGridPackage.CX_GRID__CELL_STYLE_GENERATOR:
			case CxGridPackage.CX_GRID__HEADERS:
			case CxGridPackage.CX_GRID__FOOTERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param newChildDescriptors
	 *            the new child descriptors
	 * @param object
	 *            the object
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(CoreModelPackage.Literals.YFIELD__CONVERTER,
				 CxGridRendererFactory.eINSTANCE.createCxGridNestedConverter()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID__COLUMNS,
				 CxGridFactory.eINSTANCE.createCxGridColumn()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID__SORT_ORDER,
				 CxGridFactory.eINSTANCE.createCxGridSortable()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID__CELL_STYLE_GENERATOR,
				 CxGridFactory.eINSTANCE.createCxGridDelegateCellStyleGenerator()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID__HEADERS,
				 CxGridFactory.eINSTANCE.createCxGridHeaderRow()));

		newChildDescriptors.add
			(createChildParameter
				(CxGridPackage.Literals.CX_GRID__FOOTERS,
				 CxGridFactory.eINSTANCE.createCxGridFooterRow()));
	}

	/**
	 * This returns the label text for
	 * {@link org.eclipse.emf.edit.command.CreateChildCommand}. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param owner
	 *            the owner
	 * @param feature
	 *            the feature
	 * @param child
	 *            the child
	 * @param selection
	 *            the selection
	 * @return the creates the child text
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == CoreModelPackage.Literals.YFIELD__VALIDATORS ||
			childFeature == CoreModelPackage.Literals.YFIELD__INTERNAL_VALIDATORS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
