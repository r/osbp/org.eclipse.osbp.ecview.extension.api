/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YField;
import org.eclipse.osbp.ecview.core.common.model.core.YHelperLayoutProvider;
import org.eclipse.osbp.ecview.core.extension.model.extension.YSearchField;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyId <em>Property Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditable <em>Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getConverter <em>Converter</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getRenderer <em>Renderer</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getEditorField <em>Editor Field</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSearchField <em>Search Field</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaption <em>Header Caption</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaptionI18nKey <em>Header Caption I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getExpandRatio <em>Expand Ratio</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHidden <em>Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHideable <em>Hideable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isSortable <em>Sortable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyPath <em>Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getWidth <em>Width</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMinWidthPixels <em>Min Width Pixels</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMaxWidthPixels <em>Max Width Pixels</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getUsedInMetaCells <em>Used In Meta Cells</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditsDto <em>Edits Dto</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getFilterPropertyPathForEditsDto <em>Filter Property Path For Edits Dto</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSourceType <em>Source Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn()
 * @model
 * @generated
 */
public interface CxGridColumn extends CxGridGroupable, YHelperLayoutProvider {
	
	/**
	 * Returns the value of the '<em><b>Property Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Id</em>' attribute.
	 * @see #setPropertyId(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_PropertyId()
	 * @model
	 * @generated
	 */
	String getPropertyId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyId <em>Property Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Id</em>' attribute.
	 * @see #getPropertyId()
	 * @generated
	 */
	void setPropertyId(String value);

	/**
	 * Returns the value of the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Converter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Converter</em>' containment reference.
	 * @see #setConverter(YConverter)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Converter()
	 * @model containment="true"
	 * @generated
	 */
	YConverter getConverter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getConverter <em>Converter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Converter</em>' containment reference.
	 * @see #getConverter()
	 * @generated
	 */
	void setConverter(YConverter value);

	/**
	 * Returns the value of the '<em><b>Renderer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Renderer</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Renderer</em>' containment reference.
	 * @see #setRenderer(CxGridRenderer)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Renderer()
	 * @model containment="true"
	 * @generated
	 */
	CxGridRenderer getRenderer();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getRenderer <em>Renderer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Renderer</em>' containment reference.
	 * @see #getRenderer()
	 * @generated
	 */
	void setRenderer(CxGridRenderer value);

	/**
	 * Returns the value of the '<em><b>Editor Field</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editor Field</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editor Field</em>' containment reference.
	 * @see #setEditorField(YField)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_EditorField()
	 * @model containment="true"
	 * @generated
	 */
	YField getEditorField();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getEditorField <em>Editor Field</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editor Field</em>' containment reference.
	 * @see #getEditorField()
	 * @generated
	 */
	void setEditorField(YField value);

	/**
	 * Returns the value of the '<em><b>Search Field</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Search Field</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Search Field</em>' containment reference.
	 * @see #setSearchField(YSearchField)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_SearchField()
	 * @model containment="true"
	 * @generated
	 */
	YSearchField getSearchField();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSearchField <em>Search Field</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Search Field</em>' containment reference.
	 * @see #getSearchField()
	 * @generated
	 */
	void setSearchField(YSearchField value);

	/**
	 * Returns the value of the '<em><b>Header Caption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Caption</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Caption</em>' attribute.
	 * @see #setHeaderCaption(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_HeaderCaption()
	 * @model
	 * @generated
	 */
	String getHeaderCaption();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaption <em>Header Caption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Caption</em>' attribute.
	 * @see #getHeaderCaption()
	 * @generated
	 */
	void setHeaderCaption(String value);

	/**
	 * Returns the value of the '<em><b>Header Caption I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Caption I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Caption I1 8n Key</em>' attribute.
	 * @see #setHeaderCaptionI18nKey(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_HeaderCaptionI18nKey()
	 * @model
	 * @generated
	 */
	String getHeaderCaptionI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaptionI18nKey <em>Header Caption I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Caption I1 8n Key</em>' attribute.
	 * @see #getHeaderCaptionI18nKey()
	 * @generated
	 */
	void setHeaderCaptionI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Expand Ratio</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expand Ratio</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expand Ratio</em>' attribute.
	 * @see #setExpandRatio(int)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_ExpandRatio()
	 * @model default="-1"
	 * @generated
	 */
	int getExpandRatio();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getExpandRatio <em>Expand Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expand Ratio</em>' attribute.
	 * @see #getExpandRatio()
	 * @generated
	 */
	void setExpandRatio(int value);

	/**
	 * Returns the value of the '<em><b>Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hidden</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hidden</em>' attribute.
	 * @see #setHidden(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Hidden()
	 * @model
	 * @generated
	 */
	boolean isHidden();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHidden <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hidden</em>' attribute.
	 * @see #isHidden()
	 * @generated
	 */
	void setHidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Hideable</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hideable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hideable</em>' attribute.
	 * @see #setHideable(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Hideable()
	 * @model default="true"
	 * @generated
	 */
	boolean isHideable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHideable <em>Hideable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hideable</em>' attribute.
	 * @see #isHideable()
	 * @generated
	 */
	void setHideable(boolean value);

	/**
	 * Returns the value of the '<em><b>Sortable</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sortable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sortable</em>' attribute.
	 * @see #setSortable(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Sortable()
	 * @model default="true"
	 * @generated
	 */
	boolean isSortable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isSortable <em>Sortable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sortable</em>' attribute.
	 * @see #isSortable()
	 * @generated
	 */
	void setSortable(boolean value);

	/**
	 * Returns the value of the '<em><b>Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Path</em>' attribute.
	 * @see #setPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_PropertyPath()
	 * @model
	 * @generated
	 */
	String getPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyPath <em>Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Path</em>' attribute.
	 * @see #getPropertyPath()
	 * @generated
	 */
	void setPropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Width()
	 * @model default="-1"
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label I1 8n Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #setLabelI18nKey(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_LabelI18nKey()
	 * @model
	 * @generated
	 */
	String getLabelI18nKey();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabelI18nKey <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label I1 8n Key</em>' attribute.
	 * @see #getLabelI18nKey()
	 * @generated
	 */
	void setLabelI18nKey(String value);

	/**
	 * Returns the value of the '<em><b>Editable</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editable</em>' attribute.
	 * @see #setEditable(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Editable()
	 * @model default="false"
	 * @generated
	 */
	boolean isEditable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditable <em>Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editable</em>' attribute.
	 * @see #isEditable()
	 * @generated
	 */
	void setEditable(boolean value);

	/**
	 * Returns the value of the '<em><b>Min Width Pixels</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Width Pixels</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Width Pixels</em>' attribute.
	 * @see #setMinWidthPixels(int)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_MinWidthPixels()
	 * @model default="-1"
	 * @generated
	 */
	int getMinWidthPixels();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMinWidthPixels <em>Min Width Pixels</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Width Pixels</em>' attribute.
	 * @see #getMinWidthPixels()
	 * @generated
	 */
	void setMinWidthPixels(int value);

	/**
	 * Returns the value of the '<em><b>Max Width Pixels</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Width Pixels</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Width Pixels</em>' attribute.
	 * @see #setMaxWidthPixels(int)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_MaxWidthPixels()
	 * @model default="-1"
	 * @generated
	 */
	int getMaxWidthPixels();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMaxWidthPixels <em>Max Width Pixels</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Width Pixels</em>' attribute.
	 * @see #getMaxWidthPixels()
	 * @generated
	 */
	void setMaxWidthPixels(int value);

	/**
	 * Returns the value of the '<em><b>Used In Meta Cells</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell}.
	 * It is bidirectional and its opposite is '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Used In Meta Cells</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Used In Meta Cells</em>' reference list.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_UsedInMetaCells()
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	EList<CxGridMetaCell> getUsedInMetaCells();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Class)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_Type()
	 * @model
	 * @generated
	 */
	Class<?> getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #setTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_TypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getTypeQualifiedName <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #getTypeQualifiedName()
	 * @generated
	 */
	void setTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Edits Dto</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Edits Dto</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Edits Dto</em>' attribute.
	 * @see #setEditsDto(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_EditsDto()
	 * @model
	 * @generated
	 */
	boolean isEditsDto();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditsDto <em>Edits Dto</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Edits Dto</em>' attribute.
	 * @see #isEditsDto()
	 * @generated
	 */
	void setEditsDto(boolean value);

	/**
	 * Returns the value of the '<em><b>Filter Property Path For Edits Dto</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter Property Path For Edits Dto</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter Property Path For Edits Dto</em>' attribute.
	 * @see #setFilterPropertyPathForEditsDto(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_FilterPropertyPathForEditsDto()
	 * @model
	 * @generated
	 */
	String getFilterPropertyPathForEditsDto();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getFilterPropertyPathForEditsDto <em>Filter Property Path For Edits Dto</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter Property Path For Edits Dto</em>' attribute.
	 * @see #getFilterPropertyPathForEditsDto()
	 * @generated
	 */
	void setFilterPropertyPathForEditsDto(String value);

	/**
	 * Returns the value of the '<em><b>Source Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Type</em>' attribute.
	 * @see #setSourceType(Class)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridColumn_SourceType()
	 * @model
	 * @generated
	 */
	Class<?> getSourceType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSourceType <em>Source Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Type</em>' attribute.
	 * @see #getSourceType()
	 * @generated
	 */
	void setSourceType(Class<?> value);

}