/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Progress Bar Renderer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer#getMaxValue <em>Max Value</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridProgressBarRenderer()
 * @model
 * @generated
 */
public interface CxGridProgressBarRenderer extends CxGridRenderer {
	
	/**
	 * Returns the value of the '<em><b>Max Value</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Value</em>' attribute.
	 * @see #setMaxValue(double)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridProgressBarRenderer_MaxValue()
	 * @model default="1"
	 * @generated
	 */
	double getMaxValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer#getMaxValue <em>Max Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Value</em>' attribute.
	 * @see #getMaxValue()
	 * @generated
	 */
	void setMaxValue(double value);

}