/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.util;

import org.eclipse.osbp.ecview.extension.grid.CxGridProvider;

import org.eclipse.osbp.ecview.extension.grid.renderer.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage
 * @generated
 */
public class CxGridRendererSwitch<T> extends Switch<T> {
	
	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected static CxGridRendererPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridRendererSwitch() {
		if (modelPackage == null) {
			modelPackage = CxGridRendererPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param ePackage
	 *            the e package
	 * @return whether this is a switch for the given package.
	 * @parameter ePackage the package in question.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case CxGridRendererPackage.CX_GRID_RENDERER: {
				CxGridRenderer cxGridRenderer = (CxGridRenderer)theEObject;
				T result = caseCxGridRenderer(cxGridRenderer);
				if (result == null) result = caseYElement(cxGridRenderer);
				if (result == null) result = caseCxGridProvider(cxGridRenderer);
				if (result == null) result = caseYTaggable(cxGridRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_DELEGATE_RENDERER: {
				CxGridDelegateRenderer cxGridDelegateRenderer = (CxGridDelegateRenderer)theEObject;
				T result = caseCxGridDelegateRenderer(cxGridDelegateRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridDelegateRenderer);
				if (result == null) result = caseYElement(cxGridDelegateRenderer);
				if (result == null) result = caseCxGridProvider(cxGridDelegateRenderer);
				if (result == null) result = caseYTaggable(cxGridDelegateRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_DATE_RENDERER: {
				CxGridDateRenderer cxGridDateRenderer = (CxGridDateRenderer)theEObject;
				T result = caseCxGridDateRenderer(cxGridDateRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridDateRenderer);
				if (result == null) result = caseYElement(cxGridDateRenderer);
				if (result == null) result = caseCxGridProvider(cxGridDateRenderer);
				if (result == null) result = caseYTaggable(cxGridDateRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_HTML_RENDERER: {
				CxGridHtmlRenderer cxGridHtmlRenderer = (CxGridHtmlRenderer)theEObject;
				T result = caseCxGridHtmlRenderer(cxGridHtmlRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridHtmlRenderer);
				if (result == null) result = caseYElement(cxGridHtmlRenderer);
				if (result == null) result = caseCxGridProvider(cxGridHtmlRenderer);
				if (result == null) result = caseYTaggable(cxGridHtmlRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_NUMBER_RENDERER: {
				CxGridNumberRenderer cxGridNumberRenderer = (CxGridNumberRenderer)theEObject;
				T result = caseCxGridNumberRenderer(cxGridNumberRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridNumberRenderer);
				if (result == null) result = caseYElement(cxGridNumberRenderer);
				if (result == null) result = caseCxGridProvider(cxGridNumberRenderer);
				if (result == null) result = caseYTaggable(cxGridNumberRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_PROGRESS_BAR_RENDERER: {
				CxGridProgressBarRenderer cxGridProgressBarRenderer = (CxGridProgressBarRenderer)theEObject;
				T result = caseCxGridProgressBarRenderer(cxGridProgressBarRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridProgressBarRenderer);
				if (result == null) result = caseYElement(cxGridProgressBarRenderer);
				if (result == null) result = caseCxGridProvider(cxGridProgressBarRenderer);
				if (result == null) result = caseYTaggable(cxGridProgressBarRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_TEXT_RENDERER: {
				CxGridTextRenderer cxGridTextRenderer = (CxGridTextRenderer)theEObject;
				T result = caseCxGridTextRenderer(cxGridTextRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridTextRenderer);
				if (result == null) result = caseYElement(cxGridTextRenderer);
				if (result == null) result = caseCxGridProvider(cxGridTextRenderer);
				if (result == null) result = caseYTaggable(cxGridTextRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_BUTTON_RENDERER: {
				CxGridButtonRenderer cxGridButtonRenderer = (CxGridButtonRenderer)theEObject;
				T result = caseCxGridButtonRenderer(cxGridButtonRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridButtonRenderer);
				if (result == null) result = caseYElement(cxGridButtonRenderer);
				if (result == null) result = caseCxGridProvider(cxGridButtonRenderer);
				if (result == null) result = caseYTaggable(cxGridButtonRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_BLOB_IMAGE_RENDERER: {
				CxGridBlobImageRenderer cxGridBlobImageRenderer = (CxGridBlobImageRenderer)theEObject;
				T result = caseCxGridBlobImageRenderer(cxGridBlobImageRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridBlobImageRenderer);
				if (result == null) result = caseYElement(cxGridBlobImageRenderer);
				if (result == null) result = caseCxGridProvider(cxGridBlobImageRenderer);
				if (result == null) result = caseYTaggable(cxGridBlobImageRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_IMAGE_RENDERER: {
				CxGridImageRenderer cxGridImageRenderer = (CxGridImageRenderer)theEObject;
				T result = caseCxGridImageRenderer(cxGridImageRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridImageRenderer);
				if (result == null) result = caseYElement(cxGridImageRenderer);
				if (result == null) result = caseCxGridProvider(cxGridImageRenderer);
				if (result == null) result = caseYTaggable(cxGridImageRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_RENDERER_CLICK_EVENT: {
				CxGridRendererClickEvent cxGridRendererClickEvent = (CxGridRendererClickEvent)theEObject;
				T result = caseCxGridRendererClickEvent(cxGridRendererClickEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_BOOLEAN_RENDERER: {
				CxGridBooleanRenderer cxGridBooleanRenderer = (CxGridBooleanRenderer)theEObject;
				T result = caseCxGridBooleanRenderer(cxGridBooleanRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridBooleanRenderer);
				if (result == null) result = caseYElement(cxGridBooleanRenderer);
				if (result == null) result = caseCxGridProvider(cxGridBooleanRenderer);
				if (result == null) result = caseYTaggable(cxGridBooleanRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_QUANTITY_RENDERER: {
				CxGridQuantityRenderer cxGridQuantityRenderer = (CxGridQuantityRenderer)theEObject;
				T result = caseCxGridQuantityRenderer(cxGridQuantityRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridQuantityRenderer);
				if (result == null) result = caseYElement(cxGridQuantityRenderer);
				if (result == null) result = caseCxGridProvider(cxGridQuantityRenderer);
				if (result == null) result = caseYTaggable(cxGridQuantityRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_PRICE_RENDERER: {
				CxGridPriceRenderer cxGridPriceRenderer = (CxGridPriceRenderer)theEObject;
				T result = caseCxGridPriceRenderer(cxGridPriceRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridPriceRenderer);
				if (result == null) result = caseYElement(cxGridPriceRenderer);
				if (result == null) result = caseCxGridProvider(cxGridPriceRenderer);
				if (result == null) result = caseYTaggable(cxGridPriceRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_INDICATOR_RENDERER: {
				CxGridIndicatorRenderer cxGridIndicatorRenderer = (CxGridIndicatorRenderer)theEObject;
				T result = caseCxGridIndicatorRenderer(cxGridIndicatorRenderer);
				if (result == null) result = caseCxGridRenderer(cxGridIndicatorRenderer);
				if (result == null) result = caseYElement(cxGridIndicatorRenderer);
				if (result == null) result = caseCxGridProvider(cxGridIndicatorRenderer);
				if (result == null) result = caseYTaggable(cxGridIndicatorRenderer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER: {
				CxGridNestedConverter cxGridNestedConverter = (CxGridNestedConverter)theEObject;
				T result = caseCxGridNestedConverter(cxGridNestedConverter);
				if (result == null) result = caseYConverter(cxGridNestedConverter);
				if (result == null) result = caseYElement(cxGridNestedConverter);
				if (result == null) result = caseYTaggable(cxGridNestedConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridRenderer(CxGridRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Delegate Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Delegate Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridDelegateRenderer(CxGridDelegateRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Date Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Date Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridDateRenderer(CxGridDateRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Html Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Html Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridHtmlRenderer(CxGridHtmlRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Number Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Number Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridNumberRenderer(CxGridNumberRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Progress Bar Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Progress Bar Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridProgressBarRenderer(CxGridProgressBarRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Text Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Text Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridTextRenderer(CxGridTextRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Button Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Button Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridButtonRenderer(CxGridButtonRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Blob Image Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Blob Image Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridBlobImageRenderer(CxGridBlobImageRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Image Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Image Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridImageRenderer(CxGridImageRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Click Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Click Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridRendererClickEvent(CxGridRendererClickEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Boolean Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Boolean Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridBooleanRenderer(CxGridBooleanRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Quantity Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Quantity Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridQuantityRenderer(CxGridQuantityRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Price Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Price Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridPriceRenderer(CxGridPriceRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Indicator Renderer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Indicator Renderer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridIndicatorRenderer(CxGridIndicatorRenderer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cx Grid Nested Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cx Grid Nested Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridNestedConverter(CxGridNestedConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYTaggable(YTaggable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYElement(YElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCxGridProvider(CxGridProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YConverter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YConverter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYConverter(YConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

}