/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridFactory
 * @model kind="package"
 * @generated
 */
public interface CxGridPackage extends EPackage {
	
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "grid";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.de/ecview/v1/extension/grid";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "grid";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CxGridPackage eINSTANCE = org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridImpl <em>Cx Grid</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGrid()
	 * @generated
	 */
	int CX_GRID = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__TAGS = ExtensionModelPackage.YINPUT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__ID = ExtensionModelPackage.YINPUT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__NAME = ExtensionModelPackage.YINPUT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__PROPERTIES = ExtensionModelPackage.YINPUT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Css Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__CSS_CLASS = ExtensionModelPackage.YINPUT__CSS_CLASS;

	/**
	 * The feature id for the '<em><b>Css ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__CSS_ID = ExtensionModelPackage.YINPUT__CSS_ID;

	/**
	 * The feature id for the '<em><b>Initial Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__INITIAL_VISIBLE = ExtensionModelPackage.YINPUT__INITIAL_VISIBLE;

	/**
	 * The feature id for the '<em><b>Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__VISIBLE = ExtensionModelPackage.YINPUT__VISIBLE;

	/**
	 * The feature id for the '<em><b>Authorization Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__AUTHORIZATION_GROUP = ExtensionModelPackage.YINPUT__AUTHORIZATION_GROUP;

	/**
	 * The feature id for the '<em><b>Authorization Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__AUTHORIZATION_ID = ExtensionModelPackage.YINPUT__AUTHORIZATION_ID;

	/**
	 * The feature id for the '<em><b>Orphan Datatypes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__ORPHAN_DATATYPES = ExtensionModelPackage.YINPUT__ORPHAN_DATATYPES;

	/**
	 * The feature id for the '<em><b>Orphan Datadescriptions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__ORPHAN_DATADESCRIPTIONS = ExtensionModelPackage.YINPUT__ORPHAN_DATADESCRIPTIONS;

	/**
	 * The feature id for the '<em><b>Memento Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__MEMENTO_ENABLED = ExtensionModelPackage.YINPUT__MEMENTO_ENABLED;

	/**
	 * The feature id for the '<em><b>Memento Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__MEMENTO_ID = ExtensionModelPackage.YINPUT__MEMENTO_ID;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__LABEL = ExtensionModelPackage.YINPUT__LABEL;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__LABEL_I1_8N_KEY = ExtensionModelPackage.YINPUT__LABEL_I1_8N_KEY;

	/**
	 * The feature id for the '<em><b>Last Context Click</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__LAST_CONTEXT_CLICK = ExtensionModelPackage.YINPUT__LAST_CONTEXT_CLICK;

	/**
	 * The feature id for the '<em><b>Readonly</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__READONLY = ExtensionModelPackage.YINPUT__READONLY;

	/**
	 * The feature id for the '<em><b>Initial Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__INITIAL_EDITABLE = ExtensionModelPackage.YINPUT__INITIAL_EDITABLE;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__EDITABLE = ExtensionModelPackage.YINPUT__EDITABLE;

	/**
	 * The feature id for the '<em><b>Initial Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__INITIAL_ENABLED = ExtensionModelPackage.YINPUT__INITIAL_ENABLED;

	/**
	 * The feature id for the '<em><b>Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__ENABLED = ExtensionModelPackage.YINPUT__ENABLED;

	/**
	 * The feature id for the '<em><b>Layout Idx</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__LAYOUT_IDX = ExtensionModelPackage.YINPUT__LAYOUT_IDX;

	/**
	 * The feature id for the '<em><b>Layout Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__LAYOUT_COLUMNS = ExtensionModelPackage.YINPUT__LAYOUT_COLUMNS;

	/**
	 * The feature id for the '<em><b>Tab Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__TAB_INDEX = ExtensionModelPackage.YINPUT__TAB_INDEX;

	/**
	 * The feature id for the '<em><b>Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__VALIDATORS = ExtensionModelPackage.YINPUT__VALIDATORS;

	/**
	 * The feature id for the '<em><b>Internal Validators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__INTERNAL_VALIDATORS = ExtensionModelPackage.YINPUT__INTERNAL_VALIDATORS;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__CONVERTER = ExtensionModelPackage.YINPUT__CONVERTER;

	/**
	 * The feature id for the '<em><b>Collection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__COLLECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Selection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__SELECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Multi Selection Binding Endpoint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__MULTI_SELECTION_BINDING_ENDPOINT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Use Bean Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__USE_BEAN_SERVICE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Selection Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__SELECTION_TYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Selection Event Topic</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__SELECTION_EVENT_TOPIC = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__SELECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Multi Selection</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__MULTI_SELECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__COLLECTION = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__TYPE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Emf Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__EMF_NS_URI = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__TYPE_QUALIFIED_NAME = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__COLUMNS = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Sort Order</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__SORT_ORDER = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Column Reordering Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__COLUMN_REORDERING_ALLOWED = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Cell Style Generator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__CELL_STYLE_GENERATOR = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Filtering Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__FILTERING_VISIBLE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Custom Filters</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__CUSTOM_FILTERS = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Headers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__HEADERS = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Header Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__HEADER_VISIBLE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Footers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__FOOTERS = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Footer Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__FOOTER_VISIBLE = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Editor Enabled</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__EDITOR_ENABLED = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Editor Cancel I1 8n Label Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__EDITOR_CANCEL_I1_8N_LABEL_KEY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Editor Save I1 8n Label Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__EDITOR_SAVE_I1_8N_LABEL_KEY = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Editor Saved</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__EDITOR_SAVED = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Set Last Refresh Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID__SET_LAST_REFRESH_TIME = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 26;

	/**
	 * The number of structural features of the '<em>Cx Grid</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FEATURE_COUNT = ExtensionModelPackage.YINPUT_FEATURE_COUNT + 27;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridProvider <em>Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridProvider
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridProvider()
	 * @generated
	 */
	int CX_GRID_PROVIDER = 1;

	/**
	 * The number of structural features of the '<em>Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_PROVIDER_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow <em>Meta Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridMetaRow()
	 * @generated
	 */
	int CX_GRID_META_ROW = 2;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Groupings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW__GROUPINGS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Custom Cells</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW__CUSTOM_CELLS = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Meta Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_ROW_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridHeaderRowImpl <em>Header Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridHeaderRowImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridHeaderRow()
	 * @generated
	 */
	int CX_GRID_HEADER_ROW = 3;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW__TAGS = CX_GRID_META_ROW__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW__ID = CX_GRID_META_ROW__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW__NAME = CX_GRID_META_ROW__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW__PROPERTIES = CX_GRID_META_ROW__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Groupings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW__GROUPINGS = CX_GRID_META_ROW__GROUPINGS;

	/**
	 * The feature id for the '<em><b>Custom Cells</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW__CUSTOM_CELLS = CX_GRID_META_ROW__CUSTOM_CELLS;

	/**
	 * The number of structural features of the '<em>Header Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_HEADER_ROW_FEATURE_COUNT = CX_GRID_META_ROW_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFooterRowImpl <em>Footer Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridFooterRowImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridFooterRow()
	 * @generated
	 */
	int CX_GRID_FOOTER_ROW = 4;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW__TAGS = CX_GRID_META_ROW__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW__ID = CX_GRID_META_ROW__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW__NAME = CX_GRID_META_ROW__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW__PROPERTIES = CX_GRID_META_ROW__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Groupings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW__GROUPINGS = CX_GRID_META_ROW__GROUPINGS;

	/**
	 * The feature id for the '<em><b>Custom Cells</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW__CUSTOM_CELLS = CX_GRID_META_ROW__CUSTOM_CELLS;

	/**
	 * The number of structural features of the '<em>Footer Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FOOTER_ROW_FEATURE_COUNT = CX_GRID_META_ROW_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl <em>Filter Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridFilterRow()
	 * @generated
	 */
	int CX_GRID_FILTER_ROW = 5;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW__TAGS = CX_GRID_META_ROW__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW__ID = CX_GRID_META_ROW__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW__NAME = CX_GRID_META_ROW__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW__PROPERTIES = CX_GRID_META_ROW__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Groupings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW__GROUPINGS = CX_GRID_META_ROW__GROUPINGS;

	/**
	 * The feature id for the '<em><b>Custom Cells</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW__CUSTOM_CELLS = CX_GRID_META_ROW__CUSTOM_CELLS;

	/**
	 * The number of structural features of the '<em>Filter Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_FILTER_ROW_FEATURE_COUNT = CX_GRID_META_ROW_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupableImpl <em>Groupable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupableImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridGroupable()
	 * @generated
	 */
	int CX_GRID_GROUPABLE = 6;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPABLE__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPABLE__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPABLE__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPABLE__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The number of structural features of the '<em>Groupable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPABLE_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl <em>Meta Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridMetaCell()
	 * @generated
	 */
	int CX_GRID_META_CELL = 7;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__TARGET = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__LABEL = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__LABEL_I1_8N_KEY = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Use HTML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__USE_HTML = CoreModelPackage.YELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Element</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL__ELEMENT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Meta Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_META_CELL_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl <em>Grouped Cell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridGroupedCell()
	 * @generated
	 */
	int CX_GRID_GROUPED_CELL = 8;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__TAGS = CX_GRID_GROUPABLE__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__ID = CX_GRID_GROUPABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__NAME = CX_GRID_GROUPABLE__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__PROPERTIES = CX_GRID_GROUPABLE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Groupables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__GROUPABLES = CX_GRID_GROUPABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__LABEL = CX_GRID_GROUPABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY = CX_GRID_GROUPABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Use HTML</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL__USE_HTML = CX_GRID_GROUPABLE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Grouped Cell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_GROUPED_CELL_FEATURE_COUNT = CX_GRID_GROUPABLE_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridColumnImpl <em>Column</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridColumnImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridColumn()
	 * @generated
	 */
	int CX_GRID_COLUMN = 9;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__TAGS = CX_GRID_GROUPABLE__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__ID = CX_GRID_GROUPABLE__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__NAME = CX_GRID_GROUPABLE__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__PROPERTIES = CX_GRID_GROUPABLE__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Property Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__PROPERTY_ID = CX_GRID_GROUPABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__LABEL = CX_GRID_GROUPABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Label I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__LABEL_I1_8N_KEY = CX_GRID_GROUPABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Editable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__EDITABLE = CX_GRID_GROUPABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__CONVERTER = CX_GRID_GROUPABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Renderer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__RENDERER = CX_GRID_GROUPABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Editor Field</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__EDITOR_FIELD = CX_GRID_GROUPABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Search Field</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__SEARCH_FIELD = CX_GRID_GROUPABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Header Caption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__HEADER_CAPTION = CX_GRID_GROUPABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Header Caption I1 8n Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__HEADER_CAPTION_I1_8N_KEY = CX_GRID_GROUPABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Expand Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__EXPAND_RATIO = CX_GRID_GROUPABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Hidden</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__HIDDEN = CX_GRID_GROUPABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Hideable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__HIDEABLE = CX_GRID_GROUPABLE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Sortable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__SORTABLE = CX_GRID_GROUPABLE_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__PROPERTY_PATH = CX_GRID_GROUPABLE_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__WIDTH = CX_GRID_GROUPABLE_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Min Width Pixels</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__MIN_WIDTH_PIXELS = CX_GRID_GROUPABLE_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Max Width Pixels</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__MAX_WIDTH_PIXELS = CX_GRID_GROUPABLE_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Used In Meta Cells</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__USED_IN_META_CELLS = CX_GRID_GROUPABLE_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__TYPE = CX_GRID_GROUPABLE_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__TYPE_QUALIFIED_NAME = CX_GRID_GROUPABLE_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Edits Dto</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__EDITS_DTO = CX_GRID_GROUPABLE_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Filter Property Path For Edits Dto</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__FILTER_PROPERTY_PATH_FOR_EDITS_DTO = CX_GRID_GROUPABLE_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Source Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN__SOURCE_TYPE = CX_GRID_GROUPABLE_FEATURE_COUNT + 23;

	/**
	 * The number of structural features of the '<em>Column</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_COLUMN_FEATURE_COUNT = CX_GRID_GROUPABLE_FEATURE_COUNT + 24;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridCellStyleGeneratorImpl <em>Cell Style Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridCellStyleGeneratorImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridCellStyleGenerator()
	 * @generated
	 */
	int CX_GRID_CELL_STYLE_GENERATOR = 10;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_CELL_STYLE_GENERATOR__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_CELL_STYLE_GENERATOR__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_CELL_STYLE_GENERATOR__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_CELL_STYLE_GENERATOR__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The number of structural features of the '<em>Cell Style Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_CELL_STYLE_GENERATOR_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridDelegateCellStyleGeneratorImpl <em>Delegate Cell Style Generator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridDelegateCellStyleGeneratorImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridDelegateCellStyleGenerator()
	 * @generated
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR = 11;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__TAGS = CX_GRID_CELL_STYLE_GENERATOR__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__ID = CX_GRID_CELL_STYLE_GENERATOR__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__NAME = CX_GRID_CELL_STYLE_GENERATOR__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__PROPERTIES = CX_GRID_CELL_STYLE_GENERATOR__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Delegate Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__DELEGATE_ID = CX_GRID_CELL_STYLE_GENERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Delegate Cell Style Generator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_DELEGATE_CELL_STYLE_GENERATOR_FEATURE_COUNT = CX_GRID_CELL_STYLE_GENERATOR_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridSortableImpl <em>Sortable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridSortableImpl
	 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridSortable()
	 * @generated
	 */
	int CX_GRID_SORTABLE = 12;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE__TAGS = CoreModelPackage.YELEMENT__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE__ID = CoreModelPackage.YELEMENT__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE__NAME = CoreModelPackage.YELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE__PROPERTIES = CoreModelPackage.YELEMENT__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Descending</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE__DESCENDING = CoreModelPackage.YELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Column</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE__COLUMN = CoreModelPackage.YELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sortable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CX_GRID_SORTABLE_FEATURE_COUNT = CoreModelPackage.YELEMENT_FEATURE_COUNT + 2;

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid <em>Cx Grid</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cx Grid</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid
	 * @generated
	 */
	EClass getCxGrid();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionType <em>Selection Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionType()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_SelectionType();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionEventTopic <em>Selection Event Topic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection Event Topic</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelectionEventTopic()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_SelectionEventTopic();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelection <em>Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getSelection()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_Selection();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getMultiSelection <em>Multi Selection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Multi Selection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getMultiSelection()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_MultiSelection();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Collection</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getCollection()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_Collection();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getType()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEmfNsURI <em>Emf Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Emf Ns URI</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getEmfNsURI()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_EmfNsURI();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getTypeQualifiedName()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isEditorEnabled <em>Editor Enabled</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Enabled</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#isEditorEnabled()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_EditorEnabled();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getCellStyleGenerator <em>Cell Style Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cell Style Generator</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getCellStyleGenerator()
	 * @see #getCxGrid()
	 * @generated
	 */
	EReference getCxGrid_CellStyleGenerator();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isFilteringVisible <em>Filtering Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filtering Visible</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#isFilteringVisible()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_FilteringVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isCustomFilters <em>Custom Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Custom Filters</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#isCustomFilters()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_CustomFilters();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getHeaders <em>Headers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Headers</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getHeaders()
	 * @see #getCxGrid()
	 * @generated
	 */
	EReference getCxGrid_Headers();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Columns</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getColumns()
	 * @see #getCxGrid()
	 * @generated
	 */
	EReference getCxGrid_Columns();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSortOrder <em>Sort Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sort Order</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getSortOrder()
	 * @see #getCxGrid()
	 * @generated
	 */
	EReference getCxGrid_SortOrder();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isColumnReorderingAllowed <em>Column Reordering Allowed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Column Reordering Allowed</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#isColumnReorderingAllowed()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_ColumnReorderingAllowed();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isFooterVisible <em>Footer Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Footer Visible</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#isFooterVisible()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_FooterVisible();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#isHeaderVisible <em>Header Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Visible</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#isHeaderVisible()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_HeaderVisible();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getFooters <em>Footers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Footers</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getFooters()
	 * @see #getCxGrid()
	 * @generated
	 */
	EReference getCxGrid_Footers();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorCancelI18nLabelKey <em>Editor Cancel I1 8n Label Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Cancel I1 8n Label Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorCancelI18nLabelKey()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_EditorCancelI18nLabelKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaveI18nLabelKey <em>Editor Save I1 8n Label Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Save I1 8n Label Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaveI18nLabelKey()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_EditorSaveI18nLabelKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaved <em>Editor Saved</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editor Saved</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getEditorSaved()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_EditorSaved();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGrid#getSetLastRefreshTime <em>Set Last Refresh Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Set Last Refresh Time</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGrid#getSetLastRefreshTime()
	 * @see #getCxGrid()
	 * @generated
	 */
	EAttribute getCxGrid_SetLastRefreshTime();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridProvider
	 * @generated
	 */
	EClass getCxGridProvider();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow <em>Meta Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Row</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow
	 * @generated
	 */
	EClass getCxGridMetaRow();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow#getGroupings <em>Groupings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Groupings</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow#getGroupings()
	 * @see #getCxGridMetaRow()
	 * @generated
	 */
	EReference getCxGridMetaRow_Groupings();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow#getCustomCells <em>Custom Cells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Custom Cells</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow#getCustomCells()
	 * @see #getCxGridMetaRow()
	 * @generated
	 */
	EReference getCxGridMetaRow_CustomCells();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow <em>Header Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Header Row</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow
	 * @generated
	 */
	EClass getCxGridHeaderRow();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow <em>Footer Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Footer Row</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow
	 * @generated
	 */
	EClass getCxGridFooterRow();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridFilterRow <em>Filter Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter Row</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridFilterRow
	 * @generated
	 */
	EClass getCxGridFilterRow();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupable <em>Groupable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Groupable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridGroupable
	 * @generated
	 */
	EClass getCxGridGroupable();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell <em>Meta Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Cell</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell
	 * @generated
	 */
	EClass getCxGridMetaCell();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getTarget()
	 * @see #getCxGridMetaCell()
	 * @generated
	 */
	EReference getCxGridMetaCell_Target();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabel()
	 * @see #getCxGridMetaCell()
	 * @generated
	 */
	EAttribute getCxGridMetaCell_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabelI18nKey <em>Label I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getLabelI18nKey()
	 * @see #getCxGridMetaCell()
	 * @generated
	 */
	EAttribute getCxGridMetaCell_LabelI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#isUseHTML <em>Use HTML</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use HTML</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#isUseHTML()
	 * @see #getCxGridMetaCell()
	 * @generated
	 */
	EAttribute getCxGridMetaCell_UseHTML();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Element</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell#getElement()
	 * @see #getCxGridMetaCell()
	 * @generated
	 */
	EReference getCxGridMetaCell_Element();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell <em>Grouped Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grouped Cell</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell
	 * @generated
	 */
	EClass getCxGridGroupedCell();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getGroupables <em>Groupables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Groupables</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getGroupables()
	 * @see #getCxGridGroupedCell()
	 * @generated
	 */
	EReference getCxGridGroupedCell_Groupables();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabel()
	 * @see #getCxGridGroupedCell()
	 * @generated
	 */
	EAttribute getCxGridGroupedCell_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabelI18nKey <em>Label I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#getLabelI18nKey()
	 * @see #getCxGridGroupedCell()
	 * @generated
	 */
	EAttribute getCxGridGroupedCell_LabelI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#isUseHTML <em>Use HTML</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use HTML</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell#isUseHTML()
	 * @see #getCxGridGroupedCell()
	 * @generated
	 */
	EAttribute getCxGridGroupedCell_UseHTML();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Column</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn
	 * @generated
	 */
	EClass getCxGridColumn();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyId <em>Property Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyId()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_PropertyId();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getConverter <em>Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Converter</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getConverter()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EReference getCxGridColumn_Converter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getRenderer <em>Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Renderer</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getRenderer()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EReference getCxGridColumn_Renderer();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getEditorField <em>Editor Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Editor Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getEditorField()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EReference getCxGridColumn_EditorField();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSearchField <em>Search Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Search Field</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSearchField()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EReference getCxGridColumn_SearchField();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaption <em>Header Caption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Caption</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaption()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_HeaderCaption();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaptionI18nKey <em>Header Caption I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Header Caption I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getHeaderCaptionI18nKey()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_HeaderCaptionI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getExpandRatio <em>Expand Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expand Ratio</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getExpandRatio()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_ExpandRatio();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHidden <em>Hidden</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hidden</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHidden()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Hidden();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHideable <em>Hideable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hideable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isHideable()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Hideable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isSortable <em>Sortable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sortable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isSortable()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Sortable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyPath <em>Property Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Path</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getPropertyPath()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_PropertyPath();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getWidth()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Width();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabel()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Label();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabelI18nKey <em>Label I1 8n Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label I1 8n Key</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getLabelI18nKey()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_LabelI18nKey();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditable <em>Editable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Editable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditable()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Editable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMinWidthPixels <em>Min Width Pixels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Width Pixels</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMinWidthPixels()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_MinWidthPixels();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMaxWidthPixels <em>Max Width Pixels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Width Pixels</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getMaxWidthPixels()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_MaxWidthPixels();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getUsedInMetaCells <em>Used In Meta Cells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Used In Meta Cells</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getUsedInMetaCells()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EReference getCxGridColumn_UsedInMetaCells();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getType()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_Type();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getTypeQualifiedName()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditsDto <em>Edits Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Edits Dto</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#isEditsDto()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_EditsDto();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getFilterPropertyPathForEditsDto <em>Filter Property Path For Edits Dto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter Property Path For Edits Dto</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getFilterPropertyPathForEditsDto()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_FilterPropertyPathForEditsDto();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSourceType <em>Source Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridColumn#getSourceType()
	 * @see #getCxGridColumn()
	 * @generated
	 */
	EAttribute getCxGridColumn_SourceType();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridCellStyleGenerator <em>Cell Style Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cell Style Generator</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridCellStyleGenerator
	 * @generated
	 */
	EClass getCxGridCellStyleGenerator();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator <em>Delegate Cell Style Generator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delegate Cell Style Generator</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator
	 * @generated
	 */
	EClass getCxGridDelegateCellStyleGenerator();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator#getDelegateId <em>Delegate Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delegate Id</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator#getDelegateId()
	 * @see #getCxGridDelegateCellStyleGenerator()
	 * @generated
	 */
	EAttribute getCxGridDelegateCellStyleGenerator_DelegateId();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridSortable <em>Sortable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sortable</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridSortable
	 * @generated
	 */
	EClass getCxGridSortable();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.grid.CxGridSortable#isDescending <em>Descending</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Descending</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridSortable#isDescending()
	 * @see #getCxGridSortable()
	 * @generated
	 */
	EAttribute getCxGridSortable_Descending();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.grid.CxGridSortable#getColumn <em>Column</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column</em>'.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridSortable#getColumn()
	 * @see #getCxGridSortable()
	 * @generated
	 */
	EReference getCxGridSortable_Column();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CxGridFactory getCxGridFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridImpl <em>Cx Grid</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGrid()
		 * @generated
		 */
		EClass CX_GRID = eINSTANCE.getCxGrid();

		/**
		 * The meta object literal for the '<em><b>Selection Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__SELECTION_TYPE = eINSTANCE.getCxGrid_SelectionType();

		/**
		 * The meta object literal for the '<em><b>Selection Event Topic</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__SELECTION_EVENT_TOPIC = eINSTANCE.getCxGrid_SelectionEventTopic();

		/**
		 * The meta object literal for the '<em><b>Selection</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__SELECTION = eINSTANCE.getCxGrid_Selection();

		/**
		 * The meta object literal for the '<em><b>Multi Selection</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__MULTI_SELECTION = eINSTANCE.getCxGrid_MultiSelection();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__COLLECTION = eINSTANCE.getCxGrid_Collection();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__TYPE = eINSTANCE.getCxGrid_Type();

		/**
		 * The meta object literal for the '<em><b>Emf Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__EMF_NS_URI = eINSTANCE.getCxGrid_EmfNsURI();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__TYPE_QUALIFIED_NAME = eINSTANCE.getCxGrid_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Editor Enabled</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__EDITOR_ENABLED = eINSTANCE.getCxGrid_EditorEnabled();

		/**
		 * The meta object literal for the '<em><b>Cell Style Generator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID__CELL_STYLE_GENERATOR = eINSTANCE.getCxGrid_CellStyleGenerator();

		/**
		 * The meta object literal for the '<em><b>Filtering Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__FILTERING_VISIBLE = eINSTANCE.getCxGrid_FilteringVisible();

		/**
		 * The meta object literal for the '<em><b>Custom Filters</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__CUSTOM_FILTERS = eINSTANCE.getCxGrid_CustomFilters();

		/**
		 * The meta object literal for the '<em><b>Headers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID__HEADERS = eINSTANCE.getCxGrid_Headers();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID__COLUMNS = eINSTANCE.getCxGrid_Columns();

		/**
		 * The meta object literal for the '<em><b>Sort Order</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID__SORT_ORDER = eINSTANCE.getCxGrid_SortOrder();

		/**
		 * The meta object literal for the '<em><b>Column Reordering Allowed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__COLUMN_REORDERING_ALLOWED = eINSTANCE.getCxGrid_ColumnReorderingAllowed();

		/**
		 * The meta object literal for the '<em><b>Footer Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__FOOTER_VISIBLE = eINSTANCE.getCxGrid_FooterVisible();

		/**
		 * The meta object literal for the '<em><b>Header Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__HEADER_VISIBLE = eINSTANCE.getCxGrid_HeaderVisible();

		/**
		 * The meta object literal for the '<em><b>Footers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID__FOOTERS = eINSTANCE.getCxGrid_Footers();

		/**
		 * The meta object literal for the '<em><b>Editor Cancel I1 8n Label Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__EDITOR_CANCEL_I1_8N_LABEL_KEY = eINSTANCE.getCxGrid_EditorCancelI18nLabelKey();

		/**
		 * The meta object literal for the '<em><b>Editor Save I1 8n Label Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__EDITOR_SAVE_I1_8N_LABEL_KEY = eINSTANCE.getCxGrid_EditorSaveI18nLabelKey();

		/**
		 * The meta object literal for the '<em><b>Editor Saved</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__EDITOR_SAVED = eINSTANCE.getCxGrid_EditorSaved();

		/**
		 * The meta object literal for the '<em><b>Set Last Refresh Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID__SET_LAST_REFRESH_TIME = eINSTANCE.getCxGrid_SetLastRefreshTime();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridProvider <em>Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.CxGridProvider
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridProvider()
		 * @generated
		 */
		EClass CX_GRID_PROVIDER = eINSTANCE.getCxGridProvider();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow <em>Meta Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridMetaRow()
		 * @generated
		 */
		EClass CX_GRID_META_ROW = eINSTANCE.getCxGridMetaRow();

		/**
		 * The meta object literal for the '<em><b>Groupings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_META_ROW__GROUPINGS = eINSTANCE.getCxGridMetaRow_Groupings();

		/**
		 * The meta object literal for the '<em><b>Custom Cells</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_META_ROW__CUSTOM_CELLS = eINSTANCE.getCxGridMetaRow_CustomCells();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridHeaderRowImpl <em>Header Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridHeaderRowImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridHeaderRow()
		 * @generated
		 */
		EClass CX_GRID_HEADER_ROW = eINSTANCE.getCxGridHeaderRow();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFooterRowImpl <em>Footer Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridFooterRowImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridFooterRow()
		 * @generated
		 */
		EClass CX_GRID_FOOTER_ROW = eINSTANCE.getCxGridFooterRow();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl <em>Filter Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridFilterRowImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridFilterRow()
		 * @generated
		 */
		EClass CX_GRID_FILTER_ROW = eINSTANCE.getCxGridFilterRow();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupableImpl <em>Groupable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupableImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridGroupable()
		 * @generated
		 */
		EClass CX_GRID_GROUPABLE = eINSTANCE.getCxGridGroupable();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl <em>Meta Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridMetaCellImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridMetaCell()
		 * @generated
		 */
		EClass CX_GRID_META_CELL = eINSTANCE.getCxGridMetaCell();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_META_CELL__TARGET = eINSTANCE.getCxGridMetaCell_Target();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_META_CELL__LABEL = eINSTANCE.getCxGridMetaCell_Label();

		/**
		 * The meta object literal for the '<em><b>Label I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_META_CELL__LABEL_I1_8N_KEY = eINSTANCE.getCxGridMetaCell_LabelI18nKey();

		/**
		 * The meta object literal for the '<em><b>Use HTML</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_META_CELL__USE_HTML = eINSTANCE.getCxGridMetaCell_UseHTML();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_META_CELL__ELEMENT = eINSTANCE.getCxGridMetaCell_Element();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl <em>Grouped Cell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridGroupedCell()
		 * @generated
		 */
		EClass CX_GRID_GROUPED_CELL = eINSTANCE.getCxGridGroupedCell();

		/**
		 * The meta object literal for the '<em><b>Groupables</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_GROUPED_CELL__GROUPABLES = eINSTANCE.getCxGridGroupedCell_Groupables();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_GROUPED_CELL__LABEL = eINSTANCE.getCxGridGroupedCell_Label();

		/**
		 * The meta object literal for the '<em><b>Label I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY = eINSTANCE.getCxGridGroupedCell_LabelI18nKey();

		/**
		 * The meta object literal for the '<em><b>Use HTML</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_GROUPED_CELL__USE_HTML = eINSTANCE.getCxGridGroupedCell_UseHTML();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridColumnImpl <em>Column</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridColumnImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridColumn()
		 * @generated
		 */
		EClass CX_GRID_COLUMN = eINSTANCE.getCxGridColumn();

		/**
		 * The meta object literal for the '<em><b>Property Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__PROPERTY_ID = eINSTANCE.getCxGridColumn_PropertyId();

		/**
		 * The meta object literal for the '<em><b>Converter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_COLUMN__CONVERTER = eINSTANCE.getCxGridColumn_Converter();

		/**
		 * The meta object literal for the '<em><b>Renderer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_COLUMN__RENDERER = eINSTANCE.getCxGridColumn_Renderer();

		/**
		 * The meta object literal for the '<em><b>Editor Field</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_COLUMN__EDITOR_FIELD = eINSTANCE.getCxGridColumn_EditorField();

		/**
		 * The meta object literal for the '<em><b>Search Field</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_COLUMN__SEARCH_FIELD = eINSTANCE.getCxGridColumn_SearchField();

		/**
		 * The meta object literal for the '<em><b>Header Caption</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__HEADER_CAPTION = eINSTANCE.getCxGridColumn_HeaderCaption();

		/**
		 * The meta object literal for the '<em><b>Header Caption I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__HEADER_CAPTION_I1_8N_KEY = eINSTANCE.getCxGridColumn_HeaderCaptionI18nKey();

		/**
		 * The meta object literal for the '<em><b>Expand Ratio</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__EXPAND_RATIO = eINSTANCE.getCxGridColumn_ExpandRatio();

		/**
		 * The meta object literal for the '<em><b>Hidden</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__HIDDEN = eINSTANCE.getCxGridColumn_Hidden();

		/**
		 * The meta object literal for the '<em><b>Hideable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__HIDEABLE = eINSTANCE.getCxGridColumn_Hideable();

		/**
		 * The meta object literal for the '<em><b>Sortable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__SORTABLE = eINSTANCE.getCxGridColumn_Sortable();

		/**
		 * The meta object literal for the '<em><b>Property Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__PROPERTY_PATH = eINSTANCE.getCxGridColumn_PropertyPath();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__WIDTH = eINSTANCE.getCxGridColumn_Width();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__LABEL = eINSTANCE.getCxGridColumn_Label();

		/**
		 * The meta object literal for the '<em><b>Label I1 8n Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__LABEL_I1_8N_KEY = eINSTANCE.getCxGridColumn_LabelI18nKey();

		/**
		 * The meta object literal for the '<em><b>Editable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__EDITABLE = eINSTANCE.getCxGridColumn_Editable();

		/**
		 * The meta object literal for the '<em><b>Min Width Pixels</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__MIN_WIDTH_PIXELS = eINSTANCE.getCxGridColumn_MinWidthPixels();

		/**
		 * The meta object literal for the '<em><b>Max Width Pixels</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__MAX_WIDTH_PIXELS = eINSTANCE.getCxGridColumn_MaxWidthPixels();

		/**
		 * The meta object literal for the '<em><b>Used In Meta Cells</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_COLUMN__USED_IN_META_CELLS = eINSTANCE.getCxGridColumn_UsedInMetaCells();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__TYPE = eINSTANCE.getCxGridColumn_Type();

		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__TYPE_QUALIFIED_NAME = eINSTANCE.getCxGridColumn_TypeQualifiedName();

		/**
		 * The meta object literal for the '<em><b>Edits Dto</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__EDITS_DTO = eINSTANCE.getCxGridColumn_EditsDto();

		/**
		 * The meta object literal for the '<em><b>Filter Property Path For Edits Dto</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__FILTER_PROPERTY_PATH_FOR_EDITS_DTO = eINSTANCE.getCxGridColumn_FilterPropertyPathForEditsDto();

		/**
		 * The meta object literal for the '<em><b>Source Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_COLUMN__SOURCE_TYPE = eINSTANCE.getCxGridColumn_SourceType();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridCellStyleGeneratorImpl <em>Cell Style Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridCellStyleGeneratorImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridCellStyleGenerator()
		 * @generated
		 */
		EClass CX_GRID_CELL_STYLE_GENERATOR = eINSTANCE.getCxGridCellStyleGenerator();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridDelegateCellStyleGeneratorImpl <em>Delegate Cell Style Generator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridDelegateCellStyleGeneratorImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridDelegateCellStyleGenerator()
		 * @generated
		 */
		EClass CX_GRID_DELEGATE_CELL_STYLE_GENERATOR = eINSTANCE.getCxGridDelegateCellStyleGenerator();

		/**
		 * The meta object literal for the '<em><b>Delegate Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__DELEGATE_ID = eINSTANCE.getCxGridDelegateCellStyleGenerator_DelegateId();

		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridSortableImpl <em>Sortable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridSortableImpl
		 * @see org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl#getCxGridSortable()
		 * @generated
		 */
		EClass CX_GRID_SORTABLE = eINSTANCE.getCxGridSortable();

		/**
		 * The meta object literal for the '<em><b>Descending</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CX_GRID_SORTABLE__DESCENDING = eINSTANCE.getCxGridSortable_Descending();

		/**
		 * The meta object literal for the '<em><b>Column</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CX_GRID_SORTABLE__COLUMN = eINSTANCE.getCxGridSortable_Column();

	}

}