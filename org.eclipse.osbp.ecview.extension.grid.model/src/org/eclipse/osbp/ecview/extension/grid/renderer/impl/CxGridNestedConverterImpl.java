/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cx Grid Nested Converter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getNestedDotPath <em>Nested Dot Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getBaseTypeQualifiedName <em>Base Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getNestedType <em>Nested Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getNestedTypeQualifiedName <em>Nested Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridNestedConverterImpl#getNestedTypeConverter <em>Nested Type Converter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridNestedConverterImpl extends MinimalEObjectImpl.Container implements CxGridNestedConverter {
	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The default value of the '{@link #getNestedDotPath() <em>Nested Dot Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedDotPath()
	 * @generated
	 * @ordered
	 */
	protected static final String NESTED_DOT_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNestedDotPath() <em>Nested Dot Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedDotPath()
	 * @generated
	 * @ordered
	 */
	protected String nestedDotPath = NESTED_DOT_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBaseType() <em>Base Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> baseType;

	/**
	 * The default value of the '{@link #getBaseTypeQualifiedName() <em>Base Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_TYPE_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBaseTypeQualifiedName() <em>Base Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String baseTypeQualifiedName = BASE_TYPE_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNestedType() <em>Nested Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> nestedType;

	/**
	 * The default value of the '{@link #getNestedTypeQualifiedName() <em>Nested Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String NESTED_TYPE_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNestedTypeQualifiedName() <em>Nested Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String nestedTypeQualifiedName = NESTED_TYPE_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNestedTypeConverter() <em>Nested Type Converter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNestedTypeConverter()
	 * @generated
	 * @ordered
	 */
	protected YConverter nestedTypeConverter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridNestedConverterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridRendererPackage.Literals.CX_GRID_NESTED_CONVERTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNestedDotPath() {
		return nestedDotPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNestedDotPath(String newNestedDotPath) {
		String oldNestedDotPath = nestedDotPath;
		nestedDotPath = newNestedDotPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH, oldNestedDotPath, nestedDotPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getBaseType() {
		return baseType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseType(Class<?> newBaseType) {
		Class<?> oldBaseType = baseType;
		baseType = newBaseType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE, oldBaseType, baseType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBaseTypeQualifiedName() {
		return baseTypeQualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBaseTypeQualifiedName(String newBaseTypeQualifiedName) {
		String oldBaseTypeQualifiedName = baseTypeQualifiedName;
		baseTypeQualifiedName = newBaseTypeQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME, oldBaseTypeQualifiedName, baseTypeQualifiedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Class<?> getNestedType() {
		return nestedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNestedType(Class<?> newNestedType) {
		Class<?> oldNestedType = nestedType;
		nestedType = newNestedType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE, oldNestedType, nestedType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNestedTypeQualifiedName() {
		return nestedTypeQualifiedName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNestedTypeQualifiedName(String newNestedTypeQualifiedName) {
		String oldNestedTypeQualifiedName = nestedTypeQualifiedName;
		nestedTypeQualifiedName = newNestedTypeQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME, oldNestedTypeQualifiedName, nestedTypeQualifiedName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YConverter getNestedTypeConverter() {
		return nestedTypeConverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNestedTypeConverter(YConverter newNestedTypeConverter, NotificationChain msgs) {
		YConverter oldNestedTypeConverter = nestedTypeConverter;
		nestedTypeConverter = newNestedTypeConverter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER, oldNestedTypeConverter, newNestedTypeConverter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNestedTypeConverter(YConverter newNestedTypeConverter) {
		if (newNestedTypeConverter != nestedTypeConverter) {
			NotificationChain msgs = null;
			if (nestedTypeConverter != null)
				msgs = ((InternalEObject)nestedTypeConverter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER, null, msgs);
			if (newNestedTypeConverter != null)
				msgs = ((InternalEObject)newNestedTypeConverter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER, null, msgs);
			msgs = basicSetNestedTypeConverter(newNestedTypeConverter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER, newNestedTypeConverter, newNestedTypeConverter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER:
				return basicSetNestedTypeConverter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__TAGS:
				return getTags();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__ID:
				return getId();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NAME:
				return getName();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH:
				return getNestedDotPath();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE:
				return getBaseType();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME:
				return getBaseTypeQualifiedName();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE:
				return getNestedType();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME:
				return getNestedTypeQualifiedName();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER:
				return getNestedTypeConverter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__ID:
				setId((String)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NAME:
				setName((String)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH:
				setNestedDotPath((String)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE:
				setBaseType((Class<?>)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME:
				setBaseTypeQualifiedName((String)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE:
				setNestedType((Class<?>)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME:
				setNestedTypeQualifiedName((String)newValue);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER:
				setNestedTypeConverter((YConverter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__TAGS:
				getTags().clear();
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__ID:
				setId(ID_EDEFAULT);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__PROPERTIES:
				getProperties().clear();
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH:
				setNestedDotPath(NESTED_DOT_PATH_EDEFAULT);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE:
				setBaseType((Class<?>)null);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME:
				setBaseTypeQualifiedName(BASE_TYPE_QUALIFIED_NAME_EDEFAULT);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE:
				setNestedType((Class<?>)null);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME:
				setNestedTypeQualifiedName(NESTED_TYPE_QUALIFIED_NAME_EDEFAULT);
				return;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER:
				setNestedTypeConverter((YConverter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__TAGS:
				return tags != null && !tags.isEmpty();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH:
				return NESTED_DOT_PATH_EDEFAULT == null ? nestedDotPath != null : !NESTED_DOT_PATH_EDEFAULT.equals(nestedDotPath);
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE:
				return baseType != null;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME:
				return BASE_TYPE_QUALIFIED_NAME_EDEFAULT == null ? baseTypeQualifiedName != null : !BASE_TYPE_QUALIFIED_NAME_EDEFAULT.equals(baseTypeQualifiedName);
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE:
				return nestedType != null;
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME:
				return NESTED_TYPE_QUALIFIED_NAME_EDEFAULT == null ? nestedTypeQualifiedName != null : !NESTED_TYPE_QUALIFIED_NAME_EDEFAULT.equals(nestedTypeQualifiedName);
			case CxGridRendererPackage.CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER:
				return nestedTypeConverter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", nestedDotPath: ");
		result.append(nestedDotPath);
		result.append(", baseType: ");
		result.append(baseType);
		result.append(", baseTypeQualifiedName: ");
		result.append(baseTypeQualifiedName);
		result.append(", nestedType: ");
		result.append(nestedType);
		result.append(", nestedTypeQualifiedName: ");
		result.append(nestedTypeQualifiedName);
		result.append(')');
		return result.toString();
	}

} //CxGridNestedConverterImpl
