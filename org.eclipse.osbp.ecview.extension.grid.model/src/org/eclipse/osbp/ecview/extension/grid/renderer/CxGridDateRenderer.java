/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Date Renderer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer#getDateFormat <em>Date Format</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridDateRenderer()
 * @model
 * @generated
 */
public interface CxGridDateRenderer extends CxGridRenderer {
	
	/**
	 * Returns the value of the '<em><b>Date Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Format</em>' attribute.
	 * @see #setDateFormat(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridDateRenderer_DateFormat()
	 * @model
	 * @generated
	 */
	String getDateFormat();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer#getDateFormat <em>Date Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Format</em>' attribute.
	 * @see #getDateFormat()
	 * @generated
	 */
	void setDateFormat(String value);

}