/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.util;

import org.eclipse.osbp.ecview.extension.grid.CxGridProvider;

import org.eclipse.osbp.ecview.extension.grid.renderer.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage
 * @generated
 */
public class CxGridRendererAdapterFactory extends AdapterFactoryImpl {
	
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static CxGridRendererPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridRendererAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = CxGridRendererPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridRendererSwitch<Adapter> modelSwitch =
		new CxGridRendererSwitch<Adapter>() {
			@Override
			public Adapter caseCxGridRenderer(CxGridRenderer object) {
				return createCxGridRendererAdapter();
			}
			@Override
			public Adapter caseCxGridDelegateRenderer(CxGridDelegateRenderer object) {
				return createCxGridDelegateRendererAdapter();
			}
			@Override
			public Adapter caseCxGridDateRenderer(CxGridDateRenderer object) {
				return createCxGridDateRendererAdapter();
			}
			@Override
			public Adapter caseCxGridHtmlRenderer(CxGridHtmlRenderer object) {
				return createCxGridHtmlRendererAdapter();
			}
			@Override
			public Adapter caseCxGridNumberRenderer(CxGridNumberRenderer object) {
				return createCxGridNumberRendererAdapter();
			}
			@Override
			public Adapter caseCxGridProgressBarRenderer(CxGridProgressBarRenderer object) {
				return createCxGridProgressBarRendererAdapter();
			}
			@Override
			public Adapter caseCxGridTextRenderer(CxGridTextRenderer object) {
				return createCxGridTextRendererAdapter();
			}
			@Override
			public Adapter caseCxGridButtonRenderer(CxGridButtonRenderer object) {
				return createCxGridButtonRendererAdapter();
			}
			@Override
			public Adapter caseCxGridBlobImageRenderer(CxGridBlobImageRenderer object) {
				return createCxGridBlobImageRendererAdapter();
			}
			@Override
			public Adapter caseCxGridImageRenderer(CxGridImageRenderer object) {
				return createCxGridImageRendererAdapter();
			}
			@Override
			public Adapter caseCxGridRendererClickEvent(CxGridRendererClickEvent object) {
				return createCxGridRendererClickEventAdapter();
			}
			@Override
			public Adapter caseCxGridBooleanRenderer(CxGridBooleanRenderer object) {
				return createCxGridBooleanRendererAdapter();
			}
			@Override
			public Adapter caseCxGridQuantityRenderer(CxGridQuantityRenderer object) {
				return createCxGridQuantityRendererAdapter();
			}
			@Override
			public Adapter caseCxGridPriceRenderer(CxGridPriceRenderer object) {
				return createCxGridPriceRendererAdapter();
			}
			@Override
			public Adapter caseCxGridIndicatorRenderer(CxGridIndicatorRenderer object) {
				return createCxGridIndicatorRendererAdapter();
			}
			@Override
			public Adapter caseCxGridNestedConverter(CxGridNestedConverter object) {
				return createCxGridNestedConverterAdapter();
			}
			@Override
			public Adapter caseYTaggable(YTaggable object) {
				return createYTaggableAdapter();
			}
			@Override
			public Adapter caseYElement(YElement object) {
				return createYElementAdapter();
			}
			@Override
			public Adapter caseCxGridProvider(CxGridProvider object) {
				return createCxGridProviderAdapter();
			}
			@Override
			public Adapter caseYConverter(YConverter object) {
				return createYConverterAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer <em>Cx Grid Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer
	 * @generated
	 */
	public Adapter createCxGridRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer <em>Cx Grid Delegate Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer
	 * @generated
	 */
	public Adapter createCxGridDelegateRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer <em>Cx Grid Date Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer
	 * @generated
	 */
	public Adapter createCxGridDateRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer <em>Cx Grid Html Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer
	 * @generated
	 */
	public Adapter createCxGridHtmlRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer <em>Cx Grid Number Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer
	 * @generated
	 */
	public Adapter createCxGridNumberRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer <em>Cx Grid Progress Bar Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer
	 * @generated
	 */
	public Adapter createCxGridProgressBarRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer <em>Cx Grid Text Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer
	 * @generated
	 */
	public Adapter createCxGridTextRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer <em>Cx Grid Button Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer
	 * @generated
	 */
	public Adapter createCxGridButtonRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer <em>Cx Grid Blob Image Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer
	 * @generated
	 */
	public Adapter createCxGridBlobImageRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer <em>Cx Grid Image Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer
	 * @generated
	 */
	public Adapter createCxGridImageRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent <em>Click Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent
	 * @generated
	 */
	public Adapter createCxGridRendererClickEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer <em>Cx Grid Boolean Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer
	 * @generated
	 */
	public Adapter createCxGridBooleanRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer <em>Cx Grid Quantity Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer
	 * @generated
	 */
	public Adapter createCxGridQuantityRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer <em>Cx Grid Price Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer
	 * @generated
	 */
	public Adapter createCxGridPriceRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer <em>Cx Grid Indicator Renderer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer
	 * @generated
	 */
	public Adapter createCxGridIndicatorRendererAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter <em>Cx Grid Nested Converter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter
	 * @generated
	 */
	public Adapter createCxGridNestedConverterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.core.YTaggable <em>YTaggable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YTaggable
	 * @generated
	 */
	public Adapter createYTaggableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.core.YElement <em>YElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YElement
	 * @generated
	 */
	public Adapter createYElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.extension.grid.CxGridProvider <em>Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridProvider
	 * @generated
	 */
	public Adapter createCxGridProviderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.eclipse.osbp.ecview.core.common.model.core.YConverter <em>YConverter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.eclipse.osbp.ecview.core.common.model.core.YConverter
	 * @generated
	 */
	public Adapter createYConverterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

}