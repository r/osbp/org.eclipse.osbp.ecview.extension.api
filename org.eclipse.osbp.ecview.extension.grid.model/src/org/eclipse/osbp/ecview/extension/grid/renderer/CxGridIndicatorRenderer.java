/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.renderer;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Indicator Renderer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getRedEnds <em>Red Ends</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getGreenStarts <em>Green Starts</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridIndicatorRenderer()
 * @model
 * @generated
 */
public interface CxGridIndicatorRenderer extends CxGridRenderer {
	
	/**
	 * Returns the value of the '<em><b>Red Ends</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Red Ends</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Red Ends</em>' attribute.
	 * @see #setRedEnds(double)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridIndicatorRenderer_RedEnds()
	 * @model
	 * @generated
	 */
	double getRedEnds();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getRedEnds <em>Red Ends</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Red Ends</em>' attribute.
	 * @see #getRedEnds()
	 * @generated
	 */
	void setRedEnds(double value);

	/**
	 * Returns the value of the '<em><b>Green Starts</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Green Starts</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Green Starts</em>' attribute.
	 * @see #setGreenStarts(double)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridIndicatorRenderer_GreenStarts()
	 * @model
	 * @generated
	 */
	double getGreenStarts();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer#getGreenStarts <em>Green Starts</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Green Starts</em>' attribute.
	 * @see #getGreenStarts()
	 * @generated
	 */
	void setGreenStarts(double value);

}