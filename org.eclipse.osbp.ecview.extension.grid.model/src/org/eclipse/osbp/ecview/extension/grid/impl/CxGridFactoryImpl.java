/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.impl;

import org.eclipse.osbp.ecview.extension.grid.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridFactoryImpl extends EFactoryImpl implements CxGridFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static CxGridFactory init() {
		try {
			CxGridFactory theCxGridFactory = (CxGridFactory)EPackage.Registry.INSTANCE.getEFactory(CxGridPackage.eNS_URI);
			if (theCxGridFactory != null) {
				return theCxGridFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CxGridFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CxGridPackage.CX_GRID: return createCxGrid();
			case CxGridPackage.CX_GRID_HEADER_ROW: return createCxGridHeaderRow();
			case CxGridPackage.CX_GRID_FOOTER_ROW: return createCxGridFooterRow();
			case CxGridPackage.CX_GRID_FILTER_ROW: return createCxGridFilterRow();
			case CxGridPackage.CX_GRID_META_CELL: return createCxGridMetaCell();
			case CxGridPackage.CX_GRID_GROUPED_CELL: return createCxGridGroupedCell();
			case CxGridPackage.CX_GRID_COLUMN: return createCxGridColumn();
			case CxGridPackage.CX_GRID_DELEGATE_CELL_STYLE_GENERATOR: return createCxGridDelegateCellStyleGenerator();
			case CxGridPackage.CX_GRID_SORTABLE: return createCxGridSortable();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid
	 * @generated
	 */
	public CxGrid createCxGrid() {
		CxGridImpl cxGrid = new CxGridImpl();
		return cxGrid;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid header row
	 * @generated
	 */
	public CxGridHeaderRow createCxGridHeaderRow() {
		CxGridHeaderRowImpl cxGridHeaderRow = new CxGridHeaderRowImpl();
		return cxGridHeaderRow;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid footer row
	 * @generated
	 */
	public CxGridFooterRow createCxGridFooterRow() {
		CxGridFooterRowImpl cxGridFooterRow = new CxGridFooterRowImpl();
		return cxGridFooterRow;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid filter row
	 * @generated
	 */
	public CxGridFilterRow createCxGridFilterRow() {
		CxGridFilterRowImpl cxGridFilterRow = new CxGridFilterRowImpl();
		return cxGridFilterRow;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell
	 * @generated
	 */
	public CxGridMetaCell createCxGridMetaCell() {
		CxGridMetaCellImpl cxGridMetaCell = new CxGridMetaCellImpl();
		return cxGridMetaCell;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid grouped cell
	 * @generated
	 */
	public CxGridGroupedCell createCxGridGroupedCell() {
		CxGridGroupedCellImpl cxGridGroupedCell = new CxGridGroupedCellImpl();
		return cxGridGroupedCell;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column
	 * @generated
	 */
	public CxGridColumn createCxGridColumn() {
		CxGridColumnImpl cxGridColumn = new CxGridColumnImpl();
		return cxGridColumn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid delegate cell style generator
	 * @generated
	 */
	public CxGridDelegateCellStyleGenerator createCxGridDelegateCellStyleGenerator() {
		CxGridDelegateCellStyleGeneratorImpl cxGridDelegateCellStyleGenerator = new CxGridDelegateCellStyleGeneratorImpl();
		return cxGridDelegateCellStyleGenerator;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid sortable
	 * @generated
	 */
	public CxGridSortable createCxGridSortable() {
		CxGridSortableImpl cxGridSortable = new CxGridSortableImpl();
		return cxGridSortable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid package
	 * @generated
	 */
	public CxGridPackage getCxGridPackage() {
		return (CxGridPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CxGridPackage getPackage() {
		return CxGridPackage.eINSTANCE;
	}

}