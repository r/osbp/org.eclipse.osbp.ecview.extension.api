/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.impl;

import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;

import org.eclipse.osbp.ecview.extension.grid.impl.CxGridPackageImpl;

import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;
import org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBlobImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridBooleanRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridButtonRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridDelegateRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridHtmlRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridImageRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNumberRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridPriceRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridProgressBarRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridQuantityRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererClickEvent;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridTextRenderer;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.osbp.ecview.core.common.model.binding.BindingPackage;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;

import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;

import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridRendererPackageImpl extends EPackageImpl implements CxGridRendererPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridDelegateRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridDateRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridHtmlRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridNumberRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridProgressBarRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridTextRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridButtonRendererEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cxGridBlobImageRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridImageRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridRendererClickEventEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridBooleanRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridQuantityRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridPriceRendererEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridIndicatorRendererEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cxGridNestedConverterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CxGridRendererPackageImpl() {
		super(eNS_URI, CxGridRendererFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link CxGridRendererPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the cx grid renderer package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CxGridRendererPackage init() {
		if (isInited) return (CxGridRendererPackage)EPackage.Registry.INSTANCE.getEPackage(CxGridRendererPackage.eNS_URI);

		// Obtain or create and register package
		CxGridRendererPackageImpl theCxGridRendererPackage = (CxGridRendererPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CxGridRendererPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CxGridRendererPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtDatatypesPackage.eINSTANCE.eClass();
		ExtensionModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CxGridPackageImpl theCxGridPackage = (CxGridPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CxGridPackage.eNS_URI) instanceof CxGridPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CxGridPackage.eNS_URI) : CxGridPackage.eINSTANCE);
		CxGridMementoPackageImpl theCxGridMementoPackage = (CxGridMementoPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CxGridMementoPackage.eNS_URI) instanceof CxGridMementoPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CxGridMementoPackage.eNS_URI) : CxGridMementoPackage.eINSTANCE);

		// Create package meta-data objects
		theCxGridRendererPackage.createPackageContents();
		theCxGridPackage.createPackageContents();
		theCxGridMementoPackage.createPackageContents();

		// Initialize created meta-data
		theCxGridRendererPackage.initializePackageContents();
		theCxGridPackage.initializePackageContents();
		theCxGridMementoPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCxGridRendererPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CxGridRendererPackage.eNS_URI, theCxGridRendererPackage);
		return theCxGridRendererPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer
	 * @generated
	 */
	public EClass getCxGridRenderer() {
		return cxGridRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid delegate renderer
	 * @generated
	 */
	public EClass getCxGridDelegateRenderer() {
		return cxGridDelegateRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid delegate renderer_ delegate id
	 * @generated
	 */
	public EAttribute getCxGridDelegateRenderer_DelegateId() {
		return (EAttribute)cxGridDelegateRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid date renderer
	 * @generated
	 */
	public EClass getCxGridDateRenderer() {
		return cxGridDateRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid date renderer_ date format
	 * @generated
	 */
	public EAttribute getCxGridDateRenderer_DateFormat() {
		return (EAttribute)cxGridDateRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid html renderer
	 * @generated
	 */
	public EClass getCxGridHtmlRenderer() {
		return cxGridHtmlRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid html renderer_ null representation
	 * @generated
	 */
	public EAttribute getCxGridHtmlRenderer_NullRepresentation() {
		return (EAttribute)cxGridHtmlRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid number renderer
	 * @generated
	 */
	public EClass getCxGridNumberRenderer() {
		return cxGridNumberRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid number renderer_ number format
	 * @generated
	 */
	public EAttribute getCxGridNumberRenderer_NumberFormat() {
		return (EAttribute)cxGridNumberRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid number renderer_ null representation
	 * @generated
	 */
	public EAttribute getCxGridNumberRenderer_NullRepresentation() {
		return (EAttribute)cxGridNumberRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid progress bar renderer
	 * @generated
	 */
	public EClass getCxGridProgressBarRenderer() {
		return cxGridProgressBarRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid progress bar renderer_ max value
	 * @generated
	 */
	public EAttribute getCxGridProgressBarRenderer_MaxValue() {
		return (EAttribute)cxGridProgressBarRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid text renderer
	 * @generated
	 */
	public EClass getCxGridTextRenderer() {
		return cxGridTextRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid text renderer_ null representation
	 * @generated
	 */
	public EAttribute getCxGridTextRenderer_NullRepresentation() {
		return (EAttribute)cxGridTextRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid button renderer
	 * @generated
	 */
	public EClass getCxGridButtonRenderer() {
		return cxGridButtonRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid button renderer_ null representation
	 * @generated
	 */
	public EAttribute getCxGridButtonRenderer_NullRepresentation() {
		return (EAttribute)cxGridButtonRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid button renderer_ last click event
	 * @generated
	 */
	public EReference getCxGridButtonRenderer_LastClickEvent() {
		return (EReference)cxGridButtonRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid button renderer_ event topic
	 * @generated
	 */
	public EAttribute getCxGridButtonRenderer_EventTopic() {
		return (EAttribute)cxGridButtonRendererEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCxGridBlobImageRenderer() {
		return cxGridBlobImageRendererEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCxGridBlobImageRenderer_LastClickEvent() {
		return (EReference)cxGridBlobImageRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridBlobImageRenderer_EventTopic() {
		return (EAttribute)cxGridBlobImageRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid image renderer
	 * @generated
	 */
	public EClass getCxGridImageRenderer() {
		return cxGridImageRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid image renderer_ last click event
	 * @generated
	 */
	public EReference getCxGridImageRenderer_LastClickEvent() {
		return (EReference)cxGridImageRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid image renderer_ event topic
	 * @generated
	 */
	public EAttribute getCxGridImageRenderer_EventTopic() {
		return (EAttribute)cxGridImageRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer click event
	 * @generated
	 */
	public EClass getCxGridRendererClickEvent() {
		return cxGridRendererClickEventEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer click event_ renderer
	 * @generated
	 */
	public EReference getCxGridRendererClickEvent_Renderer() {
		return (EReference)cxGridRendererClickEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer click event_ last click time
	 * @generated
	 */
	public EAttribute getCxGridRendererClickEvent_LastClickTime() {
		return (EAttribute)cxGridRendererClickEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid boolean renderer
	 * @generated
	 */
	public EClass getCxGridBooleanRenderer() {
		return cxGridBooleanRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer
	 * @generated
	 */
	public EClass getCxGridQuantityRenderer() {
		return cxGridQuantityRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer_ value property path
	 * @generated
	 */
	public EAttribute getCxGridQuantityRenderer_ValuePropertyPath() {
		return (EAttribute)cxGridQuantityRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer_ uom property path
	 * @generated
	 */
	public EAttribute getCxGridQuantityRenderer_UomPropertyPath() {
		return (EAttribute)cxGridQuantityRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer_ null representation
	 * @generated
	 */
	public EAttribute getCxGridQuantityRenderer_NullRepresentation() {
		return (EAttribute)cxGridQuantityRendererEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer_ html pattern
	 * @generated
	 */
	public EAttribute getCxGridQuantityRenderer_HtmlPattern() {
		return (EAttribute)cxGridQuantityRendererEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid quantity renderer_ number format
	 * @generated
	 */
	public EAttribute getCxGridQuantityRenderer_NumberFormat() {
		return (EAttribute)cxGridQuantityRendererEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer
	 * @generated
	 */
	public EClass getCxGridPriceRenderer() {
		return cxGridPriceRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer_ value property path
	 * @generated
	 */
	public EAttribute getCxGridPriceRenderer_ValuePropertyPath() {
		return (EAttribute)cxGridPriceRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer_ currency property path
	 * @generated
	 */
	public EAttribute getCxGridPriceRenderer_CurrencyPropertyPath() {
		return (EAttribute)cxGridPriceRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer_ null representation
	 * @generated
	 */
	public EAttribute getCxGridPriceRenderer_NullRepresentation() {
		return (EAttribute)cxGridPriceRendererEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer_ html pattern
	 * @generated
	 */
	public EAttribute getCxGridPriceRenderer_HtmlPattern() {
		return (EAttribute)cxGridPriceRendererEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid price renderer_ number format
	 * @generated
	 */
	public EAttribute getCxGridPriceRenderer_NumberFormat() {
		return (EAttribute)cxGridPriceRendererEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid indicator renderer
	 * @generated
	 */
	public EClass getCxGridIndicatorRenderer() {
		return cxGridIndicatorRendererEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid indicator renderer_ red ends
	 * @generated
	 */
	public EAttribute getCxGridIndicatorRenderer_RedEnds() {
		return (EAttribute)cxGridIndicatorRendererEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid indicator renderer_ green starts
	 * @generated
	 */
	public EAttribute getCxGridIndicatorRenderer_GreenStarts() {
		return (EAttribute)cxGridIndicatorRendererEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCxGridNestedConverter() {
		return cxGridNestedConverterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridNestedConverter_NestedDotPath() {
		return (EAttribute)cxGridNestedConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridNestedConverter_BaseType() {
		return (EAttribute)cxGridNestedConverterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridNestedConverter_BaseTypeQualifiedName() {
		return (EAttribute)cxGridNestedConverterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridNestedConverter_NestedType() {
		return (EAttribute)cxGridNestedConverterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridNestedConverter_NestedTypeQualifiedName() {
		return (EAttribute)cxGridNestedConverterEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCxGridNestedConverter_NestedTypeConverter() {
		return (EReference)cxGridNestedConverterEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid renderer factory
	 * @generated
	 */
	public CxGridRendererFactory getCxGridRendererFactory() {
		return (CxGridRendererFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		cxGridRendererEClass = createEClass(CX_GRID_RENDERER);

		cxGridDelegateRendererEClass = createEClass(CX_GRID_DELEGATE_RENDERER);
		createEAttribute(cxGridDelegateRendererEClass, CX_GRID_DELEGATE_RENDERER__DELEGATE_ID);

		cxGridDateRendererEClass = createEClass(CX_GRID_DATE_RENDERER);
		createEAttribute(cxGridDateRendererEClass, CX_GRID_DATE_RENDERER__DATE_FORMAT);

		cxGridHtmlRendererEClass = createEClass(CX_GRID_HTML_RENDERER);
		createEAttribute(cxGridHtmlRendererEClass, CX_GRID_HTML_RENDERER__NULL_REPRESENTATION);

		cxGridNumberRendererEClass = createEClass(CX_GRID_NUMBER_RENDERER);
		createEAttribute(cxGridNumberRendererEClass, CX_GRID_NUMBER_RENDERER__NUMBER_FORMAT);
		createEAttribute(cxGridNumberRendererEClass, CX_GRID_NUMBER_RENDERER__NULL_REPRESENTATION);

		cxGridProgressBarRendererEClass = createEClass(CX_GRID_PROGRESS_BAR_RENDERER);
		createEAttribute(cxGridProgressBarRendererEClass, CX_GRID_PROGRESS_BAR_RENDERER__MAX_VALUE);

		cxGridTextRendererEClass = createEClass(CX_GRID_TEXT_RENDERER);
		createEAttribute(cxGridTextRendererEClass, CX_GRID_TEXT_RENDERER__NULL_REPRESENTATION);

		cxGridButtonRendererEClass = createEClass(CX_GRID_BUTTON_RENDERER);
		createEAttribute(cxGridButtonRendererEClass, CX_GRID_BUTTON_RENDERER__NULL_REPRESENTATION);
		createEReference(cxGridButtonRendererEClass, CX_GRID_BUTTON_RENDERER__LAST_CLICK_EVENT);
		createEAttribute(cxGridButtonRendererEClass, CX_GRID_BUTTON_RENDERER__EVENT_TOPIC);

		cxGridBlobImageRendererEClass = createEClass(CX_GRID_BLOB_IMAGE_RENDERER);
		createEReference(cxGridBlobImageRendererEClass, CX_GRID_BLOB_IMAGE_RENDERER__LAST_CLICK_EVENT);
		createEAttribute(cxGridBlobImageRendererEClass, CX_GRID_BLOB_IMAGE_RENDERER__EVENT_TOPIC);

		cxGridImageRendererEClass = createEClass(CX_GRID_IMAGE_RENDERER);
		createEReference(cxGridImageRendererEClass, CX_GRID_IMAGE_RENDERER__LAST_CLICK_EVENT);
		createEAttribute(cxGridImageRendererEClass, CX_GRID_IMAGE_RENDERER__EVENT_TOPIC);

		cxGridRendererClickEventEClass = createEClass(CX_GRID_RENDERER_CLICK_EVENT);
		createEReference(cxGridRendererClickEventEClass, CX_GRID_RENDERER_CLICK_EVENT__RENDERER);
		createEAttribute(cxGridRendererClickEventEClass, CX_GRID_RENDERER_CLICK_EVENT__LAST_CLICK_TIME);

		cxGridBooleanRendererEClass = createEClass(CX_GRID_BOOLEAN_RENDERER);

		cxGridQuantityRendererEClass = createEClass(CX_GRID_QUANTITY_RENDERER);
		createEAttribute(cxGridQuantityRendererEClass, CX_GRID_QUANTITY_RENDERER__VALUE_PROPERTY_PATH);
		createEAttribute(cxGridQuantityRendererEClass, CX_GRID_QUANTITY_RENDERER__UOM_PROPERTY_PATH);
		createEAttribute(cxGridQuantityRendererEClass, CX_GRID_QUANTITY_RENDERER__NULL_REPRESENTATION);
		createEAttribute(cxGridQuantityRendererEClass, CX_GRID_QUANTITY_RENDERER__HTML_PATTERN);
		createEAttribute(cxGridQuantityRendererEClass, CX_GRID_QUANTITY_RENDERER__NUMBER_FORMAT);

		cxGridPriceRendererEClass = createEClass(CX_GRID_PRICE_RENDERER);
		createEAttribute(cxGridPriceRendererEClass, CX_GRID_PRICE_RENDERER__VALUE_PROPERTY_PATH);
		createEAttribute(cxGridPriceRendererEClass, CX_GRID_PRICE_RENDERER__CURRENCY_PROPERTY_PATH);
		createEAttribute(cxGridPriceRendererEClass, CX_GRID_PRICE_RENDERER__NULL_REPRESENTATION);
		createEAttribute(cxGridPriceRendererEClass, CX_GRID_PRICE_RENDERER__HTML_PATTERN);
		createEAttribute(cxGridPriceRendererEClass, CX_GRID_PRICE_RENDERER__NUMBER_FORMAT);

		cxGridIndicatorRendererEClass = createEClass(CX_GRID_INDICATOR_RENDERER);
		createEAttribute(cxGridIndicatorRendererEClass, CX_GRID_INDICATOR_RENDERER__RED_ENDS);
		createEAttribute(cxGridIndicatorRendererEClass, CX_GRID_INDICATOR_RENDERER__GREEN_STARTS);

		cxGridNestedConverterEClass = createEClass(CX_GRID_NESTED_CONVERTER);
		createEAttribute(cxGridNestedConverterEClass, CX_GRID_NESTED_CONVERTER__NESTED_DOT_PATH);
		createEAttribute(cxGridNestedConverterEClass, CX_GRID_NESTED_CONVERTER__BASE_TYPE);
		createEAttribute(cxGridNestedConverterEClass, CX_GRID_NESTED_CONVERTER__BASE_TYPE_QUALIFIED_NAME);
		createEAttribute(cxGridNestedConverterEClass, CX_GRID_NESTED_CONVERTER__NESTED_TYPE);
		createEAttribute(cxGridNestedConverterEClass, CX_GRID_NESTED_CONVERTER__NESTED_TYPE_QUALIFIED_NAME);
		createEReference(cxGridNestedConverterEClass, CX_GRID_NESTED_CONVERTER__NESTED_TYPE_CONVERTER);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);
		CxGridPackage theCxGridPackage = (CxGridPackage)EPackage.Registry.INSTANCE.getEPackage(CxGridPackage.eNS_URI);
		BindingPackage theBindingPackage = (BindingPackage)EPackage.Registry.INSTANCE.getEPackage(BindingPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		cxGridRendererEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		cxGridRendererEClass.getESuperTypes().add(theCxGridPackage.getCxGridProvider());
		cxGridDelegateRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridDateRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridHtmlRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridNumberRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridProgressBarRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridTextRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridButtonRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridBlobImageRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridImageRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridBooleanRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridQuantityRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridPriceRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridIndicatorRendererEClass.getESuperTypes().add(this.getCxGridRenderer());
		cxGridNestedConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());

		// Initialize classes and features; add operations and parameters
		initEClass(cxGridRendererEClass, CxGridRenderer.class, "CxGridRenderer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridDelegateRendererEClass, CxGridDelegateRenderer.class, "CxGridDelegateRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridDelegateRenderer_DelegateId(), ecorePackage.getEString(), "delegateId", null, 1, 1, CxGridDelegateRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridDateRendererEClass, CxGridDateRenderer.class, "CxGridDateRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridDateRenderer_DateFormat(), ecorePackage.getEString(), "dateFormat", null, 0, 1, CxGridDateRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridHtmlRendererEClass, CxGridHtmlRenderer.class, "CxGridHtmlRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridHtmlRenderer_NullRepresentation(), ecorePackage.getEString(), "nullRepresentation", "", 0, 1, CxGridHtmlRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridNumberRendererEClass, CxGridNumberRenderer.class, "CxGridNumberRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridNumberRenderer_NumberFormat(), ecorePackage.getEString(), "numberFormat", "#,##0.00", 0, 1, CxGridNumberRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridNumberRenderer_NullRepresentation(), ecorePackage.getEString(), "nullRepresentation", "", 0, 1, CxGridNumberRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridProgressBarRendererEClass, CxGridProgressBarRenderer.class, "CxGridProgressBarRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridProgressBarRenderer_MaxValue(), ecorePackage.getEDouble(), "maxValue", "1", 0, 1, CxGridProgressBarRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridTextRendererEClass, CxGridTextRenderer.class, "CxGridTextRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridTextRenderer_NullRepresentation(), ecorePackage.getEString(), "nullRepresentation", "", 0, 1, CxGridTextRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridButtonRendererEClass, CxGridButtonRenderer.class, "CxGridButtonRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridButtonRenderer_NullRepresentation(), ecorePackage.getEString(), "nullRepresentation", "", 0, 1, CxGridButtonRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridButtonRenderer_LastClickEvent(), this.getCxGridRendererClickEvent(), null, "lastClickEvent", null, 0, 1, CxGridButtonRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridButtonRenderer_EventTopic(), ecorePackage.getEString(), "eventTopic", "", 0, 1, CxGridButtonRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(cxGridButtonRendererEClass, theBindingPackage.getYECViewModelValueBindingEndpoint(), "createLastClickEventEndpoint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(cxGridBlobImageRendererEClass, CxGridBlobImageRenderer.class, "CxGridBlobImageRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCxGridBlobImageRenderer_LastClickEvent(), this.getCxGridRendererClickEvent(), null, "lastClickEvent", null, 0, 1, CxGridBlobImageRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridBlobImageRenderer_EventTopic(), ecorePackage.getEString(), "eventTopic", "", 0, 1, CxGridBlobImageRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(cxGridBlobImageRendererEClass, theBindingPackage.getYECViewModelValueBindingEndpoint(), "createLastClickEventEndpoint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(cxGridImageRendererEClass, CxGridImageRenderer.class, "CxGridImageRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCxGridImageRenderer_LastClickEvent(), this.getCxGridRendererClickEvent(), null, "lastClickEvent", null, 0, 1, CxGridImageRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridImageRenderer_EventTopic(), ecorePackage.getEString(), "eventTopic", "", 0, 1, CxGridImageRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(cxGridImageRendererEClass, theBindingPackage.getYECViewModelValueBindingEndpoint(), "createLastClickEventEndpoint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(cxGridRendererClickEventEClass, CxGridRendererClickEvent.class, "CxGridRendererClickEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCxGridRendererClickEvent_Renderer(), this.getCxGridRenderer(), null, "renderer", null, 1, 1, CxGridRendererClickEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridRendererClickEvent_LastClickTime(), ecorePackage.getELong(), "lastClickTime", null, 0, 1, CxGridRendererClickEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridBooleanRendererEClass, CxGridBooleanRenderer.class, "CxGridBooleanRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridQuantityRendererEClass, CxGridQuantityRenderer.class, "CxGridQuantityRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridQuantityRenderer_ValuePropertyPath(), ecorePackage.getEString(), "valuePropertyPath", null, 1, 1, CxGridQuantityRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridQuantityRenderer_UomPropertyPath(), ecorePackage.getEString(), "uomPropertyPath", null, 1, 1, CxGridQuantityRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridQuantityRenderer_NullRepresentation(), ecorePackage.getEString(), "nullRepresentation", "", 0, 1, CxGridQuantityRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridQuantityRenderer_HtmlPattern(), ecorePackage.getEString(), "htmlPattern", "<b>{@value}</b> <i>{@currency}</i>", 1, 1, CxGridQuantityRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridQuantityRenderer_NumberFormat(), ecorePackage.getEString(), "numberFormat", "#,##0.00", 1, 1, CxGridQuantityRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridPriceRendererEClass, CxGridPriceRenderer.class, "CxGridPriceRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridPriceRenderer_ValuePropertyPath(), ecorePackage.getEString(), "valuePropertyPath", null, 1, 1, CxGridPriceRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridPriceRenderer_CurrencyPropertyPath(), ecorePackage.getEString(), "currencyPropertyPath", null, 1, 1, CxGridPriceRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridPriceRenderer_NullRepresentation(), ecorePackage.getEString(), "nullRepresentation", "", 0, 1, CxGridPriceRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridPriceRenderer_HtmlPattern(), ecorePackage.getEString(), "htmlPattern", "<b>{@value}</b> <i>{@currency}</i>", 1, 1, CxGridPriceRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridPriceRenderer_NumberFormat(), ecorePackage.getEString(), "numberFormat", "#,##0.00", 1, 1, CxGridPriceRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridIndicatorRendererEClass, CxGridIndicatorRenderer.class, "CxGridIndicatorRenderer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridIndicatorRenderer_RedEnds(), ecorePackage.getEDouble(), "redEnds", null, 0, 1, CxGridIndicatorRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridIndicatorRenderer_GreenStarts(), ecorePackage.getEDouble(), "greenStarts", null, 0, 1, CxGridIndicatorRenderer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridNestedConverterEClass, CxGridNestedConverter.class, "CxGridNestedConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridNestedConverter_NestedDotPath(), ecorePackage.getEString(), "nestedDotPath", null, 0, 1, CxGridNestedConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getCxGridNestedConverter_BaseType(), g1, "baseType", null, 0, 1, CxGridNestedConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridNestedConverter_BaseTypeQualifiedName(), ecorePackage.getEString(), "baseTypeQualifiedName", null, 0, 1, CxGridNestedConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getCxGridNestedConverter_NestedType(), g1, "nestedType", null, 0, 1, CxGridNestedConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridNestedConverter_NestedTypeQualifiedName(), ecorePackage.getEString(), "nestedTypeQualifiedName", null, 0, 1, CxGridNestedConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridNestedConverter_NestedTypeConverter(), theCoreModelPackage.getYConverter(), null, "nestedTypeConverter", null, 0, 1, CxGridNestedConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

}