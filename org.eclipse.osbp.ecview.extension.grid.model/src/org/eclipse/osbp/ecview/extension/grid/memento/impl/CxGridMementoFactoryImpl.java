/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento.impl;

import org.eclipse.osbp.ecview.extension.grid.memento.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridMementoFactoryImpl extends EFactoryImpl implements CxGridMementoFactory {
	
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static CxGridMementoFactory init() {
		try {
			CxGridMementoFactory theCxGridMementoFactory = (CxGridMementoFactory)EPackage.Registry.INSTANCE.getEFactory(CxGridMementoPackage.eNS_URI);
			if (theCxGridMementoFactory != null) {
				return theCxGridMementoFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CxGridMementoFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridMementoFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case CxGridMementoPackage.CX_GRID_MEMENTO: return createCxGridMemento();
			case CxGridMementoPackage.CX_GRID_MEMENTO_COLUMN: return createCxGridMementoColumn();
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE: return createCxGridMementoSortable();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento
	 * @generated
	 */
	public CxGridMemento createCxGridMemento() {
		CxGridMementoImpl cxGridMemento = new CxGridMementoImpl();
		return cxGridMemento;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column
	 * @generated
	 */
	public CxGridMementoColumn createCxGridMementoColumn() {
		CxGridMementoColumnImpl cxGridMementoColumn = new CxGridMementoColumnImpl();
		return cxGridMementoColumn;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento sortable
	 * @generated
	 */
	public CxGridMementoSortable createCxGridMementoSortable() {
		CxGridMementoSortableImpl cxGridMementoSortable = new CxGridMementoSortableImpl();
		return cxGridMementoSortable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento package
	 * @generated
	 */
	public CxGridMementoPackage getCxGridMementoPackage() {
		return (CxGridMementoPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CxGridMementoPackage getPackage() {
		return CxGridMementoPackage.eINSTANCE;
	}

}