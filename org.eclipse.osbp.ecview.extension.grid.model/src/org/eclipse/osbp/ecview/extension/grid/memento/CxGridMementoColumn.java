/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Column</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getPropertyId <em>Property Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isEditable <em>Editable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getExpandRatio <em>Expand Ratio</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHidden <em>Hidden</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHideable <em>Hideable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isSortable <em>Sortable</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getWidth <em>Width</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn()
 * @model
 * @generated
 */
public interface CxGridMementoColumn extends EObject {
	
	/**
	 * Returns the value of the '<em><b>Property Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Id</em>' attribute.
	 * @see #setPropertyId(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_PropertyId()
	 * @model
	 * @generated
	 */
	String getPropertyId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getPropertyId <em>Property Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Id</em>' attribute.
	 * @see #getPropertyId()
	 * @generated
	 */
	void setPropertyId(String value);

	/**
	 * Returns the value of the '<em><b>Editable</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Editable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Editable</em>' attribute.
	 * @see #setEditable(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_Editable()
	 * @model default="false"
	 * @generated
	 */
	boolean isEditable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isEditable <em>Editable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Editable</em>' attribute.
	 * @see #isEditable()
	 * @generated
	 */
	void setEditable(boolean value);

	/**
	 * Returns the value of the '<em><b>Expand Ratio</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expand Ratio</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expand Ratio</em>' attribute.
	 * @see #setExpandRatio(int)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_ExpandRatio()
	 * @model default="-1"
	 * @generated
	 */
	int getExpandRatio();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getExpandRatio <em>Expand Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expand Ratio</em>' attribute.
	 * @see #getExpandRatio()
	 * @generated
	 */
	void setExpandRatio(int value);

	/**
	 * Returns the value of the '<em><b>Hidden</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hidden</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hidden</em>' attribute.
	 * @see #setHidden(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_Hidden()
	 * @model default="false"
	 * @generated
	 */
	boolean isHidden();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHidden <em>Hidden</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hidden</em>' attribute.
	 * @see #isHidden()
	 * @generated
	 */
	void setHidden(boolean value);

	/**
	 * Returns the value of the '<em><b>Hideable</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hideable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hideable</em>' attribute.
	 * @see #setHideable(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_Hideable()
	 * @model default="true"
	 * @generated
	 */
	boolean isHideable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isHideable <em>Hideable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hideable</em>' attribute.
	 * @see #isHideable()
	 * @generated
	 */
	void setHideable(boolean value);

	/**
	 * Returns the value of the '<em><b>Sortable</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sortable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sortable</em>' attribute.
	 * @see #setSortable(boolean)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_Sortable()
	 * @model default="true"
	 * @generated
	 */
	boolean isSortable();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#isSortable <em>Sortable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sortable</em>' attribute.
	 * @see #isSortable()
	 * @generated
	 */
	void setSortable(boolean value);

	/**
	 * Returns the value of the '<em><b>Width</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Width</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Width</em>' attribute.
	 * @see #setWidth(int)
	 * @see org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage#getCxGridMementoColumn_Width()
	 * @model default="-1"
	 * @generated
	 */
	int getWidth();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn#getWidth <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Width</em>' attribute.
	 * @see #getWidth()
	 * @generated
	 */
	void setWidth(int value);

}