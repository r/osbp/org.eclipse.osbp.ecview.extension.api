/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.renderer;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cx Grid Nested Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedDotPath <em>Nested Dot Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseType <em>Base Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseTypeQualifiedName <em>Base Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedType <em>Nested Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeQualifiedName <em>Nested Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeConverter <em>Nested Type Converter</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter()
 * @model
 * @generated
 */
public interface CxGridNestedConverter extends YConverter {
	/**
	 * Returns the value of the '<em><b>Nested Dot Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested Dot Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Dot Path</em>' attribute.
	 * @see #setNestedDotPath(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter_NestedDotPath()
	 * @model
	 * @generated
	 */
	String getNestedDotPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedDotPath <em>Nested Dot Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested Dot Path</em>' attribute.
	 * @see #getNestedDotPath()
	 * @generated
	 */
	void setNestedDotPath(String value);

	/**
	 * Returns the value of the '<em><b>Base Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Type</em>' attribute.
	 * @see #setBaseType(Class)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter_BaseType()
	 * @model
	 * @generated
	 */
	Class<?> getBaseType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseType <em>Base Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Type</em>' attribute.
	 * @see #getBaseType()
	 * @generated
	 */
	void setBaseType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Base Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Type Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Type Qualified Name</em>' attribute.
	 * @see #setBaseTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter_BaseTypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getBaseTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getBaseTypeQualifiedName <em>Base Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Type Qualified Name</em>' attribute.
	 * @see #getBaseTypeQualifiedName()
	 * @generated
	 */
	void setBaseTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Nested Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Type</em>' attribute.
	 * @see #setNestedType(Class)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter_NestedType()
	 * @model
	 * @generated
	 */
	Class<?> getNestedType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedType <em>Nested Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested Type</em>' attribute.
	 * @see #getNestedType()
	 * @generated
	 */
	void setNestedType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Nested Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested Type Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Type Qualified Name</em>' attribute.
	 * @see #setNestedTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter_NestedTypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getNestedTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeQualifiedName <em>Nested Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested Type Qualified Name</em>' attribute.
	 * @see #getNestedTypeQualifiedName()
	 * @generated
	 */
	void setNestedTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Nested Type Converter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nested Type Converter</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nested Type Converter</em>' containment reference.
	 * @see #setNestedTypeConverter(YConverter)
	 * @see org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage#getCxGridNestedConverter_NestedTypeConverter()
	 * @model containment="true"
	 * @generated
	 */
	YConverter getNestedTypeConverter();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.renderer.CxGridNestedConverter#getNestedTypeConverter <em>Nested Type Converter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nested Type Converter</em>' containment reference.
	 * @see #getNestedTypeConverter()
	 * @generated
	 */
	void setNestedTypeConverter(YConverter value);

} // CxGridNestedConverter
