/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.extension.grid.CxGrid;
import org.eclipse.osbp.ecview.extension.grid.CxGridCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridColumn;
import org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator;
import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridFilterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupable;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridHeaderRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaRow;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;
import org.eclipse.osbp.ecview.extension.grid.CxGridProvider;
import org.eclipse.osbp.ecview.extension.grid.CxGridSortable;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;
import org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoPackageImpl;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererPackage;
import org.eclipse.osbp.ecview.extension.grid.renderer.impl.CxGridRendererPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridPackageImpl extends EPackageImpl implements CxGridPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridProviderEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridMetaRowEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridHeaderRowEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridFooterRowEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridFilterRowEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridGroupableEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridMetaCellEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridGroupedCellEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridColumnEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridCellStyleGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridDelegateCellStyleGeneratorEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass cxGridSortableEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private CxGridPackageImpl() {
		super(eNS_URI, CxGridFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link CxGridPackage#eINSTANCE} when
	 * that field is accessed. Clients should not invoke it directly. Instead,
	 * they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the cx grid package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static CxGridPackage init() {
		if (isInited) return (CxGridPackage)EPackage.Registry.INSTANCE.getEPackage(CxGridPackage.eNS_URI);

		// Obtain or create and register package
		CxGridPackageImpl theCxGridPackage = (CxGridPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof CxGridPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new CxGridPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtDatatypesPackage.eINSTANCE.eClass();
		ExtensionModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		CxGridRendererPackageImpl theCxGridRendererPackage = (CxGridRendererPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CxGridRendererPackage.eNS_URI) instanceof CxGridRendererPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CxGridRendererPackage.eNS_URI) : CxGridRendererPackage.eINSTANCE);
		CxGridMementoPackageImpl theCxGridMementoPackage = (CxGridMementoPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CxGridMementoPackage.eNS_URI) instanceof CxGridMementoPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CxGridMementoPackage.eNS_URI) : CxGridMementoPackage.eINSTANCE);

		// Create package meta-data objects
		theCxGridPackage.createPackageContents();
		theCxGridRendererPackage.createPackageContents();
		theCxGridMementoPackage.createPackageContents();

		// Initialize created meta-data
		theCxGridPackage.initializePackageContents();
		theCxGridRendererPackage.initializePackageContents();
		theCxGridMementoPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theCxGridPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(CxGridPackage.eNS_URI, theCxGridPackage);
		return theCxGridPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid
	 * @generated
	 */
	public EClass getCxGrid() {
		return cxGridEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ selection type
	 * @generated
	 */
	public EAttribute getCxGrid_SelectionType() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ selection event topic
	 * @generated
	 */
	public EAttribute getCxGrid_SelectionEventTopic() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ selection
	 * @generated
	 */
	public EAttribute getCxGrid_Selection() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ multi selection
	 * @generated
	 */
	public EAttribute getCxGrid_MultiSelection() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ collection
	 * @generated
	 */
	public EAttribute getCxGrid_Collection() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ type
	 * @generated
	 */
	public EAttribute getCxGrid_Type() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ emf ns uri
	 * @generated
	 */
	public EAttribute getCxGrid_EmfNsURI() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ type qualified name
	 * @generated
	 */
	public EAttribute getCxGrid_TypeQualifiedName() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ editor enabled
	 * @generated
	 */
	public EAttribute getCxGrid_EditorEnabled() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ cell style generator
	 * @generated
	 */
	public EReference getCxGrid_CellStyleGenerator() {
		return (EReference)cxGridEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ filtering visible
	 * @generated
	 */
	public EAttribute getCxGrid_FilteringVisible() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ custom filters
	 * @generated
	 */
	public EAttribute getCxGrid_CustomFilters() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ headers
	 * @generated
	 */
	public EReference getCxGrid_Headers() {
		return (EReference)cxGridEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ columns
	 * @generated
	 */
	public EReference getCxGrid_Columns() {
		return (EReference)cxGridEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ sort order
	 * @generated
	 */
	public EReference getCxGrid_SortOrder() {
		return (EReference)cxGridEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ column reordering allowed
	 * @generated
	 */
	public EAttribute getCxGrid_ColumnReorderingAllowed() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ footer visible
	 * @generated
	 */
	public EAttribute getCxGrid_FooterVisible() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ header visible
	 * @generated
	 */
	public EAttribute getCxGrid_HeaderVisible() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ footers
	 * @generated
	 */
	public EReference getCxGrid_Footers() {
		return (EReference)cxGridEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ editor cancel i18n label key
	 * @generated
	 */
	public EAttribute getCxGrid_EditorCancelI18nLabelKey() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ editor save i18n label key
	 * @generated
	 */
	public EAttribute getCxGrid_EditorSaveI18nLabelKey() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid_ editor saved
	 * @generated
	 */
	public EAttribute getCxGrid_EditorSaved() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGrid_SetLastRefreshTime() {
		return (EAttribute)cxGridEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid provider
	 * @generated
	 */
	public EClass getCxGridProvider() {
		return cxGridProviderEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta row
	 * @generated
	 */
	public EClass getCxGridMetaRow() {
		return cxGridMetaRowEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta row_ groupings
	 * @generated
	 */
	public EReference getCxGridMetaRow_Groupings() {
		return (EReference)cxGridMetaRowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta row_ custom cells
	 * @generated
	 */
	public EReference getCxGridMetaRow_CustomCells() {
		return (EReference)cxGridMetaRowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid header row
	 * @generated
	 */
	public EClass getCxGridHeaderRow() {
		return cxGridHeaderRowEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid footer row
	 * @generated
	 */
	public EClass getCxGridFooterRow() {
		return cxGridFooterRowEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid filter row
	 * @generated
	 */
	public EClass getCxGridFilterRow() {
		return cxGridFilterRowEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid groupable
	 * @generated
	 */
	public EClass getCxGridGroupable() {
		return cxGridGroupableEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell
	 * @generated
	 */
	public EClass getCxGridMetaCell() {
		return cxGridMetaCellEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell_ target
	 * @generated
	 */
	public EReference getCxGridMetaCell_Target() {
		return (EReference)cxGridMetaCellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell_ label
	 * @generated
	 */
	public EAttribute getCxGridMetaCell_Label() {
		return (EAttribute)cxGridMetaCellEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell_ label i18n key
	 * @generated
	 */
	public EAttribute getCxGridMetaCell_LabelI18nKey() {
		return (EAttribute)cxGridMetaCellEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell_ use html
	 * @generated
	 */
	public EAttribute getCxGridMetaCell_UseHTML() {
		return (EAttribute)cxGridMetaCellEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid meta cell_ element
	 * @generated
	 */
	public EReference getCxGridMetaCell_Element() {
		return (EReference)cxGridMetaCellEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid grouped cell
	 * @generated
	 */
	public EClass getCxGridGroupedCell() {
		return cxGridGroupedCellEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid grouped cell_ groupables
	 * @generated
	 */
	public EReference getCxGridGroupedCell_Groupables() {
		return (EReference)cxGridGroupedCellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid grouped cell_ label
	 * @generated
	 */
	public EAttribute getCxGridGroupedCell_Label() {
		return (EAttribute)cxGridGroupedCellEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid grouped cell_ label i18n key
	 * @generated
	 */
	public EAttribute getCxGridGroupedCell_LabelI18nKey() {
		return (EAttribute)cxGridGroupedCellEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid grouped cell_ use html
	 * @generated
	 */
	public EAttribute getCxGridGroupedCell_UseHTML() {
		return (EAttribute)cxGridGroupedCellEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column
	 * @generated
	 */
	public EClass getCxGridColumn() {
		return cxGridColumnEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ property id
	 * @generated
	 */
	public EAttribute getCxGridColumn_PropertyId() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ converter
	 * @generated
	 */
	public EReference getCxGridColumn_Converter() {
		return (EReference)cxGridColumnEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ renderer
	 * @generated
	 */
	public EReference getCxGridColumn_Renderer() {
		return (EReference)cxGridColumnEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ editor field
	 * @generated
	 */
	public EReference getCxGridColumn_EditorField() {
		return (EReference)cxGridColumnEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ search field
	 * @generated
	 */
	public EReference getCxGridColumn_SearchField() {
		return (EReference)cxGridColumnEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ header caption
	 * @generated
	 */
	public EAttribute getCxGridColumn_HeaderCaption() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ header caption i18n key
	 * @generated
	 */
	public EAttribute getCxGridColumn_HeaderCaptionI18nKey() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ expand ratio
	 * @generated
	 */
	public EAttribute getCxGridColumn_ExpandRatio() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ hidden
	 * @generated
	 */
	public EAttribute getCxGridColumn_Hidden() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ hideable
	 * @generated
	 */
	public EAttribute getCxGridColumn_Hideable() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ sortable
	 * @generated
	 */
	public EAttribute getCxGridColumn_Sortable() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ property path
	 * @generated
	 */
	public EAttribute getCxGridColumn_PropertyPath() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ width
	 * @generated
	 */
	public EAttribute getCxGridColumn_Width() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ label
	 * @generated
	 */
	public EAttribute getCxGridColumn_Label() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ label i18n key
	 * @generated
	 */
	public EAttribute getCxGridColumn_LabelI18nKey() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ editable
	 * @generated
	 */
	public EAttribute getCxGridColumn_Editable() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ min width pixels
	 * @generated
	 */
	public EAttribute getCxGridColumn_MinWidthPixels() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ max width pixels
	 * @generated
	 */
	public EAttribute getCxGridColumn_MaxWidthPixels() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ used in meta cells
	 * @generated
	 */
	public EReference getCxGridColumn_UsedInMetaCells() {
		return (EReference)cxGridColumnEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ type
	 * @generated
	 */
	public EAttribute getCxGridColumn_Type() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid column_ type qualified name
	 * @generated
	 */
	public EAttribute getCxGridColumn_TypeQualifiedName() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridColumn_EditsDto() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridColumn_FilterPropertyPathForEditsDto() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCxGridColumn_SourceType() {
		return (EAttribute)cxGridColumnEClass.getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid cell style generator
	 * @generated
	 */
	public EClass getCxGridCellStyleGenerator() {
		return cxGridCellStyleGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid delegate cell style generator
	 * @generated
	 */
	public EClass getCxGridDelegateCellStyleGenerator() {
		return cxGridDelegateCellStyleGeneratorEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid delegate cell style generator_ delegate id
	 * @generated
	 */
	public EAttribute getCxGridDelegateCellStyleGenerator_DelegateId() {
		return (EAttribute)cxGridDelegateCellStyleGeneratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid sortable
	 * @generated
	 */
	public EClass getCxGridSortable() {
		return cxGridSortableEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid sortable_ descending
	 * @generated
	 */
	public EAttribute getCxGridSortable_Descending() {
		return (EAttribute)cxGridSortableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid sortable_ column
	 * @generated
	 */
	public EReference getCxGridSortable_Column() {
		return (EReference)cxGridSortableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid factory
	 * @generated
	 */
	public CxGridFactory getCxGridFactory() {
		return (CxGridFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		cxGridEClass = createEClass(CX_GRID);
		createEAttribute(cxGridEClass, CX_GRID__SELECTION_TYPE);
		createEAttribute(cxGridEClass, CX_GRID__SELECTION_EVENT_TOPIC);
		createEAttribute(cxGridEClass, CX_GRID__SELECTION);
		createEAttribute(cxGridEClass, CX_GRID__MULTI_SELECTION);
		createEAttribute(cxGridEClass, CX_GRID__COLLECTION);
		createEAttribute(cxGridEClass, CX_GRID__TYPE);
		createEAttribute(cxGridEClass, CX_GRID__EMF_NS_URI);
		createEAttribute(cxGridEClass, CX_GRID__TYPE_QUALIFIED_NAME);
		createEReference(cxGridEClass, CX_GRID__COLUMNS);
		createEReference(cxGridEClass, CX_GRID__SORT_ORDER);
		createEAttribute(cxGridEClass, CX_GRID__COLUMN_REORDERING_ALLOWED);
		createEReference(cxGridEClass, CX_GRID__CELL_STYLE_GENERATOR);
		createEAttribute(cxGridEClass, CX_GRID__FILTERING_VISIBLE);
		createEAttribute(cxGridEClass, CX_GRID__CUSTOM_FILTERS);
		createEReference(cxGridEClass, CX_GRID__HEADERS);
		createEAttribute(cxGridEClass, CX_GRID__HEADER_VISIBLE);
		createEReference(cxGridEClass, CX_GRID__FOOTERS);
		createEAttribute(cxGridEClass, CX_GRID__FOOTER_VISIBLE);
		createEAttribute(cxGridEClass, CX_GRID__EDITOR_ENABLED);
		createEAttribute(cxGridEClass, CX_GRID__EDITOR_CANCEL_I1_8N_LABEL_KEY);
		createEAttribute(cxGridEClass, CX_GRID__EDITOR_SAVE_I1_8N_LABEL_KEY);
		createEAttribute(cxGridEClass, CX_GRID__EDITOR_SAVED);
		createEAttribute(cxGridEClass, CX_GRID__SET_LAST_REFRESH_TIME);

		cxGridProviderEClass = createEClass(CX_GRID_PROVIDER);

		cxGridMetaRowEClass = createEClass(CX_GRID_META_ROW);
		createEReference(cxGridMetaRowEClass, CX_GRID_META_ROW__GROUPINGS);
		createEReference(cxGridMetaRowEClass, CX_GRID_META_ROW__CUSTOM_CELLS);

		cxGridHeaderRowEClass = createEClass(CX_GRID_HEADER_ROW);

		cxGridFooterRowEClass = createEClass(CX_GRID_FOOTER_ROW);

		cxGridFilterRowEClass = createEClass(CX_GRID_FILTER_ROW);

		cxGridGroupableEClass = createEClass(CX_GRID_GROUPABLE);

		cxGridMetaCellEClass = createEClass(CX_GRID_META_CELL);
		createEReference(cxGridMetaCellEClass, CX_GRID_META_CELL__TARGET);
		createEAttribute(cxGridMetaCellEClass, CX_GRID_META_CELL__LABEL);
		createEAttribute(cxGridMetaCellEClass, CX_GRID_META_CELL__LABEL_I1_8N_KEY);
		createEAttribute(cxGridMetaCellEClass, CX_GRID_META_CELL__USE_HTML);
		createEReference(cxGridMetaCellEClass, CX_GRID_META_CELL__ELEMENT);

		cxGridGroupedCellEClass = createEClass(CX_GRID_GROUPED_CELL);
		createEReference(cxGridGroupedCellEClass, CX_GRID_GROUPED_CELL__GROUPABLES);
		createEAttribute(cxGridGroupedCellEClass, CX_GRID_GROUPED_CELL__LABEL);
		createEAttribute(cxGridGroupedCellEClass, CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY);
		createEAttribute(cxGridGroupedCellEClass, CX_GRID_GROUPED_CELL__USE_HTML);

		cxGridColumnEClass = createEClass(CX_GRID_COLUMN);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__PROPERTY_ID);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__LABEL);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__LABEL_I1_8N_KEY);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__EDITABLE);
		createEReference(cxGridColumnEClass, CX_GRID_COLUMN__CONVERTER);
		createEReference(cxGridColumnEClass, CX_GRID_COLUMN__RENDERER);
		createEReference(cxGridColumnEClass, CX_GRID_COLUMN__EDITOR_FIELD);
		createEReference(cxGridColumnEClass, CX_GRID_COLUMN__SEARCH_FIELD);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__HEADER_CAPTION);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__HEADER_CAPTION_I1_8N_KEY);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__EXPAND_RATIO);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__HIDDEN);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__HIDEABLE);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__SORTABLE);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__PROPERTY_PATH);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__WIDTH);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__MIN_WIDTH_PIXELS);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__MAX_WIDTH_PIXELS);
		createEReference(cxGridColumnEClass, CX_GRID_COLUMN__USED_IN_META_CELLS);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__TYPE);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__TYPE_QUALIFIED_NAME);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__EDITS_DTO);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__FILTER_PROPERTY_PATH_FOR_EDITS_DTO);
		createEAttribute(cxGridColumnEClass, CX_GRID_COLUMN__SOURCE_TYPE);

		cxGridCellStyleGeneratorEClass = createEClass(CX_GRID_CELL_STYLE_GENERATOR);

		cxGridDelegateCellStyleGeneratorEClass = createEClass(CX_GRID_DELEGATE_CELL_STYLE_GENERATOR);
		createEAttribute(cxGridDelegateCellStyleGeneratorEClass, CX_GRID_DELEGATE_CELL_STYLE_GENERATOR__DELEGATE_ID);

		cxGridSortableEClass = createEClass(CX_GRID_SORTABLE);
		createEAttribute(cxGridSortableEClass, CX_GRID_SORTABLE__DESCENDING);
		createEReference(cxGridSortableEClass, CX_GRID_SORTABLE__COLUMN);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CxGridRendererPackage theCxGridRendererPackage = (CxGridRendererPackage)EPackage.Registry.INSTANCE.getEPackage(CxGridRendererPackage.eNS_URI);
		CxGridMementoPackage theCxGridMementoPackage = (CxGridMementoPackage)EPackage.Registry.INSTANCE.getEPackage(CxGridMementoPackage.eNS_URI);
		ExtensionModelPackage theExtensionModelPackage = (ExtensionModelPackage)EPackage.Registry.INSTANCE.getEPackage(ExtensionModelPackage.eNS_URI);
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theCxGridRendererPackage);
		getESubpackages().add(theCxGridMementoPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		cxGridEClass.getESuperTypes().add(theExtensionModelPackage.getYInput());
		cxGridEClass.getESuperTypes().add(theCoreModelPackage.getYCollectionBindable());
		cxGridEClass.getESuperTypes().add(theCoreModelPackage.getYSelectionBindable());
		cxGridEClass.getESuperTypes().add(theCoreModelPackage.getYMultiSelectionBindable());
		cxGridEClass.getESuperTypes().add(theExtensionModelPackage.getYBeanServiceConsumer());
		cxGridMetaRowEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		cxGridMetaRowEClass.getESuperTypes().add(this.getCxGridProvider());
		cxGridHeaderRowEClass.getESuperTypes().add(this.getCxGridMetaRow());
		cxGridFooterRowEClass.getESuperTypes().add(this.getCxGridMetaRow());
		cxGridFilterRowEClass.getESuperTypes().add(this.getCxGridMetaRow());
		cxGridGroupableEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		cxGridGroupableEClass.getESuperTypes().add(this.getCxGridProvider());
		cxGridMetaCellEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		cxGridMetaCellEClass.getESuperTypes().add(this.getCxGridProvider());
		cxGridGroupedCellEClass.getESuperTypes().add(this.getCxGridGroupable());
		cxGridColumnEClass.getESuperTypes().add(this.getCxGridGroupable());
		cxGridColumnEClass.getESuperTypes().add(theCoreModelPackage.getYHelperLayoutProvider());
		cxGridCellStyleGeneratorEClass.getESuperTypes().add(theCoreModelPackage.getYElement());
		cxGridCellStyleGeneratorEClass.getESuperTypes().add(this.getCxGridProvider());
		cxGridDelegateCellStyleGeneratorEClass.getESuperTypes().add(this.getCxGridCellStyleGenerator());
		cxGridSortableEClass.getESuperTypes().add(theCoreModelPackage.getYElement());

		// Initialize classes and features; add operations and parameters
		initEClass(cxGridEClass, CxGrid.class, "CxGrid", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGrid_SelectionType(), theExtensionModelPackage.getYSelectionType(), "selectionType", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_SelectionEventTopic(), ecorePackage.getEString(), "selectionEventTopic", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_Selection(), ecorePackage.getEJavaObject(), "selection", null, 0, 1, CxGrid.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_MultiSelection(), ecorePackage.getEJavaObject(), "multiSelection", null, 0, -1, CxGrid.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_Collection(), ecorePackage.getEJavaObject(), "collection", null, 0, -1, CxGrid.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getCxGrid_Type(), g1, "type", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_EmfNsURI(), ecorePackage.getEString(), "emfNsURI", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGrid_Columns(), this.getCxGridColumn(), null, "columns", null, 0, -1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGrid_SortOrder(), this.getCxGridSortable(), null, "sortOrder", null, 0, -1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_ColumnReorderingAllowed(), ecorePackage.getEBoolean(), "columnReorderingAllowed", "true", 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGrid_CellStyleGenerator(), this.getCxGridCellStyleGenerator(), null, "cellStyleGenerator", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_FilteringVisible(), ecorePackage.getEBoolean(), "filteringVisible", null, 0, 1, CxGrid.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_CustomFilters(), ecorePackage.getEBoolean(), "customFilters", "false", 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGrid_Headers(), this.getCxGridHeaderRow(), null, "headers", null, 0, -1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_HeaderVisible(), ecorePackage.getEBoolean(), "headerVisible", "true", 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGrid_Footers(), this.getCxGridFooterRow(), null, "footers", null, 0, -1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_FooterVisible(), ecorePackage.getEBoolean(), "footerVisible", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_EditorEnabled(), ecorePackage.getEBoolean(), "editorEnabled", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_EditorCancelI18nLabelKey(), ecorePackage.getEString(), "editorCancelI18nLabelKey", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_EditorSaveI18nLabelKey(), ecorePackage.getEString(), "editorSaveI18nLabelKey", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_EditorSaved(), ecorePackage.getEJavaObject(), "editorSaved", null, 0, 1, CxGrid.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGrid_SetLastRefreshTime(), ecorePackage.getELong(), "setLastRefreshTime", null, 0, 1, CxGrid.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		addEOperation(cxGridEClass, theCoreModelPackage.getYHelperLayout(), "createEditorFieldHelperLayout", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(cxGridProviderEClass, CxGridProvider.class, "CxGridProvider", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridMetaRowEClass, CxGridMetaRow.class, "CxGridMetaRow", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCxGridMetaRow_Groupings(), this.getCxGridGroupedCell(), null, "groupings", null, 0, -1, CxGridMetaRow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridMetaRow_CustomCells(), this.getCxGridMetaCell(), null, "customCells", null, 0, -1, CxGridMetaRow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridHeaderRowEClass, CxGridHeaderRow.class, "CxGridHeaderRow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridFooterRowEClass, CxGridFooterRow.class, "CxGridFooterRow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridFilterRowEClass, CxGridFilterRow.class, "CxGridFilterRow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridGroupableEClass, CxGridGroupable.class, "CxGridGroupable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridMetaCellEClass, CxGridMetaCell.class, "CxGridMetaCell", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCxGridMetaCell_Target(), this.getCxGridColumn(), this.getCxGridColumn_UsedInMetaCells(), "target", null, 0, 1, CxGridMetaCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMetaCell_Label(), ecorePackage.getEString(), "label", null, 0, 1, CxGridMetaCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMetaCell_LabelI18nKey(), ecorePackage.getEString(), "labelI18nKey", null, 0, 1, CxGridMetaCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridMetaCell_UseHTML(), ecorePackage.getEBoolean(), "useHTML", null, 0, 1, CxGridMetaCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridMetaCell_Element(), theCoreModelPackage.getYEmbeddable(), null, "element", null, 0, 1, CxGridMetaCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridGroupedCellEClass, CxGridGroupedCell.class, "CxGridGroupedCell", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCxGridGroupedCell_Groupables(), this.getCxGridGroupable(), null, "groupables", null, 0, -1, CxGridGroupedCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridGroupedCell_Label(), ecorePackage.getEString(), "label", null, 0, 1, CxGridGroupedCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridGroupedCell_LabelI18nKey(), ecorePackage.getEString(), "labelI18nKey", null, 0, 1, CxGridGroupedCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridGroupedCell_UseHTML(), ecorePackage.getEBoolean(), "useHTML", null, 0, 1, CxGridGroupedCell.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridColumnEClass, CxGridColumn.class, "CxGridColumn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridColumn_PropertyId(), ecorePackage.getEString(), "propertyId", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_Label(), ecorePackage.getEString(), "label", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_LabelI18nKey(), ecorePackage.getEString(), "labelI18nKey", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_Editable(), ecorePackage.getEBoolean(), "editable", "false", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridColumn_Converter(), theCoreModelPackage.getYConverter(), null, "converter", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridColumn_Renderer(), theCxGridRendererPackage.getCxGridRenderer(), null, "renderer", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridColumn_EditorField(), theCoreModelPackage.getYField(), null, "editorField", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridColumn_SearchField(), theExtensionModelPackage.getYSearchField(), null, "searchField", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_HeaderCaption(), ecorePackage.getEString(), "headerCaption", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_HeaderCaptionI18nKey(), ecorePackage.getEString(), "headerCaptionI18nKey", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_ExpandRatio(), ecorePackage.getEInt(), "expandRatio", "-1", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_Hidden(), ecorePackage.getEBoolean(), "hidden", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_Hideable(), ecorePackage.getEBoolean(), "hideable", "true", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_Sortable(), ecorePackage.getEBoolean(), "sortable", "true", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_PropertyPath(), ecorePackage.getEString(), "propertyPath", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_Width(), ecorePackage.getEInt(), "width", "-1", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_MinWidthPixels(), ecorePackage.getEInt(), "minWidthPixels", "-1", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_MaxWidthPixels(), ecorePackage.getEInt(), "maxWidthPixels", "-1", 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridColumn_UsedInMetaCells(), this.getCxGridMetaCell(), this.getCxGridMetaCell_Target(), "usedInMetaCells", null, 0, -1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getCxGridColumn_Type(), g1, "type", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_EditsDto(), ecorePackage.getEBoolean(), "editsDto", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCxGridColumn_FilterPropertyPathForEditsDto(), ecorePackage.getEString(), "filterPropertyPathForEditsDto", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEJavaClass());
		g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getCxGridColumn_SourceType(), g1, "sourceType", null, 0, 1, CxGridColumn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridCellStyleGeneratorEClass, CxGridCellStyleGenerator.class, "CxGridCellStyleGenerator", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(cxGridDelegateCellStyleGeneratorEClass, CxGridDelegateCellStyleGenerator.class, "CxGridDelegateCellStyleGenerator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridDelegateCellStyleGenerator_DelegateId(), ecorePackage.getEString(), "delegateId", null, 1, 1, CxGridDelegateCellStyleGenerator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cxGridSortableEClass, CxGridSortable.class, "CxGridSortable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCxGridSortable_Descending(), ecorePackage.getEBoolean(), "descending", "false", 0, 1, CxGridSortable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCxGridSortable_Column(), this.getCxGridColumn(), null, "column", null, 1, 1, CxGridSortable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

}