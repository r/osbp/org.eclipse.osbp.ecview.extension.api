/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.memento.impl;

import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoColumn;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoPackage;
import org.eclipse.osbp.ecview.extension.grid.memento.CxGridMementoSortable;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sortable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoSortableImpl#isDescending <em>Descending</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.memento.impl.CxGridMementoSortableImpl#getColumn <em>Column</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridMementoSortableImpl extends MinimalEObjectImpl.Container implements CxGridMementoSortable {
	
	/**
	 * The default value of the '{@link #isDescending() <em>Descending</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDescending()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DESCENDING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDescending() <em>Descending</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDescending()
	 * @generated
	 * @ordered
	 */
	protected boolean descending = DESCENDING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColumn() <em>Column</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn()
	 * @generated
	 * @ordered
	 */
	protected CxGridMementoColumn column;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridMementoSortableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridMementoPackage.Literals.CX_GRID_MEMENTO_SORTABLE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isDescending()
	 *         <em>Descending</em>}' attribute
	 * @generated
	 */
	public boolean isDescending() {
		return descending;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newDescending
	 *            the new cached value of the '{@link #isDescending()
	 *            <em>Descending</em>}' attribute
	 * @generated
	 */
	public void setDescending(boolean newDescending) {
		boolean oldDescending = descending;
		descending = newDescending;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__DESCENDING, oldDescending, descending));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getColumn() <em>Column</em>}'
	 *         reference
	 * @generated
	 */
	public CxGridMementoColumn getColumn() {
		if (column != null && column.eIsProxy()) {
			InternalEObject oldColumn = (InternalEObject)column;
			column = (CxGridMementoColumn)eResolveProxy(oldColumn);
			if (column != oldColumn) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__COLUMN, oldColumn, column));
			}
		}
		return column;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cx grid memento column
	 * @generated
	 */
	public CxGridMementoColumn basicGetColumn() {
		return column;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newColumn
	 *            the new cached value of the '{@link #getColumn()
	 *            <em>Column</em>}' reference
	 * @generated
	 */
	public void setColumn(CxGridMementoColumn newColumn) {
		CxGridMementoColumn oldColumn = column;
		column = newColumn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__COLUMN, oldColumn, column));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__DESCENDING:
				return isDescending();
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__COLUMN:
				if (resolve) return getColumn();
				return basicGetColumn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__DESCENDING:
				setDescending((Boolean)newValue);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__COLUMN:
				setColumn((CxGridMementoColumn)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__DESCENDING:
				setDescending(DESCENDING_EDEFAULT);
				return;
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__COLUMN:
				setColumn((CxGridMementoColumn)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__DESCENDING:
				return descending != DESCENDING_EDEFAULT;
			case CxGridMementoPackage.CX_GRID_MEMENTO_SORTABLE__COLUMN:
				return column != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (descending: ");
		result.append(descending);
		result.append(')');
		return result.toString();
	}

}