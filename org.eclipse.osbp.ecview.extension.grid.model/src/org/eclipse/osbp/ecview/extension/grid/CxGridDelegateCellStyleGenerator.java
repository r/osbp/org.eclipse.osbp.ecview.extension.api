/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delegate Cell Style Generator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator#getDelegateId <em>Delegate Id</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridDelegateCellStyleGenerator()
 * @model
 * @generated
 */
public interface CxGridDelegateCellStyleGenerator extends CxGridCellStyleGenerator {
	
	/**
	 * Returns the value of the '<em><b>Delegate Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delegate Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delegate Id</em>' attribute.
	 * @see #setDelegateId(String)
	 * @see org.eclipse.osbp.ecview.extension.grid.CxGridPackage#getCxGridDelegateCellStyleGenerator_DelegateId()
	 * @model required="true"
	 * @generated
	 */
	String getDelegateId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.grid.CxGridDelegateCellStyleGenerator#getDelegateId <em>Delegate Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delegate Id</em>' attribute.
	 * @see #getDelegateId()
	 * @generated
	 */
	void setDelegateId(String value);

}