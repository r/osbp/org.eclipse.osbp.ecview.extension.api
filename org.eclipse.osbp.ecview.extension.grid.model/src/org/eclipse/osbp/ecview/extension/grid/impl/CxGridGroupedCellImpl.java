/**
 *                                                                            
} *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.osbp.ecview.extension.grid.CxGridGroupable;
import org.eclipse.osbp.ecview.extension.grid.CxGridGroupedCell;
import org.eclipse.osbp.ecview.extension.grid.CxGridPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Grouped Cell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl#getGroupables <em>Groupables</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl#getLabelI18nKey <em>Label I1 8n Key</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.grid.impl.CxGridGroupedCellImpl#isUseHTML <em>Use HTML</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CxGridGroupedCellImpl extends CxGridGroupableImpl implements CxGridGroupedCell {
	
	/**
	 * The cached value of the '{@link #getGroupables() <em>Groupables</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroupables()
	 * @generated
	 * @ordered
	 */
	protected EList<CxGridGroupable> groupables;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabelI18nKey() <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelI18nKey()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_I1_8N_KEY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabelI18nKey() <em>Label I1 8n Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabelI18nKey()
	 * @generated
	 * @ordered
	 */
	protected String labelI18nKey = LABEL_I1_8N_KEY_EDEFAULT;

	/**
	 * The default value of the '{@link #isUseHTML() <em>Use HTML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseHTML()
	 * @generated
	 * @ordered
	 */
	protected static final boolean USE_HTML_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isUseHTML() <em>Use HTML</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isUseHTML()
	 * @generated
	 * @ordered
	 */
	protected boolean useHTML = USE_HTML_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected CxGridGroupedCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CxGridPackage.Literals.CX_GRID_GROUPED_CELL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getGroupables()
	 *         <em>Groupables</em>}' reference list
	 * @generated
	 */
	public EList<CxGridGroupable> getGroupables() {
		if (groupables == null) {
			groupables = new EObjectResolvingEList<CxGridGroupable>(CxGridGroupable.class, this, CxGridPackage.CX_GRID_GROUPED_CELL__GROUPABLES);
		}
		return groupables;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLabel() <em>Label</em>}'
	 *         attribute
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLabel
	 *            the new cached value of the '{@link #getLabel()
	 *            <em>Label</em>}' attribute
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_GROUPED_CELL__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLabelI18nKey()
	 *         <em>Label I1 8n Key</em>}' attribute
	 * @generated
	 */
	public String getLabelI18nKey() {
		return labelI18nKey;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLabelI18nKey
	 *            the new cached value of the '{@link #getLabelI18nKey()
	 *            <em>Label I1 8n Key</em>}' attribute
	 * @generated
	 */
	public void setLabelI18nKey(String newLabelI18nKey) {
		String oldLabelI18nKey = labelI18nKey;
		labelI18nKey = newLabelI18nKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY, oldLabelI18nKey, labelI18nKey));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #isUseHTML() <em>Use HTML</em>}'
	 *         attribute
	 * @generated
	 */
	public boolean isUseHTML() {
		return useHTML;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newUseHTML
	 *            the new cached value of the '{@link #isUseHTML()
	 *            <em>Use HTML</em>}' attribute
	 * @generated
	 */
	public void setUseHTML(boolean newUseHTML) {
		boolean oldUseHTML = useHTML;
		useHTML = newUseHTML;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CxGridPackage.CX_GRID_GROUPED_CELL__USE_HTML, oldUseHTML, useHTML));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_GROUPED_CELL__GROUPABLES:
				return getGroupables();
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL:
				return getLabel();
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY:
				return getLabelI18nKey();
			case CxGridPackage.CX_GRID_GROUPED_CELL__USE_HTML:
				return isUseHTML();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_GROUPED_CELL__GROUPABLES:
				getGroupables().clear();
				getGroupables().addAll((Collection<? extends CxGridGroupable>)newValue);
				return;
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL:
				setLabel((String)newValue);
				return;
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY:
				setLabelI18nKey((String)newValue);
				return;
			case CxGridPackage.CX_GRID_GROUPED_CELL__USE_HTML:
				setUseHTML((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_GROUPED_CELL__GROUPABLES:
				getGroupables().clear();
				return;
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL:
				setLabel(LABEL_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY:
				setLabelI18nKey(LABEL_I1_8N_KEY_EDEFAULT);
				return;
			case CxGridPackage.CX_GRID_GROUPED_CELL__USE_HTML:
				setUseHTML(USE_HTML_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CxGridPackage.CX_GRID_GROUPED_CELL__GROUPABLES:
				return groupables != null && !groupables.isEmpty();
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL:
				return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT.equals(label);
			case CxGridPackage.CX_GRID_GROUPED_CELL__LABEL_I1_8N_KEY:
				return LABEL_I1_8N_KEY_EDEFAULT == null ? labelI18nKey != null : !LABEL_I1_8N_KEY_EDEFAULT.equals(labelI18nKey);
			case CxGridPackage.CX_GRID_GROUPED_CELL__USE_HTML:
				return useHTML != USE_HTML_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (label: ");
		result.append(label);
		result.append(", labelI18nKey: ");
		result.append(labelI18nKey);
		result.append(", useHTML: ");
		result.append(useHTML);
		result.append(')');
		return result.toString();
	}

}