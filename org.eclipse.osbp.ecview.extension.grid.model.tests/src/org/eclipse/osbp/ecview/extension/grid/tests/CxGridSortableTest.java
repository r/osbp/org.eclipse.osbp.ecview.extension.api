/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridSortable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Sortable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridSortableTest extends TestCase {

	/**
	 * The fixture for this Sortable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridSortable fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridSortableTest.class);
	}

	/**
	 * Constructs a new Sortable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridSortableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Sortable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridSortable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Sortable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridSortable getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridFactory.eINSTANCE.createCxGridSortable());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridSortableTest
