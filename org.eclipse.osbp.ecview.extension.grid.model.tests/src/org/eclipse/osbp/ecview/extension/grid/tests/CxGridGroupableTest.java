/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.extension.grid.CxGridGroupable;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Groupable</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CxGridGroupableTest extends TestCase {

	/**
	 * The fixture for this Groupable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridGroupable fixture = null;

	/**
	 * Constructs a new Groupable test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridGroupableTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Groupable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridGroupable fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Groupable test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridGroupable getFixture() {
		return fixture;
	}

} //CxGridGroupableTest
