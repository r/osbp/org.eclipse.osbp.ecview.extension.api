/**
 */
package org.eclipse.osbp.ecview.extension.grid.renderer.tests;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridIndicatorRenderer;
import org.eclipse.osbp.ecview.extension.grid.renderer.CxGridRendererFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cx Grid Indicator Renderer</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridIndicatorRendererTest extends CxGridRendererTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridIndicatorRendererTest.class);
	}

	/**
	 * Constructs a new Cx Grid Indicator Renderer test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridIndicatorRendererTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Cx Grid Indicator Renderer test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected CxGridIndicatorRenderer getFixture() {
		return (CxGridIndicatorRenderer)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridRendererFactory.eINSTANCE.createCxGridIndicatorRenderer());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridIndicatorRendererTest
