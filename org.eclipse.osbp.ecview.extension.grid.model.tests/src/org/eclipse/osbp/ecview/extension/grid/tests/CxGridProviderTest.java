/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.extension.grid.CxGridProvider;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Provider</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CxGridProviderTest extends TestCase {

	/**
	 * The fixture for this Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridProvider fixture = null;

	/**
	 * Constructs a new Provider test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridProviderTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridProvider fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Provider test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridProvider getFixture() {
		return fixture;
	}

} //CxGridProviderTest
