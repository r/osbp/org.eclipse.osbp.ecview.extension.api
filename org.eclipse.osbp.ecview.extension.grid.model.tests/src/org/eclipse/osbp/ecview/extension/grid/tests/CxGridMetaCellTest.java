/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridMetaCell;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Meta Cell</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridMetaCellTest extends TestCase {

	/**
	 * The fixture for this Meta Cell test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridMetaCell fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridMetaCellTest.class);
	}

	/**
	 * Constructs a new Meta Cell test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridMetaCellTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Meta Cell test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridMetaCell fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Meta Cell test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridMetaCell getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridFactory.eINSTANCE.createCxGridMetaCell());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridMetaCellTest
