/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import org.eclipse.osbp.ecview.extension.grid.CxGridFactory;
import org.eclipse.osbp.ecview.extension.grid.CxGridFooterRow;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Footer Row</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CxGridFooterRowTest extends TestCase {

	/**
	 * The fixture for this Footer Row test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridFooterRow fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CxGridFooterRowTest.class);
	}

	/**
	 * Constructs a new Footer Row test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridFooterRowTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Footer Row test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridFooterRow fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Footer Row test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridFooterRow getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CxGridFactory.eINSTANCE.createCxGridFooterRow());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CxGridFooterRowTest
