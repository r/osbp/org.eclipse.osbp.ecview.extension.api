/**
 */
package org.eclipse.osbp.ecview.extension.grid.tests;

import junit.framework.TestCase;

import org.eclipse.osbp.ecview.extension.grid.CxGridCellStyleGenerator;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cell Style Generator</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CxGridCellStyleGeneratorTest extends TestCase {

	/**
	 * The fixture for this Cell Style Generator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridCellStyleGenerator fixture = null;

	/**
	 * Constructs a new Cell Style Generator test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CxGridCellStyleGeneratorTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Cell Style Generator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(CxGridCellStyleGenerator fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Cell Style Generator test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CxGridCellStyleGenerator getFixture() {
		return fixture;
	}

} //CxGridCellStyleGeneratorTest
