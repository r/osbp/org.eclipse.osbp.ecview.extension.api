/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.api;

/**
 * Implementation needs to return a "selector" String for the given dto. The
 * selector String is related with a single component contained in the context
 * sensitive layout.<br>
 * The component associated with the returned selector will become visible. All
 * other components will become invisible.
 */
public interface IContextSensitiveLayoutSelector {

	/**
	 * If implementations are using the function dsl, so this function group
	 * needs to be used.
	 */
	static final String FUNCTION_GROUP = "ContextSensitiveLayout";

	/**
	 * If implementations are using the function dsl, so this function name
	 * needs to be used. The input parameter for the dto needs to be Object in
	 * this version.  
	 * 
	 * TODO make function DSL allow same function name with different signature.
	 */
	static final String FUNCTION_NAME = "selectComponent";

	/**
	 * Needs to return a "selector" String for the given dto. The selector
	 * String is related with a single component contained in the context
	 * sensitive layout.<br>
	 * The component associated with the returned selector will become visible.
	 * All other components will become invisible.
	 * 
	 * @param dto
	 * @return
	 */
	String selectComponent(Object dto);

}
