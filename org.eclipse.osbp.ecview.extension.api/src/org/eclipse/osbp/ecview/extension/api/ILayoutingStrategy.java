/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.api;

import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;

/**
 * Is used to layout the given model properly.
 */
public interface ILayoutingStrategy {

	/** The Constant TAG__TEXT. */
	public static final String TAG__TEXT = "text";

	/** The Constant TAG__SUGGEST_TEXT. */
	public static final String TAG__SUGGEST_TEXT = "suggestText";

	/** The Constant TAG__TEXTAREA. */
	public static final String TAG__TEXTAREA = "textarea";
	
	/** The Constant TAG_SLIDER. */
	public static final String TAG__SLIDER = "slider";

	/** The Constant TAG__NUMBER. */
	public static final String TAG__NUMBER = "number";

	// autobinded
	public static final String TAG__AUTOBINDED = "autobinded";

	/**
	 * Attributes tagged with this property are being presented by a
	 * RichTextField using value-Binding (String).
	 */
	public final static String TAG__RICH_TEXT__STRING = "richText__String";

	/**
	 * Attributes tagged with this property are being presented by a
	 * MaskedTextField.
	 */
	public final static String TAG__MASKED_TEXT_FIELD = "maskedTextField";

	/**
	 * Attributes tagged with this property are being presented by a
	 * MaskedDecimalField.
	 */
	public final static String TAG__MASKED_DECIMAL_FIELD = "maskedDecimalField";

	/**
	 * Attributes tagged with this property are being presented by a
	 * RichTextField using blob-Value-Binding (byte[]).
	 */
	public final static String TAG__RICH_TEXT__BLOB = "richText__Blob";

	/**
	 * The property is visible on the kanban card.
	 */
	public final static String TAG__ON_KANBAN_CARD = "onKanbanCard";

	/**
	 * Attributes tagged with this property are being presented by a
	 * PasswordField using value-Binding (String).
	 */
	public final static String TAG__PASSWORD = "password";

	/** The Constant TAG__DECIMAL. */
	public static final String TAG__DECIMAL = "decimal";

	/** The Constant TAG__BOOLEAN. */
	public static final String TAG__BOOLEAN = "boolean";

	/** The Constant TAG__DATE. */
	public static final String TAG__DATE = "date";

	/** The Constant TAG__ENUM_OPTIONS. */
	public static final String TAG__ENUM_OPTIONS = "enumOptions";

	/** The Constant TAG_ENUM_COMBO. */
	public final static String TAG__ENUM_COMBO = "enumCombo";

	/** The Constant TAG__ORGANIZATION_COMBO. */
	public final static String TAG__ORGANIZATION_COMBO = "organizationCombo";

	/** The Constant TAG__PERSPECTIVE_COMBO. */
	public final static String TAG__PERSPECTIVE_COMBO = "perpectiveCombo";

	/** The Constant TAG__THEME_COMBO. */
	public final static String TAG__THEME_COMBO = "themeCombo";

	/** The Constant TAG__I18N_COMBO. */
	public final static String TAG__I18N_COMBO = "i18nCombo";

	/** The Constant TAG__PRINTSERVICE_COMBO. */
	public final static String TAG__PRINTSERVICE_COMBO = "printserviceCombo";

	/** The Constant TAG__PRINTSERVICE_COMBO. */
	public final static String TAG__MODEL_COMBO = "modelCombo";
	
	/** The Constant TAG__BLOB. */
	public static final String TAG__BLOB = "blob";

	/** The Constant TAG__BEAN_REFERENCE. */
	public static final String TAG__BEAN_REFERENCE = "beanReference";

	/** The Constant TAG__GRID. */
	public static final String TAG__GRID = "grid";

	/** The Constant TAG__TABLE. */
	public static final String TAG__TABLE = "table";

	/** The Constant TAG__GROUP. */
	public static final String TAG__GROUP = "group";

	/** The Constant TAG__EXTRASTYLE. */
	public static final String TAG__EXTRASTYLE = "extraStyle";
	
	/** The Constant TAG__BEAN. */
	public static final String TAG__BEAN = "bean";
	
	/** The Constant TAG__BEANONTAB. */
	public static final String TAG__BEANONTAB = "beanOnTab";
	
	/** The Constant TAG__CONSTRAINT. */
	public static final String TAG__CONSTRAINT = "constraint";

	/** The Constant TAG__UNIQUE. */
	public static final String TAG__UNIQUE = "unique";

	/** The Constant TAG__SAVEANDNEW. */
	public static final String TAG__SAVEANDNEW = "saveAndNew";

	/**
	 * The Constant TAG__DTO. Suspects of this type define suspects for a whole
	 * dto.
	 */
	public static final String TAG__DTO = "dto";

	/**
	 * The Constant TAG__SUBTYPE. <br>
	 * Suspects and beanslots of this type are DTO Suspects for sub classes of
	 * the main dto. Their visiblity needs to be controlled.
	 */
	public static final String TAG__SUBTYPE = "dto_subtype";

	/**
	 * The Constant TAG__SUBTYPES_ROOT. <br>
	 * Suspects of this type contain further compound suspects. One for each
	 * direct subclass.
	 */
	public static final String TAG__SUBTYPES_ROOT = "dto_subtypes_root";

	/**
	 * Model elements tagged with this tag need to be removed / disposed before
	 * next {@link #layout(YLayoutingInfo)}.
	 */
	public static final String TAG__TO_BE_CLEANED = "strategylayout__toBeCleaned";

	/** The Constant TAG__IMAGE_PICKER. 
	 * a combobox presentation with images is rendered*/
	public static final String TAG__IMAGE_PICKER = "imagePicker";

	/** The Constant TAG__READONLY. 
	 * the tagged item is read only. */
	public static final String TAG__READONLY = "readonly";

	/** The Constant TAG__ONKANBANCARD. 
	 * the tagged item is displayed on a kanban card*/
	public static final String TAG__ONKANBANCARD = "onKanbanCard";

	/** The Constant TAG__NUMBER_TO_UOMO. 
	 * the tagged item has Units Of Measurement functionality */
	public static final String TAG__NUMBER_TO_UOMO = "NumberToUomo";
	
	/**
	 * The possibility to create owners for bean references 
	 */
	public static final String TAG__SIDEKICK = "sideKick";

	/**
	 * Does the layouting for model.
	 *
	 * @param layoutingInfo
	 *            the layouting info
	 */
	void layout(YLayoutingInfo layoutingInfo);

}
