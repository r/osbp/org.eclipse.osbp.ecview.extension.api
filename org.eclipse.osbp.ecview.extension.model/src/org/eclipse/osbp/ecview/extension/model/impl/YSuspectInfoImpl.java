/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.binding.YBinding;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;
import org.eclipse.osbp.ecview.core.common.model.visibility.YVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YSuspect Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getSuspect <em>Suspect</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getBindings <em>Bindings</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getNextFocus <em>Next Focus</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getPreviousFocus <em>Previous Focus</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YSuspectInfoImpl#getVisibilityProcessors <em>Visibility Processors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YSuspectInfoImpl extends MinimalEObjectImpl.Container implements YSuspectInfo {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The cached value of the '{@link #getSuspect() <em>Suspect</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuspect()
	 * @generated
	 * @ordered
	 */
	protected YSuspect suspect;

	/**
	 * The cached value of the '{@link #getBindings() <em>Bindings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBindings()
	 * @generated
	 * @ordered
	 */
	protected EList<YBinding> bindings;

	/**
	 * The cached value of the '{@link #getNextFocus() <em>Next Focus</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextFocus()
	 * @generated
	 * @ordered
	 */
	protected YSuspectInfo nextFocus;

	/**
	 * The cached value of the '{@link #getPreviousFocus() <em>Previous Focus</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousFocus()
	 * @generated
	 * @ordered
	 */
	protected YSuspectInfo previousFocus;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected YEmbeddable target;

	/**
	 * The cached value of the '{@link #getVisibilityProcessors() <em>Visibility Processors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVisibilityProcessors()
	 * @generated
	 * @ordered
	 */
	protected EList<YVisibilityProcessor> visibilityProcessors;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YSuspectInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YECviewPackage.Literals.YSUSPECT_INFO;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getId() <em>Id</em>}' attribute
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newId
	 *            the new cached value of the '{@link #getId() <em>Id</em>}'
	 *            attribute
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getName() <em>Name</em>}'
	 *         attribute
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newName
	 *            the new cached value of the '{@link #getName() <em>Name</em>}'
	 *            attribute
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTags() <em>Tags</em>}'
	 *         attribute list
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, YECviewPackage.YSUSPECT_INFO__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getProperties()
	 *         <em>Properties</em>}' map
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, YECviewPackage.YSUSPECT_INFO__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getSuspect() <em>Suspect</em>}'
	 *         reference
	 * @generated
	 */
	public YSuspect getSuspect() {
		if (suspect != null && suspect.eIsProxy()) {
			InternalEObject oldSuspect = (InternalEObject)suspect;
			suspect = (YSuspect)eResolveProxy(oldSuspect);
			if (suspect != oldSuspect) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, YECviewPackage.YSUSPECT_INFO__SUSPECT, oldSuspect, suspect));
			}
		}
		return suspect;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect
	 * @generated
	 */
	public YSuspect basicGetSuspect() {
		return suspect;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newSuspect
	 *            the new cached value of the '{@link #getSuspect()
	 *            <em>Suspect</em>}' reference
	 * @generated
	 */
	public void setSuspect(YSuspect newSuspect) {
		YSuspect oldSuspect = suspect;
		suspect = newSuspect;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__SUSPECT, oldSuspect, suspect));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getBindings() <em>Bindings</em>}
	 *         ' containment reference list
	 * @generated
	 */
	public EList<YBinding> getBindings() {
		if (bindings == null) {
			bindings = new EObjectContainmentEList<YBinding>(YBinding.class, this, YECviewPackage.YSUSPECT_INFO__BINDINGS);
		}
		return bindings;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getNextFocus()
	 *         <em>Next Focus</em>}' reference
	 * @generated
	 */
	public YSuspectInfo getNextFocus() {
		if (nextFocus != null && nextFocus.eIsProxy()) {
			InternalEObject oldNextFocus = (InternalEObject)nextFocus;
			nextFocus = (YSuspectInfo)eResolveProxy(oldNextFocus);
			if (nextFocus != oldNextFocus) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS, oldNextFocus, nextFocus));
			}
		}
		return nextFocus;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info
	 * @generated
	 */
	public YSuspectInfo basicGetNextFocus() {
		return nextFocus;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newNextFocus
	 *            the new next focus
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetNextFocus(YSuspectInfo newNextFocus, NotificationChain msgs) {
		YSuspectInfo oldNextFocus = nextFocus;
		nextFocus = newNextFocus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS, oldNextFocus, newNextFocus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newNextFocus
	 *            the new cached value of the '{@link #getNextFocus()
	 *            <em>Next Focus</em>}' reference
	 * @generated
	 */
	public void setNextFocus(YSuspectInfo newNextFocus) {
		if (newNextFocus != nextFocus) {
			NotificationChain msgs = null;
			if (nextFocus != null)
				msgs = ((InternalEObject)nextFocus).eInverseRemove(this, YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS, YSuspectInfo.class, msgs);
			if (newNextFocus != null)
				msgs = ((InternalEObject)newNextFocus).eInverseAdd(this, YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS, YSuspectInfo.class, msgs);
			msgs = basicSetNextFocus(newNextFocus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS, newNextFocus, newNextFocus));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getPreviousFocus()
	 *         <em>Previous Focus</em>}' reference
	 * @generated
	 */
	public YSuspectInfo getPreviousFocus() {
		if (previousFocus != null && previousFocus.eIsProxy()) {
			InternalEObject oldPreviousFocus = (InternalEObject)previousFocus;
			previousFocus = (YSuspectInfo)eResolveProxy(oldPreviousFocus);
			if (previousFocus != oldPreviousFocus) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS, oldPreviousFocus, previousFocus));
			}
		}
		return previousFocus;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info
	 * @generated
	 */
	public YSuspectInfo basicGetPreviousFocus() {
		return previousFocus;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newPreviousFocus
	 *            the new previous focus
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetPreviousFocus(YSuspectInfo newPreviousFocus, NotificationChain msgs) {
		YSuspectInfo oldPreviousFocus = previousFocus;
		previousFocus = newPreviousFocus;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS, oldPreviousFocus, newPreviousFocus);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newPreviousFocus
	 *            the new cached value of the '{@link #getPreviousFocus()
	 *            <em>Previous Focus</em>}' reference
	 * @generated
	 */
	public void setPreviousFocus(YSuspectInfo newPreviousFocus) {
		if (newPreviousFocus != previousFocus) {
			NotificationChain msgs = null;
			if (previousFocus != null)
				msgs = ((InternalEObject)previousFocus).eInverseRemove(this, YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS, YSuspectInfo.class, msgs);
			if (newPreviousFocus != null)
				msgs = ((InternalEObject)newPreviousFocus).eInverseAdd(this, YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS, YSuspectInfo.class, msgs);
			msgs = basicSetPreviousFocus(newPreviousFocus, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS, newPreviousFocus, newPreviousFocus));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTarget() <em>Target</em>}'
	 *         reference
	 * @generated
	 */
	public YEmbeddable getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (YEmbeddable)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, YECviewPackage.YSUSPECT_INFO__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y embeddable
	 * @generated
	 */
	public YEmbeddable basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTarget
	 *            the new cached value of the '{@link #getTarget()
	 *            <em>Target</em>}' reference
	 * @generated
	 */
	public void setTarget(YEmbeddable newTarget) {
		YEmbeddable oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSUSPECT_INFO__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<YVisibilityProcessor> getVisibilityProcessors() {
		if (visibilityProcessors == null) {
			visibilityProcessors = new EObjectContainmentEList<YVisibilityProcessor>(YVisibilityProcessor.class, this, YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS);
		}
		return visibilityProcessors;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS:
				if (nextFocus != null)
					msgs = ((InternalEObject)nextFocus).eInverseRemove(this, YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS, YSuspectInfo.class, msgs);
				return basicSetNextFocus((YSuspectInfo)otherEnd, msgs);
			case YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS:
				if (previousFocus != null)
					msgs = ((InternalEObject)previousFocus).eInverseRemove(this, YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS, YSuspectInfo.class, msgs);
				return basicSetPreviousFocus((YSuspectInfo)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case YECviewPackage.YSUSPECT_INFO__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case YECviewPackage.YSUSPECT_INFO__BINDINGS:
				return ((InternalEList<?>)getBindings()).basicRemove(otherEnd, msgs);
			case YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS:
				return basicSetNextFocus(null, msgs);
			case YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS:
				return basicSetPreviousFocus(null, msgs);
			case YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS:
				return ((InternalEList<?>)getVisibilityProcessors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YECviewPackage.YSUSPECT_INFO__TAGS:
				return getTags();
			case YECviewPackage.YSUSPECT_INFO__ID:
				return getId();
			case YECviewPackage.YSUSPECT_INFO__NAME:
				return getName();
			case YECviewPackage.YSUSPECT_INFO__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case YECviewPackage.YSUSPECT_INFO__SUSPECT:
				if (resolve) return getSuspect();
				return basicGetSuspect();
			case YECviewPackage.YSUSPECT_INFO__BINDINGS:
				return getBindings();
			case YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS:
				if (resolve) return getNextFocus();
				return basicGetNextFocus();
			case YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS:
				if (resolve) return getPreviousFocus();
				return basicGetPreviousFocus();
			case YECviewPackage.YSUSPECT_INFO__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS:
				return getVisibilityProcessors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YECviewPackage.YSUSPECT_INFO__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__ID:
				setId((String)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__NAME:
				setName((String)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__SUSPECT:
				setSuspect((YSuspect)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__BINDINGS:
				getBindings().clear();
				getBindings().addAll((Collection<? extends YBinding>)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS:
				setNextFocus((YSuspectInfo)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS:
				setPreviousFocus((YSuspectInfo)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__TARGET:
				setTarget((YEmbeddable)newValue);
				return;
			case YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS:
				getVisibilityProcessors().clear();
				getVisibilityProcessors().addAll((Collection<? extends YVisibilityProcessor>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YECviewPackage.YSUSPECT_INFO__TAGS:
				getTags().clear();
				return;
			case YECviewPackage.YSUSPECT_INFO__ID:
				setId(ID_EDEFAULT);
				return;
			case YECviewPackage.YSUSPECT_INFO__NAME:
				setName(NAME_EDEFAULT);
				return;
			case YECviewPackage.YSUSPECT_INFO__PROPERTIES:
				getProperties().clear();
				return;
			case YECviewPackage.YSUSPECT_INFO__SUSPECT:
				setSuspect((YSuspect)null);
				return;
			case YECviewPackage.YSUSPECT_INFO__BINDINGS:
				getBindings().clear();
				return;
			case YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS:
				setNextFocus((YSuspectInfo)null);
				return;
			case YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS:
				setPreviousFocus((YSuspectInfo)null);
				return;
			case YECviewPackage.YSUSPECT_INFO__TARGET:
				setTarget((YEmbeddable)null);
				return;
			case YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS:
				getVisibilityProcessors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YECviewPackage.YSUSPECT_INFO__TAGS:
				return tags != null && !tags.isEmpty();
			case YECviewPackage.YSUSPECT_INFO__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case YECviewPackage.YSUSPECT_INFO__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case YECviewPackage.YSUSPECT_INFO__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case YECviewPackage.YSUSPECT_INFO__SUSPECT:
				return suspect != null;
			case YECviewPackage.YSUSPECT_INFO__BINDINGS:
				return bindings != null && !bindings.isEmpty();
			case YECviewPackage.YSUSPECT_INFO__NEXT_FOCUS:
				return nextFocus != null;
			case YECviewPackage.YSUSPECT_INFO__PREVIOUS_FOCUS:
				return previousFocus != null;
			case YECviewPackage.YSUSPECT_INFO__TARGET:
				return target != null;
			case YECviewPackage.YSUSPECT_INFO__VISIBILITY_PROCESSORS:
				return visibilityProcessors != null && !visibilityProcessors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 * @generated NOT
	 */
	public YView getView() {
		return findViewGeneric(eContainer());
	}

	/**
	 * Find view generic.
	 *
	 * @param container
	 *            the container
	 * @return the y view
	 */
	protected YView findViewGeneric(EObject container) {
		if (container == null) {
			return null;
		}
		if (container instanceof YView) {
			return (YView) container;
		} else if (container instanceof YLayout) {
			return ((YLayout) container).getView();
		} else {
			EObject parent = container.eContainer();
			return findViewGeneric(parent);
		}
	}

}
