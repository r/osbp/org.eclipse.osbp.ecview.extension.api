/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage
 * @generated
 */
public interface YConverterFactory extends EFactory {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	YConverterFactory eINSTANCE = org.eclipse.osbp.ecview.extension.model.converter.impl.YConverterFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>YObject To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YObject To String Converter</em>'.
	 * @generated
	 */
	YObjectToStringConverter createYObjectToStringConverter();

	/**
	 * Returns a new object of class '<em>YString To Byte Array Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YString To Byte Array Converter</em>'.
	 * @generated
	 */
	YStringToByteArrayConverter createYStringToByteArrayConverter();

	/**
	 * Returns a new object of class '<em>YCustom Decimal Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YCustom Decimal Converter</em>'.
	 * @generated
	 */
	YCustomDecimalConverter createYCustomDecimalConverter();

	/**
	 * Returns a new object of class '<em>YNumeric To Resource Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YNumeric To Resource Converter</em>'.
	 * @generated
	 */
	YNumericToResourceConverter createYNumericToResourceConverter();

	/**
	 * Returns a new object of class '<em>YString To Resource Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YString To Resource Converter</em>'.
	 * @generated
	 */
	YStringToResourceConverter createYStringToResourceConverter();

	/**
	 * Returns a new object of class '<em>YNumeric To Resource Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YNumeric To Resource Config</em>'.
	 * @generated
	 */
	YNumericToResourceConfig createYNumericToResourceConfig();

	/**
	 * Returns a new object of class '<em>YString To Resource Config</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YString To Resource Config</em>'.
	 * @generated
	 */
	YStringToResourceConfig createYStringToResourceConfig();

	/**
	 * Returns a new object of class '<em>YNumeric To Uomo Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YNumeric To Uomo Converter</em>'.
	 * @generated
	 */
	YNumericToUomoConverter createYNumericToUomoConverter();

	/**
	 * Returns a new object of class '<em>YDecimal To Uomo Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YDecimal To Uomo Converter</em>'.
	 * @generated
	 */
	YDecimalToUomoConverter createYDecimalToUomoConverter();

	/**
	 * Returns a new object of class '<em>YSimple Decimal Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YSimple Decimal Converter</em>'.
	 * @generated
	 */
	YSimpleDecimalConverter createYSimpleDecimalConverter();

	/**
	 * Returns a new object of class '<em>YVaaclipse Ui Theme To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YVaaclipse Ui Theme To String Converter</em>'.
	 * @generated
	 */
	YVaaclipseUiThemeToStringConverter createYVaaclipseUiThemeToStringConverter();

	/**
	 * Returns a new object of class '<em>YPrice To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YPrice To String Converter</em>'.
	 * @generated
	 */
	YPriceToStringConverter createYPriceToStringConverter();

	/**
	 * Returns a new object of class '<em>YQuantity To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YQuantity To String Converter</em>'.
	 * @generated
	 */
	YQuantityToStringConverter createYQuantityToStringConverter();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	YConverterPackage getYConverterPackage();

}
