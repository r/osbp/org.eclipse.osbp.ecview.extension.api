/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage
 * @generated
 */
public interface YECviewFactory extends EFactory {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	YECviewFactory eINSTANCE = org.eclipse.osbp.ecview.extension.model.impl.YECviewFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>YStrategy Layout</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YStrategy Layout</em>'.
	 * @generated
	 */
	YStrategyLayout createYStrategyLayout();

	/**
	 * Returns a new object of class '<em>YLayouting Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YLayouting Strategy</em>'.
	 * @generated
	 */
	YLayoutingStrategy createYLayoutingStrategy();

	/**
	 * Returns a new object of class '<em>YDefault Layouting Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YDefault Layouting Strategy</em>'.
	 * @generated
	 */
	YDefaultLayoutingStrategy createYDefaultLayoutingStrategy();

	/**
	 * Returns a new object of class '<em>YFocusing Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YFocusing Strategy</em>'.
	 * @generated
	 */
	YFocusingStrategy createYFocusingStrategy();

	/**
	 * Returns a new object of class '<em>YDelegating Layouting Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YDelegating Layouting Strategy</em>'.
	 * @generated
	 */
	YDelegatingLayoutingStrategy createYDelegatingLayoutingStrategy();

	/**
	 * Returns a new object of class '<em>YDelegating Focusing Strategy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YDelegating Focusing Strategy</em>'.
	 * @generated
	 */
	YDelegatingFocusingStrategy createYDelegatingFocusingStrategy();

	/**
	 * Returns a new object of class '<em>YSuspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YSuspect</em>'.
	 * @generated
	 */
	YSuspect createYSuspect();

	/**
	 * Returns a new object of class '<em>YLayouting Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YLayouting Info</em>'.
	 * @generated
	 */
	YLayoutingInfo createYLayoutingInfo();

	/**
	 * Returns a new object of class '<em>YSuspect Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YSuspect Info</em>'.
	 * @generated
	 */
	YSuspectInfo createYSuspectInfo();

	/**
	 * Returns a new object of class '<em>YBlob Upload Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YBlob Upload Component</em>'.
	 * @generated
	 */
	YBlobUploadComponent createYBlobUploadComponent();

	/**
	 * Returns a new object of class '<em>YTyped Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YTyped Suspect</em>'.
	 * @generated
	 */
	YTypedSuspect createYTypedSuspect();

	/**
	 * Returns a new object of class '<em>YTyped Compound Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YTyped Compound Suspect</em>'.
	 * @generated
	 */
	YTypedCompoundSuspect createYTypedCompoundSuspect();

	/**
	 * Returns a new object of class '<em>YSub Type Base Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YSub Type Base Suspect</em>'.
	 * @generated
	 */
	YSubTypeBaseSuspect createYSubTypeBaseSuspect();

	/**
	 * Returns a new object of class '<em>YSub Type Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YSub Type Suspect</em>'.
	 * @generated
	 */
	YSubTypeSuspect createYSubTypeSuspect();

	/**
	 * Returns a new object of class '<em>YCustom Decimal Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YCustom Decimal Field</em>'.
	 * @generated
	 */
	YCustomDecimalField createYCustomDecimalField();

	/**
	 * Returns a new object of class '<em>YI1 8n Combo Box</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YI1 8n Combo Box</em>'.
	 * @generated
	 */
	YI18nComboBox createYI18nComboBox();

	/**
	 * Returns a new object of class '<em>YIcon Combo Box</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YIcon Combo Box</em>'.
	 * @generated
	 */
	YIconComboBox createYIconComboBox();

	/**
	 * Returns a new object of class '<em>YQuantity Text Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YQuantity Text Field</em>'.
	 * @generated
	 */
	YQuantityTextField createYQuantityTextField();

	/**
	 * Returns a new object of class '<em>YCollection Suspect</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YCollection Suspect</em>'.
	 * @generated
	 */
	YCollectionSuspect createYCollectionSuspect();

	/**
	 * Returns a new object of class '<em>YColumn Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YColumn Info</em>'.
	 * @generated
	 */
	YColumnInfo createYColumnInfo();

	/**
	 * Returns a new object of class '<em>YContent Sensitive Layout</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YContent Sensitive Layout</em>'.
	 * @generated
	 */
	YContentSensitiveLayout createYContentSensitiveLayout();

	/**
	 * Returns a new object of class '<em>YRich Text Area</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YRich Text Area</em>'.
	 * @generated
	 */
	YRichTextArea createYRichTextArea();

	/**
	 * Returns a new object of class '<em>YMasked Text Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YMasked Text Field</em>'.
	 * @generated
	 */
	YMaskedTextField createYMaskedTextField();

	/**
	 * Returns a new object of class '<em>YPrefixed Masked Text Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YPrefixed Masked Text Field</em>'.
	 * @generated
	 */
	YPrefixedMaskedTextField createYPrefixedMaskedTextField();

	/**
	 * Returns a new object of class '<em>YMasked Numeric Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YMasked Numeric Field</em>'.
	 * @generated
	 */
	YMaskedNumericField createYMaskedNumericField();

	/**
	 * Returns a new object of class '<em>YMasked Decimal Field</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YMasked Decimal Field</em>'.
	 * @generated
	 */
	YMaskedDecimalField createYMaskedDecimalField();

	/**
	 * Returns a new object of class '<em>YPair Combo Box</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>YPair Combo Box</em>'.
	 * @generated
	 */
	YPairComboBox createYPairComboBox();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	YECviewPackage getYECviewPackage();

}
