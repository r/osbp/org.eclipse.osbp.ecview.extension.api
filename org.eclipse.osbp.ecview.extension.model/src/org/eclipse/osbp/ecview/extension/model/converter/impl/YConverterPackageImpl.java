/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.ExtDatatypesPackage;
import org.eclipse.osbp.ecview.core.extension.model.extension.ExtensionModelPackage;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YObjectToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToByteArrayConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YVaaclipseUiThemeToStringConverter;
import org.eclipse.osbp.ecview.extension.model.impl.YECviewPackageImpl;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage;
import org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class YConverterPackageImpl extends EPackageImpl implements YConverterPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yObjectToStringConverterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yStringToByteArrayConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yCustomDecimalConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yNumericToResourceConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yStringToResourceConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yNumericToResourceConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yStringToResourceConfigEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yNumericToUomoConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yDecimalToUomoConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass ySimpleDecimalConverterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yVaaclipseUiThemeToStringConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yPriceToStringConverterEClass = null;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private EClass yQuantityToStringConverterEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private YConverterPackageImpl() {
		super(eNS_URI, YConverterFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model,
	 * and for any others upon which it depends.
	 * 
	 * <p>
	 * This method is used to initialize {@link YConverterPackage#eINSTANCE}
	 * when that field is accessed. Clients should not invoke it directly.
	 * Instead, they should simply access that field to obtain the package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @return the y converter package
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static YConverterPackage init() {
		if (isInited) return (YConverterPackage)EPackage.Registry.INSTANCE.getEPackage(YConverterPackage.eNS_URI);

		// Obtain or create and register package
		YConverterPackageImpl theYConverterPackage = (YConverterPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof YConverterPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new YConverterPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		ExtDatatypesPackage.eINSTANCE.eClass();
		ExtensionModelPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		YECviewPackageImpl theYECviewPackage = (YECviewPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(YECviewPackage.eNS_URI) instanceof YECviewPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(YECviewPackage.eNS_URI) : YECviewPackage.eINSTANCE);
		YVisibilityPackageImpl theYVisibilityPackage = (YVisibilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(YVisibilityPackage.eNS_URI) instanceof YVisibilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(YVisibilityPackage.eNS_URI) : YVisibilityPackage.eINSTANCE);

		// Create package meta-data objects
		theYConverterPackage.createPackageContents();
		theYECviewPackage.createPackageContents();
		theYVisibilityPackage.createPackageContents();

		// Initialize created meta-data
		theYConverterPackage.initializePackageContents();
		theYECviewPackage.initializePackageContents();
		theYVisibilityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theYConverterPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(YConverterPackage.eNS_URI, theYConverterPackage);
		return theYConverterPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y object to string converter
	 * @generated
	 */
	public EClass getYObjectToStringConverter() {
		return yObjectToStringConverterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYStringToByteArrayConverter() {
		return yStringToByteArrayConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal converter
	 * @generated
	 */
	public EClass getYCustomDecimalConverter() {
		return yCustomDecimalConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal converter_ base unit
	 * @generated
	 */
	public EAttribute getYCustomDecimalConverter_BaseUnit() {
		return (EAttribute)yCustomDecimalConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource converter
	 * @generated
	 */
	public EClass getYNumericToResourceConverter() {
		return yNumericToResourceConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource converter_ configs
	 * @generated
	 */
	public EReference getYNumericToResourceConverter_Configs() {
		return (EReference)yNumericToResourceConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource converter
	 * @generated
	 */
	public EClass getYStringToResourceConverter() {
		return yStringToResourceConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource converter_ configs
	 * @generated
	 */
	public EReference getYStringToResourceConverter_Configs() {
		return (EReference)yStringToResourceConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource config
	 * @generated
	 */
	public EClass getYNumericToResourceConfig() {
		return yNumericToResourceConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource config_ value
	 * @generated
	 */
	public EAttribute getYNumericToResourceConfig_Value() {
		return (EAttribute)yNumericToResourceConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource config_ compare
	 * @generated
	 */
	public EAttribute getYNumericToResourceConfig_Compare() {
		return (EAttribute)yNumericToResourceConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource config_ resource theme path
	 * @generated
	 */
	public EAttribute getYNumericToResourceConfig_ResourceThemePath() {
		return (EAttribute)yNumericToResourceConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource config
	 * @generated
	 */
	public EClass getYStringToResourceConfig() {
		return yStringToResourceConfigEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource config_ value
	 * @generated
	 */
	public EAttribute getYStringToResourceConfig_Value() {
		return (EAttribute)yStringToResourceConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource config_ compare
	 * @generated
	 */
	public EAttribute getYStringToResourceConfig_Compare() {
		return (EAttribute)yStringToResourceConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource config_ resource theme path
	 * @generated
	 */
	public EAttribute getYStringToResourceConfig_ResourceThemePath() {
		return (EAttribute)yStringToResourceConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to uomo converter
	 * @generated
	 */
	public EClass getYNumericToUomoConverter() {
		return yNumericToUomoConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal to uomo converter
	 * @generated
	 */
	public EClass getYDecimalToUomoConverter() {
		return yDecimalToUomoConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y simple decimal converter
	 * @generated
	 */
	public EClass getYSimpleDecimalConverter() {
		return ySimpleDecimalConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y simple decimal converter_ number format pattern
	 * @generated
	 */
	public EAttribute getYSimpleDecimalConverter_NumberFormatPattern() {
		return (EAttribute)ySimpleDecimalConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYVaaclipseUiThemeToStringConverter() {
		return yVaaclipseUiThemeToStringConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y price to string converter
	 * @generated
	 */
	public EClass getYPriceToStringConverter() {
		return yPriceToStringConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y price to string converter_ value property path
	 * @generated
	 */
	public EAttribute getYPriceToStringConverter_ValuePropertyPath() {
		return (EAttribute)yPriceToStringConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y price to string converter_ currency property path
	 * @generated
	 */
	public EAttribute getYPriceToStringConverter_CurrencyPropertyPath() {
		return (EAttribute)yPriceToStringConverterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y price to string converter_ type qualified name
	 * @generated
	 */
	public EAttribute getYPriceToStringConverter_TypeQualifiedName() {
		return (EAttribute)yPriceToStringConverterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y price to string converter_ type
	 * @generated
	 */
	public EAttribute getYPriceToStringConverter_Type() {
		return (EAttribute)yPriceToStringConverterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity to string converter
	 * @generated
	 */
	public EClass getYQuantityToStringConverter() {
		return yQuantityToStringConverterEClass;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity to string converter_ amount property path
	 * @generated
	 */
	public EAttribute getYQuantityToStringConverter_AmountPropertyPath() {
		return (EAttribute)yQuantityToStringConverterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity to string converter_ uom property path
	 * @generated
	 */
	public EAttribute getYQuantityToStringConverter_UomPropertyPath() {
		return (EAttribute)yQuantityToStringConverterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity to string converter_ uom code relative property
	 *         path
	 * @generated
	 */
	public EAttribute getYQuantityToStringConverter_UomCodeRelativePropertyPath() {
		return (EAttribute)yQuantityToStringConverterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity to string converter_ quantity type qualified name
	 * @generated
	 */
	public EAttribute getYQuantityToStringConverter_QuantityTypeQualifiedName() {
		return (EAttribute)yQuantityToStringConverterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y converter factory
	 * @generated
	 */
	public YConverterFactory getYConverterFactory() {
		return (YConverterFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		yObjectToStringConverterEClass = createEClass(YOBJECT_TO_STRING_CONVERTER);

		yStringToByteArrayConverterEClass = createEClass(YSTRING_TO_BYTE_ARRAY_CONVERTER);

		yCustomDecimalConverterEClass = createEClass(YCUSTOM_DECIMAL_CONVERTER);
		createEAttribute(yCustomDecimalConverterEClass, YCUSTOM_DECIMAL_CONVERTER__BASE_UNIT);

		yNumericToResourceConverterEClass = createEClass(YNUMERIC_TO_RESOURCE_CONVERTER);
		createEReference(yNumericToResourceConverterEClass, YNUMERIC_TO_RESOURCE_CONVERTER__CONFIGS);

		yStringToResourceConverterEClass = createEClass(YSTRING_TO_RESOURCE_CONVERTER);
		createEReference(yStringToResourceConverterEClass, YSTRING_TO_RESOURCE_CONVERTER__CONFIGS);

		yNumericToResourceConfigEClass = createEClass(YNUMERIC_TO_RESOURCE_CONFIG);
		createEAttribute(yNumericToResourceConfigEClass, YNUMERIC_TO_RESOURCE_CONFIG__VALUE);
		createEAttribute(yNumericToResourceConfigEClass, YNUMERIC_TO_RESOURCE_CONFIG__COMPARE);
		createEAttribute(yNumericToResourceConfigEClass, YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH);

		yStringToResourceConfigEClass = createEClass(YSTRING_TO_RESOURCE_CONFIG);
		createEAttribute(yStringToResourceConfigEClass, YSTRING_TO_RESOURCE_CONFIG__VALUE);
		createEAttribute(yStringToResourceConfigEClass, YSTRING_TO_RESOURCE_CONFIG__COMPARE);
		createEAttribute(yStringToResourceConfigEClass, YSTRING_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH);

		yPriceToStringConverterEClass = createEClass(YPRICE_TO_STRING_CONVERTER);
		createEAttribute(yPriceToStringConverterEClass, YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH);
		createEAttribute(yPriceToStringConverterEClass, YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH);
		createEAttribute(yPriceToStringConverterEClass, YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME);
		createEAttribute(yPriceToStringConverterEClass, YPRICE_TO_STRING_CONVERTER__TYPE);

		yQuantityToStringConverterEClass = createEClass(YQUANTITY_TO_STRING_CONVERTER);
		createEAttribute(yQuantityToStringConverterEClass, YQUANTITY_TO_STRING_CONVERTER__AMOUNT_PROPERTY_PATH);
		createEAttribute(yQuantityToStringConverterEClass, YQUANTITY_TO_STRING_CONVERTER__UOM_PROPERTY_PATH);
		createEAttribute(yQuantityToStringConverterEClass, YQUANTITY_TO_STRING_CONVERTER__UOM_CODE_RELATIVE_PROPERTY_PATH);
		createEAttribute(yQuantityToStringConverterEClass, YQUANTITY_TO_STRING_CONVERTER__QUANTITY_TYPE_QUALIFIED_NAME);

		yNumericToUomoConverterEClass = createEClass(YNUMERIC_TO_UOMO_CONVERTER);

		yDecimalToUomoConverterEClass = createEClass(YDECIMAL_TO_UOMO_CONVERTER);

		ySimpleDecimalConverterEClass = createEClass(YSIMPLE_DECIMAL_CONVERTER);
		createEAttribute(ySimpleDecimalConverterEClass, YSIMPLE_DECIMAL_CONVERTER__NUMBER_FORMAT_PATTERN);

		yVaaclipseUiThemeToStringConverterEClass = createEClass(YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CoreModelPackage theCoreModelPackage = (CoreModelPackage)EPackage.Registry.INSTANCE.getEPackage(CoreModelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		yObjectToStringConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yStringToByteArrayConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yCustomDecimalConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yNumericToResourceConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yStringToResourceConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yPriceToStringConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yQuantityToStringConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yNumericToUomoConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yDecimalToUomoConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		ySimpleDecimalConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());
		yVaaclipseUiThemeToStringConverterEClass.getESuperTypes().add(theCoreModelPackage.getYConverter());

		// Initialize classes and features; add operations and parameters
		initEClass(yObjectToStringConverterEClass, YObjectToStringConverter.class, "YObjectToStringConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yStringToByteArrayConverterEClass, YStringToByteArrayConverter.class, "YStringToByteArrayConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yCustomDecimalConverterEClass, YCustomDecimalConverter.class, "YCustomDecimalConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYCustomDecimalConverter_BaseUnit(), ecorePackage.getEString(), "baseUnit", null, 0, 1, YCustomDecimalConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yNumericToResourceConverterEClass, YNumericToResourceConverter.class, "YNumericToResourceConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYNumericToResourceConverter_Configs(), this.getYNumericToResourceConfig(), null, "configs", null, 0, -1, YNumericToResourceConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yStringToResourceConverterEClass, YStringToResourceConverter.class, "YStringToResourceConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getYStringToResourceConverter_Configs(), this.getYStringToResourceConfig(), null, "configs", null, 0, -1, YStringToResourceConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yNumericToResourceConfigEClass, YNumericToResourceConfig.class, "YNumericToResourceConfig", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYNumericToResourceConfig_Value(), ecorePackage.getEDouble(), "value", null, 0, 1, YNumericToResourceConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericToResourceConfig_Compare(), theCoreModelPackage.getYCompare(), "compare", null, 0, 1, YNumericToResourceConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYNumericToResourceConfig_ResourceThemePath(), ecorePackage.getEString(), "resourceThemePath", null, 0, 1, YNumericToResourceConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yStringToResourceConfigEClass, YStringToResourceConfig.class, "YStringToResourceConfig", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYStringToResourceConfig_Value(), ecorePackage.getEString(), "value", null, 0, 1, YStringToResourceConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYStringToResourceConfig_Compare(), theCoreModelPackage.getYCompare(), "compare", null, 0, 1, YStringToResourceConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYStringToResourceConfig_ResourceThemePath(), ecorePackage.getEString(), "resourceThemePath", null, 0, 1, YStringToResourceConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yPriceToStringConverterEClass, YPriceToStringConverter.class, "YPriceToStringConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYPriceToStringConverter_ValuePropertyPath(), ecorePackage.getEString(), "valuePropertyPath", null, 1, 1, YPriceToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPriceToStringConverter_CurrencyPropertyPath(), ecorePackage.getEString(), "currencyPropertyPath", null, 1, 1, YPriceToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYPriceToStringConverter_TypeQualifiedName(), ecorePackage.getEString(), "typeQualifiedName", null, 0, 1, YPriceToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEJavaClass());
		EGenericType g2 = createEGenericType();
		g1.getETypeArguments().add(g2);
		initEAttribute(getYPriceToStringConverter_Type(), g1, "type", null, 0, 1, YPriceToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yQuantityToStringConverterEClass, YQuantityToStringConverter.class, "YQuantityToStringConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYQuantityToStringConverter_AmountPropertyPath(), ecorePackage.getEString(), "amountPropertyPath", null, 1, 1, YQuantityToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYQuantityToStringConverter_UomPropertyPath(), ecorePackage.getEString(), "uomPropertyPath", null, 1, 1, YQuantityToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYQuantityToStringConverter_UomCodeRelativePropertyPath(), ecorePackage.getEString(), "uomCodeRelativePropertyPath", null, 1, 1, YQuantityToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getYQuantityToStringConverter_QuantityTypeQualifiedName(), ecorePackage.getEString(), "quantityTypeQualifiedName", null, 0, 1, YQuantityToStringConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yNumericToUomoConverterEClass, YNumericToUomoConverter.class, "YNumericToUomoConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(yDecimalToUomoConverterEClass, YDecimalToUomoConverter.class, "YDecimalToUomoConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ySimpleDecimalConverterEClass, YSimpleDecimalConverter.class, "YSimpleDecimalConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getYSimpleDecimalConverter_NumberFormatPattern(), ecorePackage.getEString(), "numberFormatPattern", null, 0, 1, YSimpleDecimalConverter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yVaaclipseUiThemeToStringConverterEClass, YVaaclipseUiThemeToStringConverter.class, "YVaaclipseUiThemeToStringConverter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
	}

}
