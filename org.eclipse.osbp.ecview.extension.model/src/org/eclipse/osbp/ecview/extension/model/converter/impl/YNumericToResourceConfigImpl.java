/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.osbp.ecview.core.common.model.core.YCompare;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YNumeric To Resource Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl#getCompare <em>Compare</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YNumericToResourceConfigImpl#getResourceThemePath <em>Resource Theme Path</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YNumericToResourceConfigImpl extends MinimalEObjectImpl.Container implements YNumericToResourceConfig {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final double VALUE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected double value = VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getCompare() <em>Compare</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompare()
	 * @generated
	 * @ordered
	 */
	protected static final YCompare COMPARE_EDEFAULT = YCompare.EQUAL;

	/**
	 * The cached value of the '{@link #getCompare() <em>Compare</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompare()
	 * @generated
	 * @ordered
	 */
	protected YCompare compare = COMPARE_EDEFAULT;

	/**
	 * The default value of the '{@link #getResourceThemePath() <em>Resource Theme Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceThemePath()
	 * @generated
	 * @ordered
	 */
	protected static final String RESOURCE_THEME_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getResourceThemePath() <em>Resource Theme Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceThemePath()
	 * @generated
	 * @ordered
	 */
	protected String resourceThemePath = RESOURCE_THEME_PATH_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YNumericToResourceConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YConverterPackage.Literals.YNUMERIC_TO_RESOURCE_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getValue() <em>Value</em>}'
	 *         attribute
	 * @generated
	 */
	public double getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newValue
	 *            the new cached value of the '{@link #getValue()
	 *            <em>Value</em>}' attribute
	 * @generated
	 */
	public void setValue(double newValue) {
		double oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__VALUE, oldValue, value));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getCompare() <em>Compare</em>}'
	 *         attribute
	 * @generated
	 */
	public YCompare getCompare() {
		return compare;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newCompare
	 *            the new cached value of the '{@link #getCompare()
	 *            <em>Compare</em>}' attribute
	 * @generated
	 */
	public void setCompare(YCompare newCompare) {
		YCompare oldCompare = compare;
		compare = newCompare == null ? COMPARE_EDEFAULT : newCompare;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__COMPARE, oldCompare, compare));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getResourceThemePath()
	 *         <em>Resource Theme Path</em>}' attribute
	 * @generated
	 */
	public String getResourceThemePath() {
		return resourceThemePath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newResourceThemePath
	 *            the new cached value of the '{@link #getResourceThemePath()
	 *            <em>Resource Theme Path</em>}' attribute
	 * @generated
	 */
	public void setResourceThemePath(String newResourceThemePath) {
		String oldResourceThemePath = resourceThemePath;
		resourceThemePath = newResourceThemePath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH, oldResourceThemePath, resourceThemePath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__VALUE:
				return getValue();
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__COMPARE:
				return getCompare();
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH:
				return getResourceThemePath();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__VALUE:
				setValue((Double)newValue);
				return;
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__COMPARE:
				setCompare((YCompare)newValue);
				return;
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH:
				setResourceThemePath((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__VALUE:
				setValue(VALUE_EDEFAULT);
				return;
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__COMPARE:
				setCompare(COMPARE_EDEFAULT);
				return;
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH:
				setResourceThemePath(RESOURCE_THEME_PATH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__VALUE:
				return value != VALUE_EDEFAULT;
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__COMPARE:
				return compare != COMPARE_EDEFAULT;
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG__RESOURCE_THEME_PATH:
				return RESOURCE_THEME_PATH_EDEFAULT == null ? resourceThemePath != null : !RESOURCE_THEME_PATH_EDEFAULT.equals(resourceThemePath);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		result.append(value);
		result.append(", compare: ");
		result.append(compare);
		result.append(", resourceThemePath: ");
		result.append(resourceThemePath);
		result.append(')');
		return result.toString();
	}

}
