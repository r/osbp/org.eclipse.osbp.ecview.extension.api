/**
 * All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.
 * 
 * Contributors:
 *       Florian Pirchner - initial API and implementation
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YVaaclipse Ui Theme To String Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYVaaclipseUiThemeToStringConverter()
 * @model
 * @generated
 */
public interface YVaaclipseUiThemeToStringConverter extends YConverter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

} // YVaaclipseUiThemeToStringConverter
