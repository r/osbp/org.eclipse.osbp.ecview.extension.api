/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.visibility.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityFactory;
import org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class YVisibilityFactoryImpl extends EFactoryImpl implements YVisibilityFactory {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static YVisibilityFactory init() {
		try {
			YVisibilityFactory theYVisibilityFactory = (YVisibilityFactory)EPackage.Registry.INSTANCE.getEFactory(YVisibilityPackage.eNS_URI);
			if (theYVisibilityFactory != null) {
				return theYVisibilityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new YVisibilityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVisibilityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case YVisibilityPackage.YAUTHORIZATION_VISIBILITY_PROCESSOR: return createYAuthorizationVisibilityProcessor();
			case YVisibilityPackage.YSUB_TYPE_VISIBILITY_PROCESSOR: return createYSubTypeVisibilityProcessor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y authorization visibility processor
	 * @generated
	 */
	public YAuthorizationVisibilityProcessor createYAuthorizationVisibilityProcessor() {
		YAuthorizationVisibilityProcessorImpl yAuthorizationVisibilityProcessor = new YAuthorizationVisibilityProcessorImpl();
		return yAuthorizationVisibilityProcessor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSubTypeVisibilityProcessor createYSubTypeVisibilityProcessor() {
		YSubTypeVisibilityProcessorImpl ySubTypeVisibilityProcessor = new YSubTypeVisibilityProcessorImpl();
		return ySubTypeVisibilityProcessor;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y visibility package
	 * @generated
	 */
	public YVisibilityPackage getYVisibilityPackage() {
		return (YVisibilityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static YVisibilityPackage getPackage() {
		return YVisibilityPackage.eINSTANCE;
	}

}
