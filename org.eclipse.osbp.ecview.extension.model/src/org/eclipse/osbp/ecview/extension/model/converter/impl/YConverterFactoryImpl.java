/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.extension.model.converter.*;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterFactory;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YObjectToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class YConverterFactoryImpl extends EFactoryImpl implements YConverterFactory {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static YConverterFactory init() {
		try {
			YConverterFactory theYConverterFactory = (YConverterFactory)EPackage.Registry.INSTANCE.getEFactory(YConverterPackage.eNS_URI);
			if (theYConverterFactory != null) {
				return theYConverterFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new YConverterFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YConverterFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case YConverterPackage.YOBJECT_TO_STRING_CONVERTER: return createYObjectToStringConverter();
			case YConverterPackage.YSTRING_TO_BYTE_ARRAY_CONVERTER: return createYStringToByteArrayConverter();
			case YConverterPackage.YCUSTOM_DECIMAL_CONVERTER: return createYCustomDecimalConverter();
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONVERTER: return createYNumericToResourceConverter();
			case YConverterPackage.YSTRING_TO_RESOURCE_CONVERTER: return createYStringToResourceConverter();
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG: return createYNumericToResourceConfig();
			case YConverterPackage.YSTRING_TO_RESOURCE_CONFIG: return createYStringToResourceConfig();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER: return createYPriceToStringConverter();
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER: return createYQuantityToStringConverter();
			case YConverterPackage.YNUMERIC_TO_UOMO_CONVERTER: return createYNumericToUomoConverter();
			case YConverterPackage.YDECIMAL_TO_UOMO_CONVERTER: return createYDecimalToUomoConverter();
			case YConverterPackage.YSIMPLE_DECIMAL_CONVERTER: return createYSimpleDecimalConverter();
			case YConverterPackage.YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER: return createYVaaclipseUiThemeToStringConverter();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y object to string converter
	 * @generated
	 */
	public YObjectToStringConverter createYObjectToStringConverter() {
		YObjectToStringConverterImpl yObjectToStringConverter = new YObjectToStringConverterImpl();
		return yObjectToStringConverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YStringToByteArrayConverter createYStringToByteArrayConverter() {
		YStringToByteArrayConverterImpl yStringToByteArrayConverter = new YStringToByteArrayConverterImpl();
		return yStringToByteArrayConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal converter
	 * @generated
	 */
	public YCustomDecimalConverter createYCustomDecimalConverter() {
		YCustomDecimalConverterImpl yCustomDecimalConverter = new YCustomDecimalConverterImpl();
		return yCustomDecimalConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource converter
	 * @generated
	 */
	public YNumericToResourceConverter createYNumericToResourceConverter() {
		YNumericToResourceConverterImpl yNumericToResourceConverter = new YNumericToResourceConverterImpl();
		return yNumericToResourceConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource converter
	 * @generated
	 */
	public YStringToResourceConverter createYStringToResourceConverter() {
		YStringToResourceConverterImpl yStringToResourceConverter = new YStringToResourceConverterImpl();
		return yStringToResourceConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to resource config
	 * @generated
	 */
	public YNumericToResourceConfig createYNumericToResourceConfig() {
		YNumericToResourceConfigImpl yNumericToResourceConfig = new YNumericToResourceConfigImpl();
		return yNumericToResourceConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y string to resource config
	 * @generated
	 */
	public YStringToResourceConfig createYStringToResourceConfig() {
		YStringToResourceConfigImpl yStringToResourceConfig = new YStringToResourceConfigImpl();
		return yStringToResourceConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y numeric to uomo converter
	 * @generated
	 */
	public YNumericToUomoConverter createYNumericToUomoConverter() {
		YNumericToUomoConverterImpl yNumericToUomoConverter = new YNumericToUomoConverterImpl();
		return yNumericToUomoConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y decimal to uomo converter
	 * @generated
	 */
	public YDecimalToUomoConverter createYDecimalToUomoConverter() {
		YDecimalToUomoConverterImpl yDecimalToUomoConverter = new YDecimalToUomoConverterImpl();
		return yDecimalToUomoConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y simple decimal converter
	 * @generated
	 */
	public YSimpleDecimalConverter createYSimpleDecimalConverter() {
		YSimpleDecimalConverterImpl ySimpleDecimalConverter = new YSimpleDecimalConverterImpl();
		return ySimpleDecimalConverter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YVaaclipseUiThemeToStringConverter createYVaaclipseUiThemeToStringConverter() {
		YVaaclipseUiThemeToStringConverterImpl yVaaclipseUiThemeToStringConverter = new YVaaclipseUiThemeToStringConverterImpl();
		return yVaaclipseUiThemeToStringConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y price to string converter
	 * @generated
	 */
	public YPriceToStringConverter createYPriceToStringConverter() {
		YPriceToStringConverterImpl yPriceToStringConverter = new YPriceToStringConverterImpl();
		return yPriceToStringConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity to string converter
	 * @generated
	 */
	public YQuantityToStringConverter createYQuantityToStringConverter() {
		YQuantityToStringConverterImpl yQuantityToStringConverter = new YQuantityToStringConverterImpl();
		return yQuantityToStringConverter;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y converter package
	 * @generated
	 */
	public YConverterPackage getYConverterPackage() {
		return (YConverterPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static YConverterPackage getPackage() {
		return YConverterPackage.eINSTANCE;
	}

}
