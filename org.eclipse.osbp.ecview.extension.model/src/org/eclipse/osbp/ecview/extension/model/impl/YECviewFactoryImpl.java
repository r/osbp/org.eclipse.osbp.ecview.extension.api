/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.osbp.ecview.extension.model.*;
import org.eclipse.osbp.ecview.extension.model.YBlobUploadComponent;
import org.eclipse.osbp.ecview.extension.model.YCollectionSuspect;
import org.eclipse.osbp.ecview.extension.model.YColumnInfo;
import org.eclipse.osbp.ecview.extension.model.YContentSensitiveLayout;
import org.eclipse.osbp.ecview.extension.model.YCustomDecimalField;
import org.eclipse.osbp.ecview.extension.model.YDefaultLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YDelegatingLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YECviewFactory;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YIconComboBox;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField;
import org.eclipse.osbp.ecview.extension.model.YMaskedNumericField;
import org.eclipse.osbp.ecview.extension.model.YMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YPairComboBox;
import org.eclipse.osbp.ecview.extension.model.YPrefixedMaskedTextField;
import org.eclipse.osbp.ecview.extension.model.YQuantityTextField;
import org.eclipse.osbp.ecview.extension.model.YRichTextArea;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSubTypeBaseSuspect;
import org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;
import org.eclipse.osbp.ecview.extension.model.YTypedCompoundSuspect;
import org.eclipse.osbp.ecview.extension.model.YTypedSuspect;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class YECviewFactoryImpl extends EFactoryImpl implements YECviewFactory {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static YECviewFactory init() {
		try {
			YECviewFactory theYECviewFactory = (YECviewFactory)EPackage.Registry.INSTANCE.getEFactory(YECviewPackage.eNS_URI);
			if (theYECviewFactory != null) {
				return theYECviewFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new YECviewFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YECviewFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param eClass
	 *            the e class
	 * @return the e object
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case YECviewPackage.YSTRATEGY_LAYOUT: return createYStrategyLayout();
			case YECviewPackage.YLAYOUTING_STRATEGY: return createYLayoutingStrategy();
			case YECviewPackage.YDEFAULT_LAYOUTING_STRATEGY: return createYDefaultLayoutingStrategy();
			case YECviewPackage.YFOCUSING_STRATEGY: return createYFocusingStrategy();
			case YECviewPackage.YDELEGATING_LAYOUTING_STRATEGY: return createYDelegatingLayoutingStrategy();
			case YECviewPackage.YDELEGATING_FOCUSING_STRATEGY: return createYDelegatingFocusingStrategy();
			case YECviewPackage.YSUSPECT: return createYSuspect();
			case YECviewPackage.YTYPED_SUSPECT: return createYTypedSuspect();
			case YECviewPackage.YTYPED_COMPOUND_SUSPECT: return createYTypedCompoundSuspect();
			case YECviewPackage.YSUB_TYPE_BASE_SUSPECT: return createYSubTypeBaseSuspect();
			case YECviewPackage.YSUB_TYPE_SUSPECT: return createYSubTypeSuspect();
			case YECviewPackage.YLAYOUTING_INFO: return createYLayoutingInfo();
			case YECviewPackage.YSUSPECT_INFO: return createYSuspectInfo();
			case YECviewPackage.YBLOB_UPLOAD_COMPONENT: return createYBlobUploadComponent();
			case YECviewPackage.YCUSTOM_DECIMAL_FIELD: return createYCustomDecimalField();
			case YECviewPackage.YI1_8N_COMBO_BOX: return createYI18nComboBox();
			case YECviewPackage.YICON_COMBO_BOX: return createYIconComboBox();
			case YECviewPackage.YQUANTITY_TEXT_FIELD: return createYQuantityTextField();
			case YECviewPackage.YCOLLECTION_SUSPECT: return createYCollectionSuspect();
			case YECviewPackage.YCOLUMN_INFO: return createYColumnInfo();
			case YECviewPackage.YCONTENT_SENSITIVE_LAYOUT: return createYContentSensitiveLayout();
			case YECviewPackage.YRICH_TEXT_AREA: return createYRichTextArea();
			case YECviewPackage.YMASKED_TEXT_FIELD: return createYMaskedTextField();
			case YECviewPackage.YPREFIXED_MASKED_TEXT_FIELD: return createYPrefixedMaskedTextField();
			case YECviewPackage.YMASKED_NUMERIC_FIELD: return createYMaskedNumericField();
			case YECviewPackage.YMASKED_DECIMAL_FIELD: return createYMaskedDecimalField();
			case YECviewPackage.YPAIR_COMBO_BOX: return createYPairComboBox();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case YECviewPackage.NUMBER:
				return createNumberFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case YECviewPackage.NUMBER:
				return convertNumberToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y strategy layout
	 * @generated
	 */
	public YStrategyLayout createYStrategyLayout() {
		YStrategyLayoutImpl yStrategyLayout = new YStrategyLayoutImpl();
		return yStrategyLayout;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting strategy
	 * @generated
	 */
	public YLayoutingStrategy createYLayoutingStrategy() {
		YLayoutingStrategyImpl yLayoutingStrategy = new YLayoutingStrategyImpl();
		return yLayoutingStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y default layouting strategy
	 * @generated
	 */
	public YDefaultLayoutingStrategy createYDefaultLayoutingStrategy() {
		YDefaultLayoutingStrategyImpl yDefaultLayoutingStrategy = new YDefaultLayoutingStrategyImpl();
		return yDefaultLayoutingStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y focusing strategy
	 * @generated
	 */
	public YFocusingStrategy createYFocusingStrategy() {
		YFocusingStrategyImpl yFocusingStrategy = new YFocusingStrategyImpl();
		return yFocusingStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegating layouting strategy
	 * @generated
	 */
	public YDelegatingLayoutingStrategy createYDelegatingLayoutingStrategy() {
		YDelegatingLayoutingStrategyImpl yDelegatingLayoutingStrategy = new YDelegatingLayoutingStrategyImpl();
		return yDelegatingLayoutingStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y delegating focusing strategy
	 * @generated
	 */
	public YDelegatingFocusingStrategy createYDelegatingFocusingStrategy() {
		YDelegatingFocusingStrategyImpl yDelegatingFocusingStrategy = new YDelegatingFocusingStrategyImpl();
		return yDelegatingFocusingStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect
	 * @generated
	 */
	public YSuspect createYSuspect() {
		YSuspectImpl ySuspect = new YSuspectImpl();
		return ySuspect;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y layouting info
	 * @generated
	 */
	public YLayoutingInfo createYLayoutingInfo() {
		YLayoutingInfoImpl yLayoutingInfo = new YLayoutingInfoImpl();
		return yLayoutingInfo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y suspect info
	 * @generated
	 */
	public YSuspectInfo createYSuspectInfo() {
		YSuspectInfoImpl ySuspectInfo = new YSuspectInfoImpl();
		return ySuspectInfo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y blob upload component
	 * @generated
	 */
	public YBlobUploadComponent createYBlobUploadComponent() {
		YBlobUploadComponentImpl yBlobUploadComponent = new YBlobUploadComponentImpl();
		return yBlobUploadComponent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed suspect
	 * @generated
	 */
	public YTypedSuspect createYTypedSuspect() {
		YTypedSuspectImpl yTypedSuspect = new YTypedSuspectImpl();
		return yTypedSuspect;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y typed compound suspect
	 * @generated
	 */
	public YTypedCompoundSuspect createYTypedCompoundSuspect() {
		YTypedCompoundSuspectImpl yTypedCompoundSuspect = new YTypedCompoundSuspectImpl();
		return yTypedCompoundSuspect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSubTypeBaseSuspect createYSubTypeBaseSuspect() {
		YSubTypeBaseSuspectImpl ySubTypeBaseSuspect = new YSubTypeBaseSuspectImpl();
		return ySubTypeBaseSuspect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YSubTypeSuspect createYSubTypeSuspect() {
		YSubTypeSuspectImpl ySubTypeSuspect = new YSubTypeSuspectImpl();
		return ySubTypeSuspect;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y custom decimal field
	 * @generated
	 */
	public YCustomDecimalField createYCustomDecimalField() {
		YCustomDecimalFieldImpl yCustomDecimalField = new YCustomDecimalFieldImpl();
		return yCustomDecimalField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YI18nComboBox createYI18nComboBox() {
		YI18nComboBoxImpl yi18nComboBox = new YI18nComboBoxImpl();
		return yi18nComboBox;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y icon combo box
	 * @generated
	 */
	public YIconComboBox createYIconComboBox() {
		YIconComboBoxImpl yIconComboBox = new YIconComboBoxImpl();
		return yIconComboBox;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y quantity text field
	 * @generated
	 */
	public YQuantityTextField createYQuantityTextField() {
		YQuantityTextFieldImpl yQuantityTextField = new YQuantityTextFieldImpl();
		return yQuantityTextField;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y collection suspect
	 * @generated
	 */
	public YCollectionSuspect createYCollectionSuspect() {
		YCollectionSuspectImpl yCollectionSuspect = new YCollectionSuspectImpl();
		return yCollectionSuspect;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the y column info
	 * @generated
	 */
	public YColumnInfo createYColumnInfo() {
		YColumnInfoImpl yColumnInfo = new YColumnInfoImpl();
		return yColumnInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YContentSensitiveLayout createYContentSensitiveLayout() {
		YContentSensitiveLayoutImpl yContentSensitiveLayout = new YContentSensitiveLayoutImpl();
		return yContentSensitiveLayout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YRichTextArea createYRichTextArea() {
		YRichTextAreaImpl yRichTextArea = new YRichTextAreaImpl();
		return yRichTextArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMaskedTextField createYMaskedTextField() {
		YMaskedTextFieldImpl yMaskedTextField = new YMaskedTextFieldImpl();
		return yMaskedTextField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YPrefixedMaskedTextField createYPrefixedMaskedTextField() {
		YPrefixedMaskedTextFieldImpl yPrefixedMaskedTextField = new YPrefixedMaskedTextFieldImpl();
		return yPrefixedMaskedTextField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMaskedNumericField createYMaskedNumericField() {
		YMaskedNumericFieldImpl yMaskedNumericField = new YMaskedNumericFieldImpl();
		return yMaskedNumericField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YMaskedDecimalField createYMaskedDecimalField() {
		YMaskedDecimalFieldImpl yMaskedDecimalField = new YMaskedDecimalFieldImpl();
		return yMaskedDecimalField;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Number createNumberFromString(EDataType eDataType, String initialValue) {
		return (Number)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNumberToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YPairComboBox createYPairComboBox() {
		YPairComboBoxImpl yPairComboBox = new YPairComboBoxImpl();
		return yPairComboBox;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the YE cview package
	 * @generated
	 */
	public YECviewPackage getYECviewPackage() {
		return (YECviewPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the package
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static YECviewPackage getPackage() {
		return YECviewPackage.eINSTANCE;
	}

}
