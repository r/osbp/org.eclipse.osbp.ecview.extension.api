/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.osbp.ecview.core.common.model.core.YCollectionBindable;
import org.eclipse.osbp.ecview.core.common.model.core.YSelectionBindable;
import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription;
import org.eclipse.osbp.ecview.core.extension.model.datatypes.YComboBoxDatatype;
import org.eclipse.osbp.ecview.core.extension.model.extension.YBeanServiceConsumer;
import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YIcon Combo Box</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatadescription <em>Datadescription</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatatype <em>Datatype</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getSelection <em>Selection</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getType <em>Type</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getEmfNsURI <em>Emf Ns URI</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getCaptionProperty <em>Caption Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getImageProperty <em>Image Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescriptionProperty <em>Description Property</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox()
 * @model
 * @generated
 */
public interface YIconComboBox extends YInput, YCollectionBindable, YSelectionBindable, YBeanServiceConsumer {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datadescription</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datadescription</em>' reference.
	 * @see #setDatadescription(YDatadescription)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_Datadescription()
	 * @model
	 * @generated
	 */
	YDatadescription getDatadescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatadescription <em>Datadescription</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datadescription</em>' reference.
	 * @see #getDatadescription()
	 * @generated
	 */
	void setDatadescription(YDatadescription value);

	/**
	 * Returns the value of the '<em><b>Datatype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datatype</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datatype</em>' reference.
	 * @see #setDatatype(YComboBoxDatatype)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_Datatype()
	 * @model
	 * @generated
	 */
	YComboBoxDatatype getDatatype();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDatatype <em>Datatype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datatype</em>' reference.
	 * @see #getDatatype()
	 * @generated
	 */
	void setDatatype(YComboBoxDatatype value);

	/**
	 * Returns the value of the '<em><b>Selection</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selection</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selection</em>' attribute.
	 * @see #setSelection(Object)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_Selection()
	 * @model transient="true"
	 * @generated
	 */
	Object getSelection();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getSelection <em>Selection</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selection</em>' attribute.
	 * @see #getSelection()
	 * @generated
	 */
	void setSelection(Object value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Class)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_Type()
	 * @model
	 * @generated
	 */
	Class<?> getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Class<?> value);

	/**
	 * Returns the value of the '<em><b>Emf Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Emf Ns URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #setEmfNsURI(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_EmfNsURI()
	 * @model
	 * @generated
	 */
	String getEmfNsURI();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getEmfNsURI <em>Emf Ns URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Emf Ns URI</em>' attribute.
	 * @see #getEmfNsURI()
	 * @generated
	 */
	void setEmfNsURI(String value);

	/**
	 * Returns the value of the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #setTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_TypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getTypeQualifiedName <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #getTypeQualifiedName()
	 * @generated
	 */
	void setTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Caption Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Caption Property</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Caption Property</em>' attribute.
	 * @see #setCaptionProperty(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_CaptionProperty()
	 * @model
	 * @generated
	 */
	String getCaptionProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getCaptionProperty <em>Caption Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Caption Property</em>' attribute.
	 * @see #getCaptionProperty()
	 * @generated
	 */
	void setCaptionProperty(String value);

	/**
	 * Returns the value of the '<em><b>Image Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Property</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Property</em>' attribute.
	 * @see #setImageProperty(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_ImageProperty()
	 * @model
	 * @generated
	 */
	String getImageProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getImageProperty <em>Image Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Property</em>' attribute.
	 * @see #getImageProperty()
	 * @generated
	 */
	void setImageProperty(String value);

	/**
	 * Returns the value of the '<em><b>Description Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description Property</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Property</em>' attribute.
	 * @see #setDescriptionProperty(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_DescriptionProperty()
	 * @model
	 * @generated
	 */
	String getDescriptionProperty();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescriptionProperty <em>Description Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description Property</em>' attribute.
	 * @see #getDescriptionProperty()
	 * @generated
	 */
	void setDescriptionProperty(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYIconComboBox_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YIconComboBox#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

}
