/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YSimple Decimal Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter#getNumberFormatPattern <em>Number Format Pattern</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYSimpleDecimalConverter()
 * @model
 * @generated
 */
public interface YSimpleDecimalConverter extends YConverter {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Number Format Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Format Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Format Pattern</em>' attribute.
	 * @see #setNumberFormatPattern(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYSimpleDecimalConverter_NumberFormatPattern()
	 * @model
	 * @generated
	 */
	String getNumberFormatPattern();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter#getNumberFormatPattern <em>Number Format Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Format Pattern</em>' attribute.
	 * @see #getNumberFormatPattern()
	 * @generated
	 */
	void setNumberFormatPattern(String value);

}
