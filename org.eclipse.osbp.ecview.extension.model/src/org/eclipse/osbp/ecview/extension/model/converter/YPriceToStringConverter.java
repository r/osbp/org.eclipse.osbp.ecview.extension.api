/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YPrice To String Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getValuePropertyPath <em>Value Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getCurrencyPropertyPath <em>Currency Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYPriceToStringConverter()
 * @model
 * @generated
 */
public interface YPriceToStringConverter extends YConverter {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Value Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Property Path</em>' attribute.
	 * @see #setValuePropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYPriceToStringConverter_ValuePropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getValuePropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getValuePropertyPath <em>Value Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Property Path</em>' attribute.
	 * @see #getValuePropertyPath()
	 * @generated
	 */
	void setValuePropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Currency Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Currency Property Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Currency Property Path</em>' attribute.
	 * @see #setCurrencyPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYPriceToStringConverter_CurrencyPropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getCurrencyPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getCurrencyPropertyPath <em>Currency Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Currency Property Path</em>' attribute.
	 * @see #getCurrencyPropertyPath()
	 * @generated
	 */
	void setCurrencyPropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Qualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #setTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYPriceToStringConverter_TypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getTypeQualifiedName <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Qualified Name</em>' attribute.
	 * @see #getTypeQualifiedName()
	 * @generated
	 */
	void setTypeQualifiedName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(Class)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYPriceToStringConverter_Type()
	 * @model
	 * @generated
	 */
	Class<?> getType();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(Class<?> value);

}
