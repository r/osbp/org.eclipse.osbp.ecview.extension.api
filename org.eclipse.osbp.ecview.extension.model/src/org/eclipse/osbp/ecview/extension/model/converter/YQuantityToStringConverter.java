/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter;

import org.eclipse.osbp.ecview.core.common.model.core.YConverter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YQuantity To String Converter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getAmountPropertyPath <em>Amount Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomPropertyPath <em>Uom Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomCodeRelativePropertyPath <em>Uom Code Relative Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getQuantityTypeQualifiedName <em>Quantity Type Qualified Name</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYQuantityToStringConverter()
 * @model
 * @generated
 */
public interface YQuantityToStringConverter extends YConverter {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Amount Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * The property path in DOT notation to access the amount value in the quantity object.
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Amount Property Path</em>' attribute.
	 * @see #setAmountPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYQuantityToStringConverter_AmountPropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getAmountPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getAmountPropertyPath <em>Amount Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Amount Property Path</em>' attribute.
	 * @see #getAmountPropertyPath()
	 * @generated
	 */
	void setAmountPropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Uom Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 *	The property path in DOT notation to access the uom object in the quantity object.
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uom Property Path</em>' attribute.
	 * @see #setUomPropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYQuantityToStringConverter_UomPropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getUomPropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomPropertyPath <em>Uom Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uom Property Path</em>' attribute.
	 * @see #getUomPropertyPath()
	 * @generated
	 */
	void setUomPropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Uom Code Relative Property Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * The relative property path in DOT notation to access the uom-code to show in the uom object.
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uom Code Relative Property Path</em>' attribute.
	 * @see #setUomCodeRelativePropertyPath(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYQuantityToStringConverter_UomCodeRelativePropertyPath()
	 * @model required="true"
	 * @generated
	 */
	String getUomCodeRelativePropertyPath();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getUomCodeRelativePropertyPath <em>Uom Code Relative Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Uom Code Relative Property Path</em>' attribute.
	 * @see #getUomCodeRelativePropertyPath()
	 * @generated
	 */
	void setUomCodeRelativePropertyPath(String value);

	/**
	 * Returns the value of the '<em><b>Quantity Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * The full qualified name of the Quantity class. 
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantity Type Qualified Name</em>' attribute.
	 * @see #setQuantityTypeQualifiedName(String)
	 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage#getYQuantityToStringConverter_QuantityTypeQualifiedName()
	 * @model
	 * @generated
	 */
	String getQuantityTypeQualifiedName();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter#getQuantityTypeQualifiedName <em>Quantity Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantity Type Qualified Name</em>' attribute.
	 * @see #getQuantityTypeQualifiedName()
	 * @generated
	 */
	void setQuantityTypeQualifiedName(String value);

}
