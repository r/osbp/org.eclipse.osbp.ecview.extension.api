/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.emf.common.util.EList;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YStrategy Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingStrategy <em>Layouting Strategy</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getFocusingStrategies <em>Focusing Strategies</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getSuspects <em>Suspects</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingInfo <em>Layouting Info</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getDefaultFocusingEnhancerId <em>Default Focusing Enhancer Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getNumberColumns <em>Number Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#isSaveAndNew <em>Save And New</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout()
 * @model
 * @generated
 */
public interface YStrategyLayout extends YEmbeddable {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Layouting Strategy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layouting Strategy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layouting Strategy</em>' containment reference.
	 * @see #setLayoutingStrategy(YLayoutingStrategy)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_LayoutingStrategy()
	 * @model containment="true"
	 * @generated
	 */
	YLayoutingStrategy getLayoutingStrategy();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingStrategy <em>Layouting Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layouting Strategy</em>' containment reference.
	 * @see #getLayoutingStrategy()
	 * @generated
	 */
	void setLayoutingStrategy(YLayoutingStrategy value);

	/**
	 * Returns the value of the '<em><b>Focusing Strategies</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.model.YFocusingStrategy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Focusing Strategies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Focusing Strategies</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_FocusingStrategies()
	 * @model containment="true"
	 * @generated
	 */
	EList<YFocusingStrategy> getFocusingStrategies();

	/**
	 * Returns the value of the '<em><b>Suspects</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.osbp.ecview.extension.model.YSuspect}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Suspects</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Suspects</em>' containment reference list.
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_Suspects()
	 * @model containment="true"
	 * @generated
	 */
	EList<YSuspect> getSuspects();

	/**
	 * Returns the value of the '<em><b>Layouting Info</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layouting Info</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layouting Info</em>' containment reference.
	 * @see #setLayoutingInfo(YLayoutingInfo)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_LayoutingInfo()
	 * @model containment="true" transient="true"
	 * @generated
	 */
	YLayoutingInfo getLayoutingInfo();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getLayoutingInfo <em>Layouting Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layouting Info</em>' containment reference.
	 * @see #getLayoutingInfo()
	 * @generated
	 */
	void setLayoutingInfo(YLayoutingInfo value);

	/**
	 * Returns the value of the '<em><b>Default Focusing Enhancer Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Focusing Enhancer Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Focusing Enhancer Id</em>' attribute.
	 * @see #setDefaultFocusingEnhancerId(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_DefaultFocusingEnhancerId()
	 * @model
	 * @generated
	 */
	String getDefaultFocusingEnhancerId();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getDefaultFocusingEnhancerId <em>Default Focusing Enhancer Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Focusing Enhancer Id</em>' attribute.
	 * @see #getDefaultFocusingEnhancerId()
	 * @generated
	 */
	void setDefaultFocusingEnhancerId(String value);

	/**
	 * Returns the value of the '<em><b>Number Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Columns</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Columns</em>' attribute.
	 * @see #setNumberColumns(int)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_NumberColumns()
	 * @model
	 * @generated
	 */
	int getNumberColumns();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#getNumberColumns <em>Number Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Columns</em>' attribute.
	 * @see #getNumberColumns()
	 * @generated
	 */
	void setNumberColumns(int value);

	/**
	 * Returns the value of the '<em><b>Save And New</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Save And New</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Save And New</em>' attribute.
	 * @see #setSaveAndNew(boolean)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYStrategyLayout_SaveAndNew()
	 * @model
	 * @generated
	 */
	boolean isSaveAndNew();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YStrategyLayout#isSaveAndNew <em>Save And New</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Save And New</em>' attribute.
	 * @see #isSaveAndNew()
	 * @generated
	 */
	void setSaveAndNew(boolean value);

	/**
	 * Uses the suspect info form layouting info to find the proper suspectinfo
	 * for the given embeddable.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the y suspect info
	 */
	YSuspectInfo findInfoFor(YEmbeddable yEmbeddable);
	
	/**
	 * Tries to find the info without an nextFocus.
	 *
	 * @return the y suspect info
	 */
	YSuspectInfo findLastFocus();

}
