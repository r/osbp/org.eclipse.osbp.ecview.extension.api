/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.osbp.ecview.core.common.model.core.YValueBindable;

import org.eclipse.osbp.ecview.core.common.model.datatypes.YDatadescription;

import org.eclipse.osbp.ecview.core.extension.model.extension.YInput;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YMasked Decimal Field</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDatadescription <em>Datadescription</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getValue <em>Value</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getMask <em>Mask</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDecimalSeparator <em>Decimal Separator</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getGroupingSeparator <em>Grouping Separator</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYMaskedDecimalField()
 * @model
 * @generated
 */
public interface YMaskedDecimalField extends YInput, YValueBindable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Datadescription</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datadescription</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datadescription</em>' reference.
	 * @see #setDatadescription(YDatadescription)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYMaskedDecimalField_Datadescription()
	 * @model
	 * @generated
	 */
	YDatadescription getDatadescription();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDatadescription <em>Datadescription</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datadescription</em>' reference.
	 * @see #getDatadescription()
	 * @generated
	 */
	void setDatadescription(YDatadescription value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(Number)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYMaskedDecimalField_Value()
	 * @model dataType="org.eclipse.osbp.ecview.extension.model.Number" transient="true"
	 * @generated
	 */
	Number getValue();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Number value);

	/**
	 * Returns the value of the '<em><b>Mask</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mask</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mask</em>' attribute.
	 * @see #setMask(String)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYMaskedDecimalField_Mask()
	 * @model default=""
	 * @generated
	 */
	String getMask();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getMask <em>Mask</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mask</em>' attribute.
	 * @see #getMask()
	 * @generated
	 */
	void setMask(String value);

	/**
	 * Returns the value of the '<em><b>Decimal Separator</b></em>' attribute.
	 * The default value is <code>"null"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Decimal Separator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Decimal Separator</em>' attribute.
	 * @see #setDecimalSeparator(Character)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYMaskedDecimalField_DecimalSeparator()
	 * @model default="null"
	 * @generated
	 */
	Character getDecimalSeparator();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getDecimalSeparator <em>Decimal Separator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Decimal Separator</em>' attribute.
	 * @see #getDecimalSeparator()
	 * @generated
	 */
	void setDecimalSeparator(Character value);

	/**
	 * Returns the value of the '<em><b>Grouping Separator</b></em>' attribute.
	 * The default value is <code>"null"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grouping Separator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grouping Separator</em>' attribute.
	 * @see #setGroupingSeparator(Character)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYMaskedDecimalField_GroupingSeparator()
	 * @model default="null"
	 * @generated
	 */
	Character getGroupingSeparator();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YMaskedDecimalField#getGroupingSeparator <em>Grouping Separator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grouping Separator</em>' attribute.
	 * @see #getGroupingSeparator()
	 * @generated
	 */
	void setGroupingSeparator(Character value);

} // YMaskedDecimalField
