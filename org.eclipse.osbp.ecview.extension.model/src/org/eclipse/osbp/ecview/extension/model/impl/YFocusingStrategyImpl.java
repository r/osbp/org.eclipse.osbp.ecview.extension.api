/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.YKeyStrokeDefinition;
import org.eclipse.osbp.ecview.core.common.model.core.YLayout;
import org.eclipse.osbp.ecview.core.common.model.core.YView;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YFocusingStrategy;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YFocusing Strategy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl#getKeyStrokeDefinition <em>Key Stroke Definition</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YFocusingStrategyImpl#getTempStrokeDefinition <em>Temp Stroke Definition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YFocusingStrategyImpl extends MinimalEObjectImpl.Container implements YFocusingStrategy {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The cached value of the '{@link #getKeyStrokeDefinition() <em>Key Stroke Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKeyStrokeDefinition()
	 * @generated
	 * @ordered
	 */
	protected YKeyStrokeDefinition keyStrokeDefinition;

	/**
	 * The cached value of the '{@link #getTempStrokeDefinition() <em>Temp Stroke Definition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTempStrokeDefinition()
	 * @generated
	 * @ordered
	 */
	protected YKeyStrokeDefinition tempStrokeDefinition;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YFocusingStrategyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YECviewPackage.Literals.YFOCUSING_STRATEGY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getId() <em>Id</em>}' attribute
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newId
	 *            the new cached value of the '{@link #getId() <em>Id</em>}'
	 *            attribute
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YFOCUSING_STRATEGY__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getName() <em>Name</em>}'
	 *         attribute
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newName
	 *            the new cached value of the '{@link #getName() <em>Name</em>}'
	 *            attribute
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YFOCUSING_STRATEGY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTags() <em>Tags</em>}'
	 *         attribute list
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, YECviewPackage.YFOCUSING_STRATEGY__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getProperties()
	 *         <em>Properties</em>}' map
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, YECviewPackage.YFOCUSING_STRATEGY__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getKeyStrokeDefinition()
	 *         <em>Key Stroke Definition</em>}' containment reference
	 * @generated
	 */
	public YKeyStrokeDefinition getKeyStrokeDefinition() {
		return keyStrokeDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newKeyStrokeDefinition
	 *            the new key stroke definition
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetKeyStrokeDefinition(YKeyStrokeDefinition newKeyStrokeDefinition, NotificationChain msgs) {
		YKeyStrokeDefinition oldKeyStrokeDefinition = keyStrokeDefinition;
		keyStrokeDefinition = newKeyStrokeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION, oldKeyStrokeDefinition, newKeyStrokeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newKeyStrokeDefinition
	 *            the new cached value of the '{@link #getKeyStrokeDefinition()
	 *            <em>Key Stroke Definition</em>}' containment reference
	 * @generated
	 */
	public void setKeyStrokeDefinition(YKeyStrokeDefinition newKeyStrokeDefinition) {
		if (newKeyStrokeDefinition != keyStrokeDefinition) {
			NotificationChain msgs = null;
			if (keyStrokeDefinition != null)
				msgs = ((InternalEObject)keyStrokeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION, null, msgs);
			if (newKeyStrokeDefinition != null)
				msgs = ((InternalEObject)newKeyStrokeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION, null, msgs);
			msgs = basicSetKeyStrokeDefinition(newKeyStrokeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION, newKeyStrokeDefinition, newKeyStrokeDefinition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTempStrokeDefinition()
	 *         <em>Temp Stroke Definition</em>}' containment reference
	 * @generated
	 */
	public YKeyStrokeDefinition getTempStrokeDefinition() {
		return tempStrokeDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTempStrokeDefinition
	 *            the new temp stroke definition
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetTempStrokeDefinition(YKeyStrokeDefinition newTempStrokeDefinition, NotificationChain msgs) {
		YKeyStrokeDefinition oldTempStrokeDefinition = tempStrokeDefinition;
		tempStrokeDefinition = newTempStrokeDefinition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION, oldTempStrokeDefinition, newTempStrokeDefinition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTempStrokeDefinition
	 *            the new cached value of the '
	 *            {@link #getTempStrokeDefinition()
	 *            <em>Temp Stroke Definition</em>}' containment reference
	 * @generated
	 */
	public void setTempStrokeDefinition(YKeyStrokeDefinition newTempStrokeDefinition) {
		if (newTempStrokeDefinition != tempStrokeDefinition) {
			NotificationChain msgs = null;
			if (tempStrokeDefinition != null)
				msgs = ((InternalEObject)tempStrokeDefinition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION, null, msgs);
			if (newTempStrokeDefinition != null)
				msgs = ((InternalEObject)newTempStrokeDefinition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION, null, msgs);
			msgs = basicSetTempStrokeDefinition(newTempStrokeDefinition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION, newTempStrokeDefinition, newTempStrokeDefinition));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the view gen
	 * @generated
	 */
	public YView getViewGen() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 * @generated NOT
	 */
	public YView getView() {
		return findViewGeneric(eContainer());
	}
	
	/**
	 * Find view generic.
	 *
	 * @param container
	 *            the container
	 * @return the y view
	 */
	protected YView findViewGeneric(EObject container) {
		if (container == null) {
			return null;
		}
		if (container instanceof YView) {
			return (YView) container;
		} else if (container instanceof YLayout) {
			return ((YLayout) container).getView();
		} else {
			EObject parent = container.eContainer();
			return findViewGeneric(parent);
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case YECviewPackage.YFOCUSING_STRATEGY__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
			case YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION:
				return basicSetKeyStrokeDefinition(null, msgs);
			case YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION:
				return basicSetTempStrokeDefinition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YECviewPackage.YFOCUSING_STRATEGY__TAGS:
				return getTags();
			case YECviewPackage.YFOCUSING_STRATEGY__ID:
				return getId();
			case YECviewPackage.YFOCUSING_STRATEGY__NAME:
				return getName();
			case YECviewPackage.YFOCUSING_STRATEGY__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION:
				return getKeyStrokeDefinition();
			case YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION:
				return getTempStrokeDefinition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YECviewPackage.YFOCUSING_STRATEGY__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__ID:
				setId((String)newValue);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__NAME:
				setName((String)newValue);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION:
				setKeyStrokeDefinition((YKeyStrokeDefinition)newValue);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION:
				setTempStrokeDefinition((YKeyStrokeDefinition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YECviewPackage.YFOCUSING_STRATEGY__TAGS:
				getTags().clear();
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__ID:
				setId(ID_EDEFAULT);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__NAME:
				setName(NAME_EDEFAULT);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__PROPERTIES:
				getProperties().clear();
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION:
				setKeyStrokeDefinition((YKeyStrokeDefinition)null);
				return;
			case YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION:
				setTempStrokeDefinition((YKeyStrokeDefinition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YECviewPackage.YFOCUSING_STRATEGY__TAGS:
				return tags != null && !tags.isEmpty();
			case YECviewPackage.YFOCUSING_STRATEGY__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case YECviewPackage.YFOCUSING_STRATEGY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case YECviewPackage.YFOCUSING_STRATEGY__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case YECviewPackage.YFOCUSING_STRATEGY__KEY_STROKE_DEFINITION:
				return keyStrokeDefinition != null;
			case YECviewPackage.YFOCUSING_STRATEGY__TEMP_STROKE_DEFINITION:
				return tempStrokeDefinition != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

}
