/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.CoreModelPackage;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YStringToStringMapImpl;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YPrice To String Converter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getTags <em>Tags</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getProperties <em>Properties</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getValuePropertyPath <em>Value Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getCurrencyPropertyPath <em>Currency Property Path</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getTypeQualifiedName <em>Type Qualified Name</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.converter.impl.YPriceToStringConverterImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YPriceToStringConverterImpl extends MinimalEObjectImpl.Container implements YPriceToStringConverter {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached value of the '{@link #getTags() <em>Tags</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTags()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tags;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getProperties() <em>Properties</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperties()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> properties;

	/**
	 * The default value of the '{@link #getValuePropertyPath() <em>Value Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValuePropertyPath()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_PROPERTY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValuePropertyPath() <em>Value Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValuePropertyPath()
	 * @generated
	 * @ordered
	 */
	protected String valuePropertyPath = VALUE_PROPERTY_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getCurrencyPropertyPath() <em>Currency Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected static final String CURRENCY_PROPERTY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCurrencyPropertyPath() <em>Currency Property Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrencyPropertyPath()
	 * @generated
	 * @ordered
	 */
	protected String currencyPropertyPath = CURRENCY_PROPERTY_PATH_EDEFAULT;

	/**
	 * The default value of the '{@link #getTypeQualifiedName() <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_QUALIFIED_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTypeQualifiedName() <em>Type Qualified Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeQualifiedName()
	 * @generated
	 * @ordered
	 */
	protected String typeQualifiedName = TYPE_QUALIFIED_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Class<?> type;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YPriceToStringConverterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YConverterPackage.Literals.YPRICE_TO_STRING_CONVERTER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getId() <em>Id</em>}' attribute
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newId
	 *            the new cached value of the '{@link #getId() <em>Id</em>}'
	 *            attribute
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YPRICE_TO_STRING_CONVERTER__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getName() <em>Name</em>}'
	 *         attribute
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newName
	 *            the new cached value of the '{@link #getName() <em>Name</em>}'
	 *            attribute
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YPRICE_TO_STRING_CONVERTER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTags() <em>Tags</em>}'
	 *         attribute list
	 * @generated
	 */
	public EList<String> getTags() {
		if (tags == null) {
			tags = new EDataTypeUniqueEList<String>(String.class, this, YConverterPackage.YPRICE_TO_STRING_CONVERTER__TAGS);
		}
		return tags;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getProperties()
	 *         <em>Properties</em>}' map
	 * @generated
	 */
	public EMap<String, String> getProperties() {
		if (properties == null) {
			properties = new EcoreEMap<String,String>(CoreModelPackage.Literals.YSTRING_TO_STRING_MAP, YStringToStringMapImpl.class, this, YConverterPackage.YPRICE_TO_STRING_CONVERTER__PROPERTIES);
		}
		return properties;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getValuePropertyPath()
	 *         <em>Value Property Path</em>}' attribute
	 * @generated
	 */
	public String getValuePropertyPath() {
		return valuePropertyPath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newValuePropertyPath
	 *            the new cached value of the '{@link #getValuePropertyPath()
	 *            <em>Value Property Path</em>}' attribute
	 * @generated
	 */
	public void setValuePropertyPath(String newValuePropertyPath) {
		String oldValuePropertyPath = valuePropertyPath;
		valuePropertyPath = newValuePropertyPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH, oldValuePropertyPath, valuePropertyPath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getCurrencyPropertyPath()
	 *         <em>Currency Property Path</em>}' attribute
	 * @generated
	 */
	public String getCurrencyPropertyPath() {
		return currencyPropertyPath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newCurrencyPropertyPath
	 *            the new cached value of the '
	 *            {@link #getCurrencyPropertyPath()
	 *            <em>Currency Property Path</em>}' attribute
	 * @generated
	 */
	public void setCurrencyPropertyPath(String newCurrencyPropertyPath) {
		String oldCurrencyPropertyPath = currencyPropertyPath;
		currencyPropertyPath = newCurrencyPropertyPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH, oldCurrencyPropertyPath, currencyPropertyPath));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getTypeQualifiedName()
	 *         <em>Type Qualified Name</em>}' attribute
	 * @generated
	 */
	public String getTypeQualifiedName() {
		return typeQualifiedName;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newTypeQualifiedName
	 *            the new cached value of the '{@link #getTypeQualifiedName()
	 *            <em>Type Qualified Name</em>}' attribute
	 * @generated
	 */
	public void setTypeQualifiedName(String newTypeQualifiedName) {
		String oldTypeQualifiedName = typeQualifiedName;
		typeQualifiedName = newTypeQualifiedName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME, oldTypeQualifiedName, typeQualifiedName));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getType() <em>Type</em>}'
	 *         attribute
	 * @generated
	 */
	public Class<?> getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newType
	 *            the new cached value of the '{@link #getType() <em>Type</em>}'
	 *            attribute
	 * @generated
	 */
	public void setType(Class<?> newType) {
		Class<?> oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__PROPERTIES:
				return ((InternalEList<?>)getProperties()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TAGS:
				return getTags();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__ID:
				return getId();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__NAME:
				return getName();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__PROPERTIES:
				if (coreType) return getProperties();
				else return getProperties().map();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH:
				return getValuePropertyPath();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH:
				return getCurrencyPropertyPath();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME:
				return getTypeQualifiedName();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE:
				return getType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TAGS:
				getTags().clear();
				getTags().addAll((Collection<? extends String>)newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__ID:
				setId((String)newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__NAME:
				setName((String)newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__PROPERTIES:
				((EStructuralFeature.Setting)getProperties()).set(newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH:
				setValuePropertyPath((String)newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH:
				setCurrencyPropertyPath((String)newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME:
				setTypeQualifiedName((String)newValue);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE:
				setType((Class<?>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TAGS:
				getTags().clear();
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__ID:
				setId(ID_EDEFAULT);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__PROPERTIES:
				getProperties().clear();
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH:
				setValuePropertyPath(VALUE_PROPERTY_PATH_EDEFAULT);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH:
				setCurrencyPropertyPath(CURRENCY_PROPERTY_PATH_EDEFAULT);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME:
				setTypeQualifiedName(TYPE_QUALIFIED_NAME_EDEFAULT);
				return;
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE:
				setType((Class<?>)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TAGS:
				return tags != null && !tags.isEmpty();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__PROPERTIES:
				return properties != null && !properties.isEmpty();
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__VALUE_PROPERTY_PATH:
				return VALUE_PROPERTY_PATH_EDEFAULT == null ? valuePropertyPath != null : !VALUE_PROPERTY_PATH_EDEFAULT.equals(valuePropertyPath);
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__CURRENCY_PROPERTY_PATH:
				return CURRENCY_PROPERTY_PATH_EDEFAULT == null ? currencyPropertyPath != null : !CURRENCY_PROPERTY_PATH_EDEFAULT.equals(currencyPropertyPath);
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE_QUALIFIED_NAME:
				return TYPE_QUALIFIED_NAME_EDEFAULT == null ? typeQualifiedName != null : !TYPE_QUALIFIED_NAME_EDEFAULT.equals(typeQualifiedName);
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER__TYPE:
				return type != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tags: ");
		result.append(tags);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", valuePropertyPath: ");
		result.append(valuePropertyPath);
		result.append(", currencyPropertyPath: ");
		result.append(currencyPropertyPath);
		result.append(", typeQualifiedName: ");
		result.append(typeQualifiedName);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

}
