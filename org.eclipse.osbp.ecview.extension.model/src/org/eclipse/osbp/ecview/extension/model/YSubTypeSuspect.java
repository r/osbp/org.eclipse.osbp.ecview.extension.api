/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation
 * 
 */
 package org.eclipse.osbp.ecview.extension.model;

import org.eclipse.osbp.ecview.core.common.model.core.YBeanSlot;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>YSub Type Suspect</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect#getBeanSlot <em>Bean Slot</em>}</li>
 * </ul>
 *
 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSubTypeSuspect()
 * @model
 * @generated
 */
public interface YSubTypeSuspect extends YTypedCompoundSuspect {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * Returns the value of the '<em><b>Bean Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bean Slot</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bean Slot</em>' reference.
	 * @see #setBeanSlot(YBeanSlot)
	 * @see org.eclipse.osbp.ecview.extension.model.YECviewPackage#getYSubTypeSuspect_BeanSlot()
	 * @model required="true"
	 * @generated
	 */
	YBeanSlot getBeanSlot();

	/**
	 * Sets the value of the '{@link org.eclipse.osbp.ecview.extension.model.YSubTypeSuspect#getBeanSlot <em>Bean Slot</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bean Slot</em>' reference.
	 * @see #getBeanSlot()
	 * @generated
	 */
	void setBeanSlot(YBeanSlot value);

} // YSubTypeSuspect
