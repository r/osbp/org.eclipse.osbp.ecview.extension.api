/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.visibility;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.osbp.ecview.core.common.model.visibility.VisibilityPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.visibility.YVisibilityFactory
 * @model kind="package"
 * @generated
 */
public interface YVisibilityPackage extends EPackage {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "visibility";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://osbp.de/ecview/v1/extension/visibility";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "visibility";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	YVisibilityPackage eINSTANCE = org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.visibility.impl.YAuthorizationVisibilityProcessorImpl <em>YAuthorization Visibility Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YAuthorizationVisibilityProcessorImpl
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl#getYAuthorizationVisibilityProcessor()
	 * @generated
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR = 0;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__TAGS = VisibilityPackage.YVISIBILITY_PROCESSOR__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__ID = VisibilityPackage.YVISIBILITY_PROCESSOR__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__NAME = VisibilityPackage.YVISIBILITY_PROCESSOR__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__PROPERTIES = VisibilityPackage.YVISIBILITY_PROCESSOR__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Data Used</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__DATA_USED = VisibilityPackage.YVISIBILITY_PROCESSOR__DATA_USED;

	/**
	 * The feature id for the '<em><b>Triggers On</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__TRIGGERS_ON = VisibilityPackage.YVISIBILITY_PROCESSOR__TRIGGERS_ON;

	/**
	 * The feature id for the '<em><b>Delegate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__DELEGATE = VisibilityPackage.YVISIBILITY_PROCESSOR__DELEGATE;

	/**
	 * The feature id for the '<em><b>Delegate Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR__DELEGATE_QUALIFIED_NAME = VisibilityPackage.YVISIBILITY_PROCESSOR__DELEGATE_QUALIFIED_NAME;

	/**
	 * The number of structural features of the '<em>YAuthorization Visibility Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YAUTHORIZATION_VISIBILITY_PROCESSOR_FEATURE_COUNT = VisibilityPackage.YVISIBILITY_PROCESSOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.osbp.ecview.extension.model.visibility.impl.YSubTypeVisibilityProcessorImpl <em>YSub Type Visibility Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YSubTypeVisibilityProcessorImpl
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl#getYSubTypeVisibilityProcessor()
	 * @generated
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR = 1;

	/**
	 * The feature id for the '<em><b>Tags</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__TAGS = VisibilityPackage.YVISIBILITY_PROCESSOR__TAGS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__ID = VisibilityPackage.YVISIBILITY_PROCESSOR__ID;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__NAME = VisibilityPackage.YVISIBILITY_PROCESSOR__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__PROPERTIES = VisibilityPackage.YVISIBILITY_PROCESSOR__PROPERTIES;

	/**
	 * The feature id for the '<em><b>Data Used</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__DATA_USED = VisibilityPackage.YVISIBILITY_PROCESSOR__DATA_USED;

	/**
	 * The feature id for the '<em><b>Triggers On</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__TRIGGERS_ON = VisibilityPackage.YVISIBILITY_PROCESSOR__TRIGGERS_ON;

	/**
	 * The feature id for the '<em><b>Delegate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__DELEGATE = VisibilityPackage.YVISIBILITY_PROCESSOR__DELEGATE;

	/**
	 * The feature id for the '<em><b>Delegate Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__DELEGATE_QUALIFIED_NAME = VisibilityPackage.YVISIBILITY_PROCESSOR__DELEGATE_QUALIFIED_NAME;

	/**
	 * The feature id for the '<em><b>Type Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__TYPE_QUALIFIED_NAME = VisibilityPackage.YVISIBILITY_PROCESSOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__TYPE = VisibilityPackage.YVISIBILITY_PROCESSOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR__TARGET = VisibilityPackage.YVISIBILITY_PROCESSOR_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>YSub Type Visibility Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YSUB_TYPE_VISIBILITY_PROCESSOR_FEATURE_COUNT = VisibilityPackage.YVISIBILITY_PROCESSOR_FEATURE_COUNT + 3;

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor <em>YAuthorization Visibility Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YAuthorization Visibility Processor</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.YAuthorizationVisibilityProcessor
	 * @generated
	 */
	EClass getYAuthorizationVisibilityProcessor();

	/**
	 * Returns the meta object for class '{@link org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor <em>YSub Type Visibility Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YSub Type Visibility Processor</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor
	 * @generated
	 */
	EClass getYSubTypeVisibilityProcessor();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor#getTypeQualifiedName <em>Type Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type Qualified Name</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor#getTypeQualifiedName()
	 * @see #getYSubTypeVisibilityProcessor()
	 * @generated
	 */
	EAttribute getYSubTypeVisibilityProcessor_TypeQualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor#getType()
	 * @see #getYSubTypeVisibilityProcessor()
	 * @generated
	 */
	EAttribute getYSubTypeVisibilityProcessor_Type();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see org.eclipse.osbp.ecview.extension.model.visibility.YSubTypeVisibilityProcessor#getTarget()
	 * @see #getYSubTypeVisibilityProcessor()
	 * @generated
	 */
	EReference getYSubTypeVisibilityProcessor_Target();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	YVisibilityFactory getYVisibilityFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.visibility.impl.YAuthorizationVisibilityProcessorImpl <em>YAuthorization Visibility Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YAuthorizationVisibilityProcessorImpl
		 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl#getYAuthorizationVisibilityProcessor()
		 * @generated
		 */
		EClass YAUTHORIZATION_VISIBILITY_PROCESSOR = eINSTANCE.getYAuthorizationVisibilityProcessor();
		/**
		 * The meta object literal for the '{@link org.eclipse.osbp.ecview.extension.model.visibility.impl.YSubTypeVisibilityProcessorImpl <em>YSub Type Visibility Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YSubTypeVisibilityProcessorImpl
		 * @see org.eclipse.osbp.ecview.extension.model.visibility.impl.YVisibilityPackageImpl#getYSubTypeVisibilityProcessor()
		 * @generated
		 */
		EClass YSUB_TYPE_VISIBILITY_PROCESSOR = eINSTANCE.getYSubTypeVisibilityProcessor();
		/**
		 * The meta object literal for the '<em><b>Type Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUB_TYPE_VISIBILITY_PROCESSOR__TYPE_QUALIFIED_NAME = eINSTANCE.getYSubTypeVisibilityProcessor_TypeQualifiedName();
		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute YSUB_TYPE_VISIBILITY_PROCESSOR__TYPE = eINSTANCE.getYSubTypeVisibilityProcessor_Type();
		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference YSUB_TYPE_VISIBILITY_PROCESSOR__TARGET = eINSTANCE.getYSubTypeVisibilityProcessor_Target();

	}

}
