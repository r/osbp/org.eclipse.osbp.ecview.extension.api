/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.converter.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import org.eclipse.osbp.ecview.core.common.model.core.YConverter;
import org.eclipse.osbp.ecview.core.common.model.core.YElement;
import org.eclipse.osbp.ecview.core.common.model.core.YTaggable;
import org.eclipse.osbp.ecview.extension.model.converter.*;
import org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage;
import org.eclipse.osbp.ecview.extension.model.converter.YCustomDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YDecimalToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToResourceConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YNumericToUomoConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YObjectToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YPriceToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YQuantityToStringConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YSimpleDecimalConverter;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConfig;
import org.eclipse.osbp.ecview.extension.model.converter.YStringToResourceConverter;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see org.eclipse.osbp.ecview.extension.model.converter.YConverterPackage
 * @generated
 */
public class YConverterSwitch<T> extends Switch<T> {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached model package <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected static YConverterPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YConverterSwitch() {
		if (modelPackage == null) {
			modelPackage = YConverterPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 *
	 * @param ePackage
	 *            the e package
	 * @return whether this is a switch for the given package.
	 * @parameter ePackage the package in question.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case YConverterPackage.YOBJECT_TO_STRING_CONVERTER: {
				YObjectToStringConverter yObjectToStringConverter = (YObjectToStringConverter)theEObject;
				T result = caseYObjectToStringConverter(yObjectToStringConverter);
				if (result == null) result = caseYConverter(yObjectToStringConverter);
				if (result == null) result = caseYElement(yObjectToStringConverter);
				if (result == null) result = caseYTaggable(yObjectToStringConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YSTRING_TO_BYTE_ARRAY_CONVERTER: {
				YStringToByteArrayConverter yStringToByteArrayConverter = (YStringToByteArrayConverter)theEObject;
				T result = caseYStringToByteArrayConverter(yStringToByteArrayConverter);
				if (result == null) result = caseYConverter(yStringToByteArrayConverter);
				if (result == null) result = caseYElement(yStringToByteArrayConverter);
				if (result == null) result = caseYTaggable(yStringToByteArrayConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YCUSTOM_DECIMAL_CONVERTER: {
				YCustomDecimalConverter yCustomDecimalConverter = (YCustomDecimalConverter)theEObject;
				T result = caseYCustomDecimalConverter(yCustomDecimalConverter);
				if (result == null) result = caseYConverter(yCustomDecimalConverter);
				if (result == null) result = caseYElement(yCustomDecimalConverter);
				if (result == null) result = caseYTaggable(yCustomDecimalConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONVERTER: {
				YNumericToResourceConverter yNumericToResourceConverter = (YNumericToResourceConverter)theEObject;
				T result = caseYNumericToResourceConverter(yNumericToResourceConverter);
				if (result == null) result = caseYConverter(yNumericToResourceConverter);
				if (result == null) result = caseYElement(yNumericToResourceConverter);
				if (result == null) result = caseYTaggable(yNumericToResourceConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YSTRING_TO_RESOURCE_CONVERTER: {
				YStringToResourceConverter yStringToResourceConverter = (YStringToResourceConverter)theEObject;
				T result = caseYStringToResourceConverter(yStringToResourceConverter);
				if (result == null) result = caseYConverter(yStringToResourceConverter);
				if (result == null) result = caseYElement(yStringToResourceConverter);
				if (result == null) result = caseYTaggable(yStringToResourceConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YNUMERIC_TO_RESOURCE_CONFIG: {
				YNumericToResourceConfig yNumericToResourceConfig = (YNumericToResourceConfig)theEObject;
				T result = caseYNumericToResourceConfig(yNumericToResourceConfig);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YSTRING_TO_RESOURCE_CONFIG: {
				YStringToResourceConfig yStringToResourceConfig = (YStringToResourceConfig)theEObject;
				T result = caseYStringToResourceConfig(yStringToResourceConfig);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YPRICE_TO_STRING_CONVERTER: {
				YPriceToStringConverter yPriceToStringConverter = (YPriceToStringConverter)theEObject;
				T result = caseYPriceToStringConverter(yPriceToStringConverter);
				if (result == null) result = caseYConverter(yPriceToStringConverter);
				if (result == null) result = caseYElement(yPriceToStringConverter);
				if (result == null) result = caseYTaggable(yPriceToStringConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YQUANTITY_TO_STRING_CONVERTER: {
				YQuantityToStringConverter yQuantityToStringConverter = (YQuantityToStringConverter)theEObject;
				T result = caseYQuantityToStringConverter(yQuantityToStringConverter);
				if (result == null) result = caseYConverter(yQuantityToStringConverter);
				if (result == null) result = caseYElement(yQuantityToStringConverter);
				if (result == null) result = caseYTaggable(yQuantityToStringConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YNUMERIC_TO_UOMO_CONVERTER: {
				YNumericToUomoConverter yNumericToUomoConverter = (YNumericToUomoConverter)theEObject;
				T result = caseYNumericToUomoConverter(yNumericToUomoConverter);
				if (result == null) result = caseYConverter(yNumericToUomoConverter);
				if (result == null) result = caseYElement(yNumericToUomoConverter);
				if (result == null) result = caseYTaggable(yNumericToUomoConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YDECIMAL_TO_UOMO_CONVERTER: {
				YDecimalToUomoConverter yDecimalToUomoConverter = (YDecimalToUomoConverter)theEObject;
				T result = caseYDecimalToUomoConverter(yDecimalToUomoConverter);
				if (result == null) result = caseYConverter(yDecimalToUomoConverter);
				if (result == null) result = caseYElement(yDecimalToUomoConverter);
				if (result == null) result = caseYTaggable(yDecimalToUomoConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YSIMPLE_DECIMAL_CONVERTER: {
				YSimpleDecimalConverter ySimpleDecimalConverter = (YSimpleDecimalConverter)theEObject;
				T result = caseYSimpleDecimalConverter(ySimpleDecimalConverter);
				if (result == null) result = caseYConverter(ySimpleDecimalConverter);
				if (result == null) result = caseYElement(ySimpleDecimalConverter);
				if (result == null) result = caseYTaggable(ySimpleDecimalConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case YConverterPackage.YVAACLIPSE_UI_THEME_TO_STRING_CONVERTER: {
				YVaaclipseUiThemeToStringConverter yVaaclipseUiThemeToStringConverter = (YVaaclipseUiThemeToStringConverter)theEObject;
				T result = caseYVaaclipseUiThemeToStringConverter(yVaaclipseUiThemeToStringConverter);
				if (result == null) result = caseYConverter(yVaaclipseUiThemeToStringConverter);
				if (result == null) result = caseYElement(yVaaclipseUiThemeToStringConverter);
				if (result == null) result = caseYTaggable(yVaaclipseUiThemeToStringConverter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YObject To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YObject To String Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYObjectToStringConverter(YObjectToStringConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YString To Byte Array Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YString To Byte Array Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYStringToByteArrayConverter(YStringToByteArrayConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YCustom Decimal Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YCustom Decimal Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYCustomDecimalConverter(YCustomDecimalConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YNumeric To Resource Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YNumeric To Resource Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYNumericToResourceConverter(YNumericToResourceConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YString To Resource Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YString To Resource Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYStringToResourceConverter(YStringToResourceConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YNumeric To Resource Config</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YNumeric To Resource Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYNumericToResourceConfig(YNumericToResourceConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YString To Resource Config</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YString To Resource Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYStringToResourceConfig(YStringToResourceConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YNumeric To Uomo Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YNumeric To Uomo Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYNumericToUomoConverter(YNumericToUomoConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YDecimal To Uomo Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YDecimal To Uomo Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYDecimalToUomoConverter(YDecimalToUomoConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YSimple Decimal Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YSimple Decimal Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYSimpleDecimalConverter(YSimpleDecimalConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YVaaclipse Ui Theme To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YVaaclipse Ui Theme To String Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYVaaclipseUiThemeToStringConverter(YVaaclipseUiThemeToStringConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YTaggable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYTaggable(YTaggable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YPrice To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YPrice To String Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYPriceToStringConverter(YPriceToStringConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YQuantity To String Converter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YQuantity To String Converter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYQuantityToStringConverter(YQuantityToStringConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYElement(YElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YConverter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YConverter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYConverter(YConverter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

}
