/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.model.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.osbp.ecview.core.common.model.core.YEmbeddable;
import org.eclipse.osbp.ecview.core.common.model.core.impl.YEmbeddableImpl;
import org.eclipse.osbp.ecview.extension.model.YECviewPackage;
import org.eclipse.osbp.ecview.extension.model.YFocusingStrategy;
import org.eclipse.osbp.ecview.extension.model.YLayoutingInfo;
import org.eclipse.osbp.ecview.extension.model.YLayoutingStrategy;
import org.eclipse.osbp.ecview.extension.model.YStrategyLayout;
import org.eclipse.osbp.ecview.extension.model.YSuspect;
import org.eclipse.osbp.ecview.extension.model.YSuspectInfo;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>YStrategy Layout</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#getLayoutingStrategy <em>Layouting Strategy</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#getFocusingStrategies <em>Focusing Strategies</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#getSuspects <em>Suspects</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#getLayoutingInfo <em>Layouting Info</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#getDefaultFocusingEnhancerId <em>Default Focusing Enhancer Id</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#getNumberColumns <em>Number Columns</em>}</li>
 *   <li>{@link org.eclipse.osbp.ecview.extension.model.impl.YStrategyLayoutImpl#isSaveAndNew <em>Save And New</em>}</li>
 * </ul>
 *
 * @generated
 */
public class YStrategyLayoutImpl extends YEmbeddableImpl implements YStrategyLayout {
	
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	public static final String copyright = "All rights reserved by Loetz GmbH und CoKG Heidelberg 2015.\n\nContributors:\n      Florian Pirchner - initial API and implementation";

	/**
	 * The cached value of the '{@link #getLayoutingStrategy() <em>Layouting Strategy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutingStrategy()
	 * @generated
	 * @ordered
	 */
	protected YLayoutingStrategy layoutingStrategy;

	/**
	 * The cached value of the '{@link #getFocusingStrategies() <em>Focusing Strategies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFocusingStrategies()
	 * @generated
	 * @ordered
	 */
	protected EList<YFocusingStrategy> focusingStrategies;

	/**
	 * The cached value of the '{@link #getSuspects() <em>Suspects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuspects()
	 * @generated
	 * @ordered
	 */
	protected EList<YSuspect> suspects;

	/**
	 * The cached value of the '{@link #getLayoutingInfo() <em>Layouting Info</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayoutingInfo()
	 * @generated
	 * @ordered
	 */
	protected YLayoutingInfo layoutingInfo;

	/**
	 * The default value of the '{@link #getDefaultFocusingEnhancerId() <em>Default Focusing Enhancer Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultFocusingEnhancerId()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_FOCUSING_ENHANCER_ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultFocusingEnhancerId() <em>Default Focusing Enhancer Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultFocusingEnhancerId()
	 * @generated
	 * @ordered
	 */
	protected String defaultFocusingEnhancerId = DEFAULT_FOCUSING_ENHANCER_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumberColumns() <em>Number Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberColumns()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_COLUMNS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumberColumns() <em>Number Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumberColumns()
	 * @generated
	 * @ordered
	 */
	protected int numberColumns = NUMBER_COLUMNS_EDEFAULT;

	/**
	 * The default value of the '{@link #isSaveAndNew() <em>Save And New</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSaveAndNew()
	 * @generated
	 * @ordered
	 */
	protected static final boolean SAVE_AND_NEW_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isSaveAndNew() <em>Save And New</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSaveAndNew()
	 * @generated
	 * @ordered
	 */
	protected boolean saveAndNew = SAVE_AND_NEW_EDEFAULT;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @generated
	 */
	protected YStrategyLayoutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the e class
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return YECviewPackage.Literals.YSTRATEGY_LAYOUT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLayoutingStrategy()
	 *         <em>Layouting Strategy</em>}' containment reference
	 * @generated
	 */
	public YLayoutingStrategy getLayoutingStrategy() {
		return layoutingStrategy;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLayoutingStrategy
	 *            the new layouting strategy
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetLayoutingStrategy(YLayoutingStrategy newLayoutingStrategy, NotificationChain msgs) {
		YLayoutingStrategy oldLayoutingStrategy = layoutingStrategy;
		layoutingStrategy = newLayoutingStrategy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY, oldLayoutingStrategy, newLayoutingStrategy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLayoutingStrategy
	 *            the new cached value of the '{@link #getLayoutingStrategy()
	 *            <em>Layouting Strategy</em>}' containment reference
	 * @generated
	 */
	public void setLayoutingStrategy(YLayoutingStrategy newLayoutingStrategy) {
		if (newLayoutingStrategy != layoutingStrategy) {
			NotificationChain msgs = null;
			if (layoutingStrategy != null)
				msgs = ((InternalEObject)layoutingStrategy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY, null, msgs);
			if (newLayoutingStrategy != null)
				msgs = ((InternalEObject)newLayoutingStrategy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY, null, msgs);
			msgs = basicSetLayoutingStrategy(newLayoutingStrategy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY, newLayoutingStrategy, newLayoutingStrategy));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getFocusingStrategies()
	 *         <em>Focusing Strategies</em>}' containment reference list
	 * @generated
	 */
	public EList<YFocusingStrategy> getFocusingStrategies() {
		if (focusingStrategies == null) {
			focusingStrategies = new EObjectContainmentEList<YFocusingStrategy>(YFocusingStrategy.class, this, YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES);
		}
		return focusingStrategies;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getSuspects() <em>Suspects</em>}
	 *         ' containment reference list
	 * @generated
	 */
	public EList<YSuspect> getSuspects() {
		if (suspects == null) {
			suspects = new EObjectContainmentEList<YSuspect>(YSuspect.class, this, YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS);
		}
		return suspects;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getLayoutingInfo()
	 *         <em>Layouting Info</em>}' containment reference
	 * @generated
	 */
	public YLayoutingInfo getLayoutingInfo() {
		return layoutingInfo;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLayoutingInfo
	 *            the new layouting info
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	public NotificationChain basicSetLayoutingInfo(YLayoutingInfo newLayoutingInfo, NotificationChain msgs) {
		YLayoutingInfo oldLayoutingInfo = layoutingInfo;
		layoutingInfo = newLayoutingInfo;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO, oldLayoutingInfo, newLayoutingInfo);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newLayoutingInfo
	 *            the new cached value of the '{@link #getLayoutingInfo()
	 *            <em>Layouting Info</em>}' containment reference
	 * @generated
	 */
	public void setLayoutingInfo(YLayoutingInfo newLayoutingInfo) {
		if (newLayoutingInfo != layoutingInfo) {
			NotificationChain msgs = null;
			if (layoutingInfo != null)
				msgs = ((InternalEObject)layoutingInfo).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO, null, msgs);
			if (newLayoutingInfo != null)
				msgs = ((InternalEObject)newLayoutingInfo).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO, null, msgs);
			msgs = basicSetLayoutingInfo(newLayoutingInfo, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO, newLayoutingInfo, newLayoutingInfo));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the cached value of the '{@link #getDefaultFocusingEnhancerId()
	 *         <em>Default Focusing Enhancer Id</em>}' attribute
	 * @generated
	 */
	public String getDefaultFocusingEnhancerId() {
		return defaultFocusingEnhancerId;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param newDefaultFocusingEnhancerId
	 *            the new cached value of the '
	 *            {@link #getDefaultFocusingEnhancerId()
	 *            <em>Default Focusing Enhancer Id</em>}' attribute
	 * @generated
	 */
	public void setDefaultFocusingEnhancerId(String newDefaultFocusingEnhancerId) {
		String oldDefaultFocusingEnhancerId = defaultFocusingEnhancerId;
		defaultFocusingEnhancerId = newDefaultFocusingEnhancerId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID, oldDefaultFocusingEnhancerId, defaultFocusingEnhancerId));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumberColumns() {
		return numberColumns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumberColumns(int newNumberColumns) {
		int oldNumberColumns = numberColumns;
		numberColumns = newNumberColumns;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__NUMBER_COLUMNS, oldNumberColumns, numberColumns));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSaveAndNew() {
		return saveAndNew;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSaveAndNew(boolean newSaveAndNew) {
		boolean oldSaveAndNew = saveAndNew;
		saveAndNew = newSaveAndNew;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, YECviewPackage.YSTRATEGY_LAYOUT__SAVE_AND_NEW, oldSaveAndNew, saveAndNew));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param otherEnd
	 *            the other end
	 * @param featureID
	 *            the feature id
	 * @param msgs
	 *            the msgs
	 * @return the notification chain
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY:
				return basicSetLayoutingStrategy(null, msgs);
			case YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES:
				return ((InternalEList<?>)getFocusingStrategies()).basicRemove(otherEnd, msgs);
			case YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS:
				return ((InternalEList<?>)getSuspects()).basicRemove(otherEnd, msgs);
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO:
				return basicSetLayoutingInfo(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param resolve
	 *            the resolve
	 * @param coreType
	 *            the core type
	 * @return the object
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY:
				return getLayoutingStrategy();
			case YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES:
				return getFocusingStrategies();
			case YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS:
				return getSuspects();
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO:
				return getLayoutingInfo();
			case YECviewPackage.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID:
				return getDefaultFocusingEnhancerId();
			case YECviewPackage.YSTRATEGY_LAYOUT__NUMBER_COLUMNS:
				return getNumberColumns();
			case YECviewPackage.YSTRATEGY_LAYOUT__SAVE_AND_NEW:
				return isSaveAndNew();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @param newValue
	 *            the new value
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY:
				setLayoutingStrategy((YLayoutingStrategy)newValue);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES:
				getFocusingStrategies().clear();
				getFocusingStrategies().addAll((Collection<? extends YFocusingStrategy>)newValue);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS:
				getSuspects().clear();
				getSuspects().addAll((Collection<? extends YSuspect>)newValue);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO:
				setLayoutingInfo((YLayoutingInfo)newValue);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID:
				setDefaultFocusingEnhancerId((String)newValue);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__NUMBER_COLUMNS:
				setNumberColumns((Integer)newValue);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__SAVE_AND_NEW:
				setSaveAndNew((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY:
				setLayoutingStrategy((YLayoutingStrategy)null);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES:
				getFocusingStrategies().clear();
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS:
				getSuspects().clear();
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO:
				setLayoutingInfo((YLayoutingInfo)null);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID:
				setDefaultFocusingEnhancerId(DEFAULT_FOCUSING_ENHANCER_ID_EDEFAULT);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__NUMBER_COLUMNS:
				setNumberColumns(NUMBER_COLUMNS_EDEFAULT);
				return;
			case YECviewPackage.YSTRATEGY_LAYOUT__SAVE_AND_NEW:
				setSaveAndNew(SAVE_AND_NEW_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @param featureID
	 *            the feature id
	 * @return true, if successful
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_STRATEGY:
				return layoutingStrategy != null;
			case YECviewPackage.YSTRATEGY_LAYOUT__FOCUSING_STRATEGIES:
				return focusingStrategies != null && !focusingStrategies.isEmpty();
			case YECviewPackage.YSTRATEGY_LAYOUT__SUSPECTS:
				return suspects != null && !suspects.isEmpty();
			case YECviewPackage.YSTRATEGY_LAYOUT__LAYOUTING_INFO:
				return layoutingInfo != null;
			case YECviewPackage.YSTRATEGY_LAYOUT__DEFAULT_FOCUSING_ENHANCER_ID:
				return DEFAULT_FOCUSING_ENHANCER_ID_EDEFAULT == null ? defaultFocusingEnhancerId != null : !DEFAULT_FOCUSING_ENHANCER_ID_EDEFAULT.equals(defaultFocusingEnhancerId);
			case YECviewPackage.YSTRATEGY_LAYOUT__NUMBER_COLUMNS:
				return numberColumns != NUMBER_COLUMNS_EDEFAULT;
			case YECviewPackage.YSTRATEGY_LAYOUT__SAVE_AND_NEW:
				return saveAndNew != SAVE_AND_NEW_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->.
	 *
	 * @return the string
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (defaultFocusingEnhancerId: ");
		result.append(defaultFocusingEnhancerId);
		result.append(", numberColumns: ");
		result.append(numberColumns);
		result.append(", saveAndNew: ");
		result.append(saveAndNew);
		result.append(')');
		return result.toString();
	}

	/**
	 * Sets the label.
	 *
	 * @param label
	 *            the new label
	 * @generated NOT
	 */
	@Override
	public void setLabel(String label) {
		// nothing to do

	}

	/**
	 * Sets the label i18n key.
	 *
	 * @param i18nKey
	 *            the new label i18n key
	 * @generated NOT
	 */
	@Override
	public void setLabelI18nKey(String i18nKey) {
		// nothing to do

	}

	/**
	 * Find info for.
	 *
	 * @param yEmbeddable
	 *            the y embeddable
	 * @return the y suspect info
	 * @generated NOT
	 */
	@Override
	public YSuspectInfo findInfoFor(YEmbeddable yEmbeddable) {
		if (getLayoutingInfo() != null) {
			for (YSuspectInfo info : getLayoutingInfo()
					.getActiveSuspectInfos()) {
				if (info.getTarget() == yEmbeddable) {
					return info;
				}
			}
		}
		return null;
	}

	/**
	 * Find last focus.
	 *
	 * @return the y suspect info
	 * @generated NOT
	 */
	@Override
	public YSuspectInfo findLastFocus() {
		return getLayoutingInfo() != null ? findLastFocus(getLayoutingInfo()
				.getFirstFocus()) : null;
	}

	/**
	 * Find last focus.
	 *
	 * @param current
	 *            the current
	 * @return the y suspect info
	 * @generated NOT
	 */
	private YSuspectInfo findLastFocus(YSuspectInfo current) {
		if (current == null) {
			return null;
		}

		if (current.getNextFocus() == null) {
			return current;
		}

		return findLastFocus(current.getNextFocus());
	}

	/**
	 * Gets the label.
	 *
	 * @return the label
	 * @generated NOT
	 */
	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Gets the label i18n key.
	 *
	 * @return the label i18n key
	 * @generated NOT
	 */
	@Override
	public String getLabelI18nKey() {
		// TODO Auto-generated method stub
		return null;
	}

}
