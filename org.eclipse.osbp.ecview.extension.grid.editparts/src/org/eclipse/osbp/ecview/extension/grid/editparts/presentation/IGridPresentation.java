/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.editparts.presentation;

import java.util.Map;

import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;
import org.eclipse.osbp.runtime.common.memento.IMementoHandler;

/**
 * An presentation abstraction for the StrategyLayout.
 *
 * @param <C>
 *            the generic type
 */
public interface IGridPresentation<C> extends IWidgetPresentation<C>,
		IMementoHandler {

	/**
	 * Is used by the editpart to update the header.
	 */
	public void updateHeader();

	/**
	 * Is used by the editpart to update the footer.
	 */
	public void updateFooter();

	/**
	 * Is used by the editpart to update the columns.
	 */
	public void updateColumns();

	/**
	 * Updates the selection type based on the underlying model.
	 */
	public void updateSelectionType();

	/**
	 * Updates the filter in the grid.
	 */
	public void updateFilter();

	/**
	 * Updates the cell style generator.
	 */
	public void updateCellStyleGenerator();

	/**
	 * Sets a UI Kit specific filter. All properties will resolved to
	 * Equals-Filter and be joined by ADD.
	 *
	 * @param filter
	 *            the filter
	 */
	public void setFilter(Map<String, Object> filter);

}
