/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */

package org.eclipse.osbp.ecview.extension.grid.editparts;

import org.eclipse.osbp.ecview.core.common.editpart.IElementEditpart;
import org.eclipse.osbp.runtime.common.dispose.IDisposable;

/**
 * The abstraction for a grid column.
 */
public interface IGridColumnEditpart extends IElementEditpart {

	/**
	 * Is called from the grid presentation and sets the reference to the UI
	 * column.
	 *
	 * @param column
	 *            the new column reference
	 */
	void setColumnReference(ColumnReference column);

	/**
	 * Returns a reference to the column.
	 *
	 * @return the column reference
	 */
	ColumnReference getColumnReference();

	/**
	 * Initializes the column.
	 */
	void initializeColumn();

	/**
	 * The Interface ColumnReference.
	 */
	interface ColumnReference extends IDisposable {

		/**
		 * Sets the new converter for the column.
		 *
		 * @param converter
		 *            the new converter
		 */
		void setConverter(Object converter);

		/**
		 * Sets the new renderer for the column. If datatypes of model and
		 * renderer do not match, a proper converter need to be passed too.
		 *
		 * @param renderer
		 *            the renderer
		 * @param converter
		 *            the converter
		 */
		void setRenderer(Object renderer, Object converter);

		/**
		 * Updates the properties.
		 */
		void updateProperties();

		/**
		 * Updates the editor field.
		 */
		void updateEditorField();
	}
}
