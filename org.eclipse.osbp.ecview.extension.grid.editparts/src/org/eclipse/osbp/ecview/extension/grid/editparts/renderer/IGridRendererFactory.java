/**
 *                                                                            
 *  Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany) 
 *                                                                            
 *  All rights reserved. This program and the accompanying materials           
 *  are made available under the terms of the Eclipse Public License 2.0        
 *  which accompanies this distribution, and is available at                  
 *  https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 *  SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 *  Contributors:                                                      
 * 	   Florian Pirchner - Initial implementation
 * 
 */
package org.eclipse.osbp.ecview.extension.grid.editparts.renderer;

import org.eclipse.osbp.ecview.core.common.context.IViewContext;
import org.eclipse.osbp.ecview.core.common.presentation.IWidgetPresentation;

/**
 * Is used to create UI-Kit specific renderers.
 */
public interface IGridRendererFactory {

	/**
	 * Returns true, if the factory can be used for the given element.
	 * 
	 * @param uiContext
	 *            contains information about the current ui instance
	 * @param editpart
	 *            the editpart for which a renderer should be created.
	 * @return result
	 */
	boolean isFor(IViewContext uiContext, IGridRendererEditpart editpart);

	/**
	 * Is used to create a new instance of a renderer.
	 *
	 * @param uiContext
	 *            contains information about the current ui instance
	 * @param editpart
	 *            the editpart for which a renderer should be created.
	 * @return renderer
	 * @throws IllegalArgumentException
	 *             if no presentation could be created.
	 */
	Object createRenderer(IViewContext uiContext,
			IGridRendererEditpart editpart) throws IllegalArgumentException;
}
